QT += quick multimedia multimediawidgets
CONFIG += c++11  # release

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Refer to the documentation for the
# deprecated API to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
        main.cpp \
        src/controller/displayscontroller.cpp \
        src/controller/playlistcollectioncontroller.cpp \
        src/controller/sessioncontroller.cpp \
        src/model/display.cpp \
        src/model/displaycell.cpp \
        src/model/displaycells.cpp \
        src/model/displaygrid.cpp \
        src/model/displaygridmodel.cpp \
        src/model/displayresolution.cpp \
        src/model/displayresolutionlist.cpp \
        src/model/displayslist.cpp \
        src/model/displaysworkarea.cpp \
        src/model/displayworkareamodel.cpp \
        src/model/logicaldisplay.cpp \
        src/model/physicaldisplay.cpp \
        src/model/playlistcollection.cpp \
        src/model/playlistcollectionlist.cpp \
        src/model/playlistcollectionmodel.cpp \
        src/model/session.cpp \
        src/model/sessionlist.cpp \
        src/model/sessionmodel.cpp \
        src/model/videocontent.cpp \
        src/model/videomanager.cpp \
        src/model/videoplaylist.cpp \
        src/model/videoplaylistmodel.cpp

RESOURCES += \
    qml.qrc

LIBS += -lUser32
win32: LIBS += -lUser32

win32 {
 #CONFIG += static
 #QMAKE_POST_LINK += mt -nologo -manifest $$PWD/manifest.xml -outputresource:$$OUT_PWD/$$TARGET.exe $$escape_expand(\n\t)
}

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH =

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

DISTFILES += \
    src/view/EditorPlaylistWindow.qml \
    src/view/main.qml \
    src/view/qmldir

HEADERS += \
    src/controller/displayscontroller.h \
    src/controller/playlistcollectioncontroller.h \
    src/controller/sessioncontroller.h \
    src/model/display.h \
    src/model/displaycell.h \
    src/model/displaycells.h \
    src/model/displaygrid.h \
    src/model/displaygridmodel.h \
    src/model/displayresolution.h \
    src/model/displayresolutionlist.h \
    src/model/displayslist.h \
    src/model/displaysworkarea.h \
    src/model/displayworkareamodel.h \
    src/model/logicaldisplay.h \
    src/model/physicaldisplay.h \
    src/model/playlistcollection.h \
    src/model/playlistcollectionlist.h \
    src/model/playlistcollectionmodel.h \
    src/model/session.h \
    src/model/sessionlist.h \
    src/model/sessionmodel.h \
    src/model/videocontent.h \
    src/model/videomanager.h \
    src/model/videoplaylist.h \
    src/model/videoplaylistmodel.h

RC_ICONS = screenControl.ico
