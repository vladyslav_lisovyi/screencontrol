#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQuickView>
#include <QDebug>
#include <QQmlContext>

//#include "src/model/displaygridmodel.h"
//#include "src/model/displaygrid.h"
//#include "src/model/displaysworkarea.h"

#include "src/model/videomanager.h"

//#include "src/model/videoplaylist.h"
#include "src/model/videoplaylistmodel.h"

#include "src/model/playlistcollectionlist.h"
//#include "src/model/playlistcollectionlistmodel.h"
#include "src/model/playlistcollectionmodel.h"

//#include "src/model/sessionlist.h"
#include "src/model/sessionmodel.h"

#include "src/controller/sessioncontroller.h"
#include "src/controller/displayscontroller.h"
#include "src/controller/playlistcollectioncontroller.h"

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);

    qmlRegisterType<VideoPlaylistModel>("Video1", 1, 0, "VideoPlaylistModel");
    qmlRegisterUncreatableType<VideoPlaylist>("Video1", 1, 0, "VideoPlaylist",
                                            QStringLiteral("VideoPlaylist should not be created in QML"));

    qmlRegisterType<PlaylistCollectionModel>("PlaylistCollectionList", 1, 0, "PlaylistCollectionModel");
    qmlRegisterUncreatableType<PlaylistCollectionList>("PlaylistCollectionList", 1, 0, "PlaylistCollectionList",
                                            QStringLiteral("PlaylistCollectionList should not be created in QML"));

    qmlRegisterType<SessionModel>("Session", 1, 0, "SessionModel");
    qmlRegisterUncreatableType<SessionList>("Session", 1, 0, "SessionList",
                                            QStringLiteral("SessionList should not be created in QML"));

    qmlRegisterType<VideoManager>("VideoManager", 1, 0, "Videomanager");

    SessionController sessionController;
    //sessionController.loadSession(0);
    //sessionController.displayWorkarea()->setWidthContentWindowRes(1222);
    //sessionController.selectDisplay(1);
    //int aaaa = sessionController.getPlaylistCollectionIndex(1);

    //sessionController.itemPosDown(0);
    //sessionController.deleteDataFromCurrentSession();
    //sessionController.loadSession(1);
    //sessionController.deleteDataFromCurrentSession();
    //sessionController.loadSession(0);

    //sessionController.addVideo("file:///C:/jj4/10_JoJo no Kimyou na Bouken - Diamond wa Kudakenai [Persona99](720).rus.jpn.mkv");
    //sessionController.saveSession();
    //int index = sessionController.getPlaylistCollectionIndexFromPlaylistName("JoJo no Kimyou na Bouken (2015)");
    //sessionController.addNewSession("fff;;k;l");
    //sessionController.selectDisplay(0);
    //sessionController.selectDisplay(1);
    //sessionController.selectDisplay(0);
    //sessionController.selectDisplay(1);

    //sessionController.loadSession(0);

    //sessionController.displaysController()->selectDisplay(0);
    //DisplayWorkareaModel* model = new DisplayWorkareaModel();
    //model->setDisplayCells(sessionController.displaysController()->displayWorkarea()->model()->displayCells());
    //DisplaysController displaysController;
    //QSharedPointer<DisplaysController> displaysController = QSharedPointer<DisplaysController>(sessionController.displaysController());
    //PlaylistCollectionController* playlistCollectionController = sessionController.playlistCollectionController();

//    displaysController = sessionController.displaysController();
//    //playlistCollectionController = sessionController.playlistCollectionController();
    //displaysController->selectDisplay(0);
    //displaysController->currentDisplay()->setPosition(60,60,2000,768);
//    displaysController->currentDisplay()->setPosition(160,60,2700,768);


    //displaysController->selectDisplay(1);
    //qDebug() << "Columns " << displaysController->displayWorkarea()->getColumns();
    //sessionController.addNewSession("fff;;k;l");
    //sessionController.displaysController()->selectDisplay(0);
    //sessionController.playlistCollectionController()->playlistCollection()->appendItem("file:///C:/jj3/JoJo no Kimyou na Bouken (2015)");

    int a = 0;
    //SessionList sessions;
    //sessions.checkAvailableSaves();
    //sessions.loadingSessionsFromFolder();
    //sessions.createSave("sas");

    //VideoPlaylist playlist;
    //playlist.initVideoPlaylistByFolder("file:///C:/jj3/JoJo no Kimyou na Bouken (2015)");

    //sessions.loadSave(0);


    //DisplayGrid displayGrid;
    //DisplaysWorkarea displaysWorkarea;// = nullptr; // = sessions.selectedSession()->displayWorkarea();// = *(sessions.selectedSession()->displayWorkarea()); //->addVideo("file:///C:/sillent hill/Silent Hill Theme (Gingertail Cover).webm");
//    VideoContent video = VideoContent("file:///C:/sillent hill/Silent Hill Theme (Gingertail Cover).webm");
//    VideoPlaylist playlist;
    //playlist.setPlaylistName("Sillent hill");
//    playlist.appendItem("file:///C:/sillent hill/Silent Hill Theme (Gingertail Cover).webm");
//    playlist.appendItem("file:///C:/sillent hill/Silent Hill 2 - Promise (Reprise) Gingertail Cover.webm");
//    playlist.appendItem("file:///C:/sillent hill/Silent Hill 2 - Promise (Reprise) Gingertail Cover.webm");
//    playlist.appendItem("file:///C:/jj3/JoJo no Kimyou na Bouken (2015)/01_JoJo no Kimyou na Bouken - Stardust Crusaders - Egypt Hen [Persona99](720).rus.jpn.mkv");
//    playlist.appendItem("file:///C:/jj3/JoJo no Kimyou na Bouken (2015)/02_JoJo no Kimyou na Bouken - Stardust Crusaders - Egypt Hen [Persona99](720).rus.jpn.mkv");
//    playlist.appendItem("file:///C:/jj3/JoJo no Kimyou na Bouken (2015)/03_JoJo no Kimyou na Bouken - Stardust Crusaders - Egypt Hen [Persona99](720).rus.jpn.mkv");
//    playlist.appendItem("file:///C:/jj3/JoJo no Kimyou na Bouken (2015)/04_JoJo no Kimyou na Bouken - Stardust Crusaders - Egypt Hen [Persona99](720).rus.jpn.mkv");

//    playlist.changeVideo(1, "file:///C:/jj3/JoJo no Kimyou na Bouken (2015)/05_JoJo no Kimyou na Bouken - Stardust Crusaders - Egypt Hen [Persona99](720).rus.jpn.mkv");

//    VideoPlaylist2 playlist2;
//    playlist2.appendItem("file:///C:/sillent hill/Silent Hill Theme (Gingertail Cover).webm");
//    playlist2.appendItem("file:///C:/sillent hill/Silent Hill 2 - Promise (Reprise) Gingertail Cover.webm");

//    QString q = "file:///C:/sillent hill/Silent Hill 2 - Promise (Reprise) Gingertail Cover.webm";
//    QStringList q2 = q.split('.');

    PlaylistCollectionList collection;// = nullptr;
    //collection.appendItem("file:///C:/jj2/JoJo no Kimyou na Bouken (2014)"); /// file:///C:/jj2/JoJo no Kimyou na Bouken (2014)

    //    collection.appendItem("file:///C:/jj2/JoJo no Kimyou na Bouken (2014)");
//    collection.appendItem("file:///C:/jj3/JoJo no Kimyou na Bouken (2015)");
//    VideoPlaylistModel* videoModel = new VideoPlaylistModel();
//    videoModel->addVideo("file:///C:/sillent hill/Silent Hill Theme (Gingertail Cover).webm");
//    //displaysWorkarea.setVideoModel()
//    displaysWorkarea.saveToFile();
    //VideoPlaylist* playlist = new VideoPlaylist();
    //playlist->appendItem("file:///C:/sillent hill/Silent Hill Theme (Gingertail Cover).webm");

    //qDebug() << res;
    //cell.displays[1].showHideTaskBar(true);
    //DisplaysList list = DisplaysList();
    int i = 0;
    QQmlApplicationEngine engine;

    //engine.rootContext()->setContextProperty(QStringLiteral("displayGrid"), &displayGrid);
    engine.rootContext()->setContextProperty(QStringLiteral("sessionController"), &sessionController);
    //engine.rootContext()->setContextProperty(QStringLiteral("displaysController"), &displaysController);
    //engine.rootContext()->setContextProperty(QStringLiteral("playlistCollectionController"), playlistCollectionController);

    //engine.rootContext()->setContextProperty(QStringLiteral("sessions"), &sessions);
    //engine.rootContext()->setContextProperty(QStringLiteral("displaysWorkarea"), displaysWorkarea);
    //engine.rootContext()->setContextProperty(QStringLiteral("collection"), collection);
    //engine.rootContext()->setContextProperty(QStringLiteral("collection"), &collection);
    //engine.rootContext()->setContextProperty(QStringLiteral("displaysWorkarea"), &displaysWorkarea);

    //engine.rootContext()->setContextProperty(QStringLiteral("videoPlaylist"), &playlist);

    engine.rootContext()->setContextProperty(QStringLiteral("collection"), &collection);

    //VideoManager videomanager;
    //QQmlContext *context = engine.rootContext();
    //context->setContextProperty("videomanager", &videomanager);
    //engine.rootContext()->setContextProperty(QStringLiteral("videomanager"), &videomanager);
    //if (engine.rootObjects().isEmpty())
    //    return -1;


    const QUrl url(QStringLiteral("qrc:/src/view/main.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine.load(url);

    //if(sessions.selectedSession() != nullptr)
    //    displaysWorkarea = (sessions.selectedSession()->displayWorkarea());

    return app.exec();
}
