import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.3
import QtQuick.Dialogs 1.2

import Video1 1.0
import PlaylistCollectionList 1.0
import Session 1.0

ApplicationWindow {
    id: mainWindow
    visible: true
    width: 640
    height: 480
    title: qsTr("Screen Control")
    minimumWidth: 640
    minimumHeight: 480

    GroupBox {
        id: mainGroupBox
        anchors.fill: parent
        title: qsTr("Main menu")
        anchors.leftMargin: 10
        anchors.rightMargin: 10
        anchors.bottomMargin: 10
        anchors.topMargin: 10

        GridLayout {
            id: mainGrid
            anchors.fill: parent
            rows: 2
            columns: 3

            Item {
                 //color: "red"
                 Layout.fillHeight: true
                 Layout.fillWidth: true
                 Layout.columnSpan: 3
                 Layout.row: 0
                 Layout.column: 0

                 ListView {
                     id: listViewsSessions
                     anchors.fill: parent
                     clip: true
                     boundsBehavior: Flickable.StopAtBounds

                     ScrollBar.vertical: ScrollBar {}

                     model: SessionModel {
                         id: sessionsModel
                         //sessionList: sessions
                         sessionList: sessionController.sessionList
                     }

                     delegate: Item {
                         anchors.horizontalCenter: parent.horizontalCenter

                         height: 40
                         width: listViewsSessions.width


                         Rectangle {
                             //id: wrapper
                             anchors.fill: parent
                             border {
                                 color: "black"
                                 width: 1
                             }

                             Text {
                                 anchors.centerIn: parent
                                 renderType: Text.NativeRendering
                                 text: model.filename
                                 //text: model.color
                                 font.pointSize: 10
                             }

                             MouseArea {
                                 anchors.fill: parent
                                 onClicked: {
                                     if (!ListView.isCurrentItem)
                                     {
                                         listViewsSessions.currentIndex = index;

                                     }
                                     else
                                         listViewsSessions.currentIndex = 0;
                                 }
                             }
                         }
                     }

                     highlightFollowsCurrentItem: true
                     highlightMoveDuration: 1
                     highlightResizeVelocity : -1

                     highlight: Rectangle {
                         border.color: "#797979"
                         border.width: 3
                         color: "#797979"
                         opacity: 0.3
                         width: listViewsSessions.width
                         y:  listViewsSessions.currentItem.y
                         z: Infinity
                     }
                 }
            }

            Item {
                 Layout.fillHeight: true
                 Layout.fillWidth: true
                 Layout.row: 1
                 Layout.column: 0
                 Layout.maximumHeight: 100

                 Button {
                     id: createButton
                     anchors.horizontalCenter: parent.horizontalCenter
                     anchors.verticalCenter: parent.verticalCenter
                     width: parent.fill
                     height: 50
                     text: "Create"
                     font.family: "Verdana"
                     spacing: 4

                     onClicked: {
                         sessionSaveWindow.show();
                     }
                 }
            }

            Item {
                 //color: "black"
                 Layout.fillHeight: true
                 Layout.fillWidth: true
                 Layout.row: 1
                 Layout.column: 1
                 Layout.maximumHeight: 100
                 Layout.maximumWidth: 100
            }

            Item {
                 //color: "blue"
                 Layout.fillHeight: true
                 Layout.fillWidth: true
                 Layout.row: 1
                 Layout.column: 2
                 Layout.maximumHeight: 100

                 Button {
                     id: loadButton
                     anchors.horizontalCenter: parent.horizontalCenter
                     anchors.verticalCenter: parent.verticalCenter
                     width: parent.fill
                     height: 50
                     text: "Load"

                     onClicked: {
                         if(listViewsSessions.currentIndex != -1)
                         {
                             sessionController.loadSession(listViewsSessions.currentIndex)
                             //contentWindow.show()
                             //editorWindow.visible = true
                         }
                     }

                     Connections {
                         target: sessionController
                         onConfigurationException: {
                             console.log("ERROR")
                             //editorWindow.visible = false
                             configMessageBox.open()
                         }

                         onCurrentSessionLoaded: {
                             editorWindow.show()
                         }
                     }
                 }
            }
        }
    }

    EditorPlaylistWindow {
        id: editorWindow
        title: qsTr("Second")
        //visible: true

        onCloseEditorWindow: {
            editorWindow.close()
            mainWindow.show()
        }

        Connections {
            target: editorWindow
            onSaveCurrentSession: {
                //listViewsSessions.model.get(listViewsSessions.currentIndex).displayworkarea = displaysWorkarea
                //listViewsSessions.model.get(listViewsSessions.currentIndex).playlistCollectionList = collection
                //sessions.saveCurrentSession(displaysWorkarea, collection)
            }
        }
    }

    SessionSave {
        id: sessionSaveWindow

        onCancelPressed: {
            sessionSaveWindow.close()
            mainWindow.show()
        }

        Connections {
            target: sessionSaveWindow
            onSavePressed: {
                var index = sessionController.addNewSession(filename)
                listViewsSessions.currentIndex = parseInt(index)
                sessionSaveWindow.close()
                //editorWindow.currentDisplayWorkarea = displaysWorkarea
                //editorWindow.currentPlaylistCollection = collection
                editorWindow.show()
                //mainWindow.hide();
            }
        }
    }

    Item {
        Connections {
            target: sessionController
            onCurrentSessionChanged: {
                //console.log("Current Session Changed")
            }
        }
    }

    MessageDialog {
        id: configMessageBox
        title: "Configuration error"
        icon: StandardIcon.Critical
        text: "Current display's configuration does not match the saved configuration. Please check current display's configuration"
        standardButtons: StandardButton.Ok
        //Component.onCompleted: visible = true
        onAccepted: {
            configMessageBox.close()
            //visible = false
            Qt.quit()
        }
    }
}


