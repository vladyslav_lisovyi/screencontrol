import QtQuick 2.0
import QtQuick.Window 2.3
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.3

ApplicationWindow {
    id: saveSessionWindow
    title: qsTr("New session")
    minimumWidth: 350
    minimumHeight: 240

    signal cancelPressed
    signal savePressed(string filename)

    GridLayout {
        id: gridNewSession
        anchors.fill: parent
        rows: 2
        columns: 2

        Item {
            id: saveNameItem
            Layout.row: 0
            Layout.column: 0
            Layout.minimumWidth: 100
            Layout.maximumWidth: 100
            Layout.fillHeight: true
            //Layout.minimumWidth: 100

            Text {
                id: saveNameText
                text: qsTr("File name:")
                font.pointSize: 10
                height: 20
                width: 50
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.verticalCenter
            }
        }

        Item {
            Layout.row: 0
            Layout.column: 1
            Layout.fillHeight: true
            Layout.fillWidth: true

            TextField {
                id: saveNameTextfield
                width: parent.width - 20
                height: 40
                font.pointSize: 10
                anchors.left: parent.left
                anchors.verticalCenter: parent.verticalCenter
                //Layout.alignment: Qt.AlignLeft
                anchors.leftMargin: 5
                anchors.rightMargin: 5
                validator: RegExpValidator {regExp: /([^:*"<>|/\\])+/}
            }
        }

        Item {
            id: saveButtonItem
            Layout.row: 1
            Layout.column: 0
            Layout.columnSpan: 2
            height: 50
            Layout.fillWidth: true

            Button {
                id: saveSession
                height: 30
                width: 80
                anchors.right: cancelSession.left
                anchors.rightMargin: 5
                anchors.verticalCenter: parent.verticalCenter
                text: "Save"
                enabled: saveNameTextfield.text.length >= 1 ? true : false
                onClicked: {
                    //console.log(saveNameTextfield.text)
                    saveSessionWindow.savePressed(saveNameTextfield.text)
                    saveNameTextfield.text = ""
                }
            }

            Button {
                id: cancelSession
                height: 30
                width: 80
                anchors.right: parent.right
                anchors.rightMargin: 5
                anchors.verticalCenter: parent.verticalCenter
                text: "Cancel"
                onClicked: saveSessionWindow.cancelPressed()
            }
        }
    }
}
