﻿import QtQuick 2.12
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.12
import QtQuick.Window 2.3
import QtQuick.Dialogs 1.2
//import QtQml.Models 2.3
import QtMultimedia 5.12

import VideoManager 1.0
import Video1 1.0
import PlaylistCollectionList 1.0

ApplicationWindow {
    id: editorPlaylist
    //visibility: "FullScreen"
    //visible: false
    //flags: Qt.Window | Qt.FramelessWindowHint
    flags: Qt.Window

    signal closeEditorWindow
    signal saveCurrentSession
    //screen: Qt.application.screens[1]
    //x: screen.virtualX
    //y: screen.virtualY + 35 // + screen.height - height

    minimumHeight: 705 //Screen.desktopAvailableHeight
    minimumWidth: 1000 //Screen.desktopAvailableWidth
    height: 705
    width: 1000
    property variant contentVideo

    property variant currentDisplayWorkarea //: displaysController
    property variant currentPlaylistCollection //: collection //null
    //property alias contentWindowOpen: contentWindow
    //visibility: "Maximized"
    //property int x: value
    //property int y: value
    //property int width: value
    //property int height: value

    //minimumHeight: 600
    //minimumWidth: 1000

    //DisplaysWorkarea {
    //    id: displaysworkarea
    //}

    header: TabBar {
        position: TabBar.Header
        font.pointSize: 9
        currentIndex: 1
        id: bar
        width: 400

        TabButton {
            text: qsTr("Screeen editor")
            height: 30
        }

        TabButton {
            text: qsTr("Playlist")
            height: 30
        }

        onCurrentIndexChanged: {
            //if(displayRepeater.display == -1)
            if(currentIndex === 0)
            {
                if(displayRepeater.display != -1) //displayRepeater.display
                {
                    sessionController.selectDisplay(displayRepeater.display)
                    control.currentIndex = sessionController.resolutionIndex()
                }
                if(displayRepeater.display == -1) //displayRepeater.display
                {
                    control.currentIndex = -1
                }
            }
            if(currentIndex === 1)
            {
                if(displayContentRepeater.contentDisplay != -1)
                {
                    sessionController.selectDisplay(displayContentRepeater.contentDisplay)
                }
            }
        }
    }

    StackLayout {
        id: stackLayout
        visible: true
        anchors.fill: parent
        currentIndex: bar.currentIndex

        Page {
            id: editorPageTab
            //visible: false
            //z: 1
            padding: 5

            //background: Rectangle {
                //visible: !styleData.hasColor
                //color: "#af0faf" //SystemPaletteSingleton.window(true)
            //}

            GridLayout {
                id: editorGridLayout
                anchors.fill: parent
                //rows: 2
                //columns: 3
                rows: 3
                columns: 2
                rowSpacing: 10
                //property real buttonSize: 20

                Item { // Rectangle {
                    id: displaysItem
                    //color: "blue"
                    Layout.fillHeight: true
                    Layout.fillWidth: true
                    Layout.row: 0
                    Layout.column: 0
                    //Layout.columnSpan: 3
                    Layout.columnSpan: 2
                    Layout.maximumHeight: 200

                    ColumnLayout {
                        id: displaysGridLayout
                        anchors.fill: parent
                        //rows: 1
                        //columns: 2
                        //columnSpacing: 20
                        clip: true

                        Text {
                            id: displaysText
                            text: qsTr("Displays")
                            Layout.alignment: Qt.AlignHCenter | Qt.AlignTop
                            //horizontalAlignment: Text.AlignLeft
                            font.pointSize: 12

                            //Layout.row: 0
                            //Layout.column: 0
                            //anchors.top: parent.top
                            //Layout.alignment: Qt.AlignTop
                        }

                        Frame {
                            Layout.maximumHeight: 180
                            Layout.row: 0
                            Layout.column: 0
                            Layout.fillHeight: true
                            Layout.fillWidth: true
                            clip: true
                            z: -1
                            //anchors.top: displaysText.bottom
                            //Layout.alignment: Qt.AlignBottom
                            //anchors.topMargin: 5

                            Repeater {
                                id: displayRepeater
                                //anchors.fill: parent
                                width: parent.width - 30
                                height: parent.height
                                Layout.alignment: Qt.AlignTop & Qt.AlignLeft
                                //anchors.top: parent.top
                                //anchors.left: parent.left
                                clip: true
                                z: 4

                                property int display: -1

                                //model: displaysWorkarea.model

                                //model: editorPlaylist.currentDisplayWorkarea.displayWorkarea.model
                                //model:  displaysController.displayWorkarea.model
                                model: sessionController.displayWorkarea.model

                                delegate:
                                    Rectangle {
                                        id: selectionRect
                                        border.width: 1
                                        border.color: "black"
                                        color: "white" // "transparent"
                                        //x: ((model.position.x + sessionController.displayWorkarea.startPos.x) / sessionController.displayWorkarea.fullResolution.width) * displayRepeater.width //* 0.3 //((model.position.x - editorPlaylist.currentDisplayWorkarea.startPos.x) / editorPlaylist.currentDisplayWorkarea.fullResolution.width) * displayContentRepeater.width * 0.3  //(model.position.x/displaysWorkarea.fullResolution.width) * displayContentRepeater.width
                                        //y: ((model.position.y + sessionController.displayWorkarea.startPos.y) / sessionController.displayWorkarea.fullResolution.height) * displayRepeater.height //* 0.3 //((model.position.y - editorPlaylist.currentDisplayWorkarea.startPos.y) / editorPlaylist.currentDisplayWorkarea.fullResolution.height) * displayContentRepeater.height * 0.3 //(model.position.y/displaysWorkarea.fullResolution.height) * displayContentRepeater.height
                                        //width: (model.resolution.width/(sessionController.displayWorkarea.fullResolution.width - (sessionController.displayWorkarea.startPos.right - model.position.x)))*displayRepeater.width // + (sessionController.displayWorkarea.startPos.right - model.position.x)
                                        //height: (model.resolution.height/(sessionController.displayWorkarea.fullResolution.height))*displayRepeater.height // (sessionController.displayWorkarea.startPos.bottom - model.position.y)
                                        //z: mouseArea.drag.active || mouseArea.pressed ? 2 : 1
                                        property var xDrawPos: sessionController.displayWorkarea.startPos.x < 0 ? sessionController.displayWorkarea.startPos.x : 0
                                        property var yDrawPos: sessionController.displayWorkarea.startPos.y < 0 ? sessionController.displayWorkarea.startPos.y : 0

//                                        x: (model.position.x === 0 && model.position.y === 0) ? ((model.position.x) / sessionController.displayWorkarea.fullResolution.width) * displayRepeater.width // * (1.0 - (sessionController.displayWorkarea.xOffset/displayRepeater.width))
//                                                                                              : ((model.position.x + sessionController.displayWorkarea.xOffset) / sessionController.displayWorkarea.fullResolution.width) * displayRepeater.width // * (1.0 - (sessionController.displayWorkarea.xOffset/displayRepeater.width))
//                                        y: (model.position.x === 0 && model.position.y === 0) ? ((model.position.y - selectionRect.yDrawPos) / sessionController.displayWorkarea.fullResolution.height) * displayRepeater.height // * (1.0 - (sessionController.displayWorkarea.yOffset/displayRepeater.height))
//                                                                                              : ((model.position.y + sessionController.displayWorkarea.yOffset - selectionRect.yDrawPos) / sessionController.displayWorkarea.fullResolution.height) * displayRepeater.height // * (1.0 - (sessionController.displayWorkarea.yOffset/displayRepeater.height))
//                                        width: (model.position.x === 0 && model.position.y === 0) ? (model.resolution.width/(sessionController.displayWorkarea.fullResolution.width)) * (displayRepeater.width - (sessionController.displayWorkarea.xOffset)) //* (1.0 - (sessionController.displayWorkarea.xOffset/displayRepeater.width))
//                                                                                                  : (model.resolution.width/(sessionController.displayWorkarea.fullResolution.width - (sessionController.displayWorkarea.startPos.x - model.position.x)))* (displayRepeater.width - (sessionController.displayWorkarea.xOffset)) //* (1.0 - (sessionController.displayWorkarea.xOffset/displayRepeater.width))
//                                        height: (model.position.x === 0 && model.position.y === 0) ? ((model.resolution.height)/sessionController.displayWorkarea.fullResolution.height) * (displayRepeater.height - Math.abs(sessionController.displayWorkarea.yOffset)) //* (1.0 - (sessionController.displayWorkarea.yOffset/displayRepeater.height))
//                                                                                                   : (model.resolution.height/(sessionController.displayWorkarea.fullResolution.height - (sessionController.displayWorkarea.startPos.y - model.position.y))) * (displayRepeater.height - Math.abs(sessionController.displayWorkarea.yOffset)) // * (1.0 - (sessionController.displayWorkarea.yOffset/displayRepeater.height))

                                        x: ((model.position.x) / sessionController.displayWorkarea.fullResolution.width) * displayRepeater.width //* 0.3 //((model.position.x - editorPlaylist.currentDisplayWorkarea.startPos.x) / editorPlaylist.currentDisplayWorkarea.fullResolution.width) * displayContentRepeater.width * 0.3  //(model.position.x/displaysWorkarea.fullResolution.width) * displayContentRepeater.width
                                        y: ((model.position.y) / sessionController.displayWorkarea.fullResolution.height) * displayRepeater.height //* 0.3 //((model.position.y - editorPlaylist.currentDisplayWorkarea.startPos.y) / editorPlaylist.currentDisplayWorkarea.fullResolution.height) * displayContentRepeater.height * 0.3 //(model.position.y/displaysWorkarea.fullResolution.height) * displayContentRepeater.height
                                        width: (model.resolution.width/sessionController.displayWorkarea.fullResolution.width)*displayRepeater.width // + (sessionController.displayWorkarea.startPos.right - model.position.x)
                                        height: (model.resolution.height/sessionController.displayWorkarea.fullResolution.height)*displayRepeater.height // (sessionController.displayWorkarea.startPos.bottom - model.position.y)

                                        z: mouseArea.drag.active || mouseArea.pressed ? 0 : -1
                                        property variant myData
                                        //property alias displayCellIndex: index

                                        Text {
                                            id: idDisplayText
                                            //text: (model.idDisplay)
                                            text: model.idDisplay
                                            //text: displaysWorkarea.gridModel.displayId
                                            //text: displaysWorkarea.fullResolution.width
                                            //text: (model.resolution.width/displaysWorkarea.fullResolution.width)*displayRepeater.width
                                            //text: (model.position.x/displaysWorkarea.fullResolution.width)*displayRepeater.width
                                            //text: model.resolution.width
                                            font.pointSize: 20
                                            horizontalAlignment: Text.Center
                                            width: 20
                                            height: 50
                                            anchors.horizontalCenter: parent.horizontalCenter
                                            anchors.verticalCenter: parent.verticalCenter
                                            //Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                                        }

                                        Text {
                                            id: primaryScreen
                                            text: qsTr("(primary screen)")
                                            font.pointSize: 10
                                            height: 50
                                            verticalAlignment: Text.Center
                                            anchors.left: (idDisplayText.right)
                                            anchors.verticalCenter: idDisplayText.verticalCenter
                                            visible: model.primaryScreen ? true : false
                                        }

                                        MouseArea {
                                            id: mouseArea
                                            anchors.fill: parent
                                            //z: 0
                                            onClicked: {
                                                if(displayRepeater.display == -1)
                                                {
                                                    console.log("index " + index)
                                                    // (model.resolution.width/(sessionController.displayWorkarea.fullResolution.width - (sessionController.displayWorkarea.startPos.right - model.position.x)))*displayRepeater.width

                                                    console.log(index + " fullResolution.width " + sessionController.displayWorkarea.fullResolution.width)
                                                    console.log(index + " fullResolution.height " + sessionController.displayWorkarea.fullResolution.height)
                                                    console.log(index + " x " + (model.position.x / sessionController.displayWorkarea.fullResolution.width) * displayRepeater.width)
                                                    console.log(index + " y " + (model.position.y / sessionController.displayWorkarea.fullResolution.height) * displayRepeater.height)
                                                    console.log(index + " width " + (model.resolution.width/sessionController.displayWorkarea.fullResolution.width)*displayRepeater.width )
                                                    console.log(index + " height " + (model.resolution.height/sessionController.displayWorkarea.fullResolution.height)*displayRepeater.height )

                                                    displayRepeater.itemAt(index).color = "green"
                                                    displayRepeater.display = index
                                                    //if(sessionController.currentDisplayIndex() != -1)// && sessionController.currentDisplayIndex() != displayRepeater.display)
                                                    sessionController.selectDisplay(index)
                                                    //editorPlaylist.currentDisplayWorkarea.selectDisplay(index)
                                                    selectionRect.myData = model

                                                    //selectedItem.displayCellIndex = model.index

                                                    //verticalResolutionValue.text = model.resolution.height
                                                    //horizontalResolutionValue.text = model.resolution.width

                                                    leftOffsetValue.text = model.position.x
                                                    topOffsetValue.text = model.position.y
                                                    rightOffsetValue.text = (model.position.right)
                                                    bottomOffsetValue.text = (model.position.bottom)

                                                    topBlending.text = model.blendingTop
                                                    topOpacity.text = model.blendingOpacityTop.toFixed(3)
                                                    topBlendTransitionPosition.text = model.blendingPositionTop.toFixed(3)

                                                    bottomBlending.text = model.blendingBottom
                                                    bottomOpacity.text = model.blendingOpacityBottom.toFixed(3)
                                                    bottomBlendTransitionPosition.text = model.blendingPositionBottom.toFixed(3)

                                                    leftBlending.text = model.blendingLeft
                                                    leftOpacity.text = model.blendingOpacityLeft.toFixed(3)
                                                    leftBlendTransitionPosition.text = model.blendingPositionLeft.toFixed(3)

                                                    rightBlending.text = model.blendingRight
                                                    rightOpacity.text = model.blendingOpacityRight.toFixed(3)
                                                    rightBlendTransitionPosition.text = model.blendingPositionRight.toFixed(3)

                                                    //console.log(displaysWorkarea.model.resolutionIndex(displayRepeater.display))
                                                    control.currentIndex = sessionController.resolutionIndex()

                                                    //console.log("ID " + model.physID)
                                                    //console.log(displaysWorkarea.gridModel.getDisplayIndex(model.physID))
                                                    //listViewDisplays.currentIndex = editorPlaylist.currentDisplayWorkarea.gridModel.getDisplayIndex(model.physID)
                                                }

                                                else
                                                {
                                                    console.log("index " + index)

                                                    displayRepeater.itemAt(displayRepeater.display).color = "white"
                                                    displayRepeater.itemAt(index).color = "green"
                                                    displayRepeater.display = index
                                                    if(sessionController.currentDisplayIndex() != displayRepeater.display )
                                                        sessionController.selectDisplay(index)
                                                    //editorPlaylist.currentDisplayWorkarea.selectDisplay(index)
                                                    selectionRect.myData = model

                                                    //selectedItem.displayCellIndex = model.index

                                                    //verticalResolutionValue.text = model.resolution.height
                                                    //horizontalResolutionValue.text = model.resolution.width

                                                    leftOffsetValue.text = model.position.x
                                                    topOffsetValue.text = model.position.y
                                                    rightOffsetValue.text = (model.position.right)
                                                    bottomOffsetValue.text = (model.position.bottom)

                                                    topBlending.text = model.blendingTop
                                                    topOpacity.text = model.blendingOpacityTop.toFixed(3)
                                                    topBlendTransitionPosition.text = model.blendingPositionTop.toFixed(3)

                                                    bottomBlending.text = model.blendingBottom
                                                    bottomOpacity.text = model.blendingOpacityBottom.toFixed(3)
                                                    bottomBlendTransitionPosition.text = model.blendingPositionBottom.toFixed(3)

                                                    leftBlending.text = model.blendingLeft
                                                    leftOpacity.text = model.blendingOpacityLeft.toFixed(3)
                                                    leftBlendTransitionPosition.text = model.blendingPositionLeft.toFixed(3)

                                                    rightBlending.text = model.blendingRight
                                                    rightOpacity.text = model.blendingOpacityRight.toFixed(3)
                                                    rightBlendTransitionPosition.text = model.blendingPositionRight.toFixed(3)

    //                                                if(selectionRect.currentIndex == index)
    //                                                    selectionRect.currentIndex = -1
    //                                                else
    //                                                    selectionRect.currentIndex = index
                                                    //console.log(displaysWorkarea.model.resolutionIndex(displayRepeater.display))
                                                    control.currentIndex = sessionController.resolutionIndex()

                                                    //console.log("ID " + model.physID)
                                                    //console.log(displaysWorkarea.gridModel.getDisplayIndex(model.physID))
                                                    //listViewDisplays.currentIndex = editorPlaylist.currentDisplayWorkarea.gridModel.getDisplayIndex(model.physID)
                                                }
                                            }
                                        }
                                    }
                                }
                        }
                    }

                }

                Item {
                    id: displayControlItem
                    width: 300
                    //color: "green"
                    Layout.fillHeight: true
                    //Layout.fillWidth: true
                    Layout.row: 1
                    Layout.column: 0
                    Layout.minimumWidth: 300
                    Layout.preferredWidth: 300
                    //width: 300

                    Frame {
                        anchors.fill: parent

                        ColumnLayout { // ColumnLayout
                            //id: columnLayout
                            id: columnLayout
                            //rows: 4
                            //columns: 1
                            anchors.fill: parent

                            Item {
                                id: startPosFullResItem

                                Layout.fillWidth: true
                                height: 100
                                Layout.maximumHeight: 100
                                Layout.alignment: Qt.AlignTop

                                ColumnLayout {
                                    anchors.fill: parent

                                    Text {
                                        id: startPosText
                                        text: qsTr("Start position")
                                        Layout.bottomMargin: 0
                                        font.pointSize: 10
                                        font.weight: Font.ExtraLight
                                    }

                                    Item {
                                        Layout.fillWidth: true
                                        Layout.fillHeight: true

                                        RowLayout {
                                            id: rowLayout
                                            Layout.fillWidth: true
                                            anchors.fill: parent

                                            Item {
                                                Layout.fillWidth: true
                                                Layout.fillHeight: true

                                                Layout.maximumWidth: 20

                                                Text {
                                                    text: qsTr("x")
                                                    height: 20
                                                    width: 10
                                                    font.pointSize: 10
                                                    font.weight: Font.ExtraLight
                                                    anchors.right: parent.right
                                                    anchors.verticalCenter: parent.verticalCenter
                                                }
                                            }

                                            Item {
                                                Layout.fillWidth: true
                                                Layout.fillHeight: true

                                                TextField {
                                                    id: xStartPos
                                                    height: 20
                                                    width: parent.width
                                                    font.pointSize: 10
                                                    font.weight: Font.ExtraLight
                                                    anchors.verticalCenter: parent.verticalCenter
                                                    text: sessionController.displayWorkarea.startPos.x
                                                    horizontalAlignment: Text.AlignHCenter
                                                    verticalAlignment: Text.AlignVCenter
                                                    rightPadding: 6
                                                    leftPadding: 6
                                                    padding: 2
                                                    validator: RegExpValidator {regExp: /[\d-]+/}
                                                    background: Rectangle {

                                                        color: "#797979"
                                                        opacity: 0.3

                                                    }

                                                    onEditingFinished :
                                                    {
                                                        sessionController.displayWorkarea.setXStartPosition(parseInt(xStartPos.text))
                                                    }
                                                }
                                            }

                                            Item {
                                                Layout.fillWidth: true
                                                Layout.fillHeight: true

                                                Layout.maximumWidth: 20

                                                Text {
                                                    text: qsTr("y")
                                                    height: 20
                                                    width: 10
                                                    font.pointSize: 10
                                                    font.weight: Font.ExtraLight
                                                    anchors.right: parent.right
                                                    anchors.verticalCenter: parent.verticalCenter
                                                }
                                            }

                                            Item {
                                                Layout.fillWidth: true
                                                Layout.fillHeight: true

                                                TextField {
                                                    id: yStartPos
                                                    height: 20
                                                    width: parent.width
                                                    font.pointSize: 10
                                                    font.weight: Font.ExtraLight
                                                    anchors.verticalCenter: parent.verticalCenter
                                                    text: sessionController.displayWorkarea.startPos.y
                                                    horizontalAlignment: Text.AlignHCenter
                                                    verticalAlignment: Text.AlignVCenter
                                                    rightPadding: 6
                                                    leftPadding: 6
                                                    padding: 2
                                                    validator: RegExpValidator {regExp: /[\d-]+/}
                                                    background: Rectangle {

                                                        color: "#797979"
                                                        opacity: 0.3

                                                    }

                                                    onEditingFinished :
                                                    {
                                                        sessionController.displayWorkarea.setYStartPosition(parseInt(yStartPos.text))
                                                    }
                                                }
                                            }

                                            Item {
                                                Layout.fillWidth: true
                                                Layout.fillHeight: true
                                            }
                                        }
                                    }

                                    Text {
                                        id: fullResText
                                        text: qsTr("Resolution of content window")
                                        Layout.bottomMargin: 0
                                        font.pointSize: 10
                                        font.weight: Font.ExtraLight
                                    }

                                    Item {
                                        Layout.fillWidth: true
                                        Layout.fillHeight: true

                                        RowLayout {
                                            Layout.fillWidth: true
                                            anchors.fill: parent

                                            Item {
                                                Layout.fillWidth: true
                                                Layout.fillHeight: true

                                                TextField {
                                                    id: widthFullRes
                                                    height: 20
                                                    width: parent.width
                                                    font.pointSize: 10
                                                    font.weight: Font.ExtraLight
                                                    anchors.verticalCenter: parent.verticalCenter
                                                    text: sessionController.displayWorkarea.contentWindowResolution.width
                                                    horizontalAlignment: Text.AlignHCenter
                                                    verticalAlignment: Text.AlignVCenter
                                                    rightPadding: 6
                                                    leftPadding: 6
                                                    padding: 2
                                                    validator: RegExpValidator {regExp: /[\d-]+/}
                                                    background: Rectangle {

                                                        color: "#797979"
                                                        opacity: 0.3

                                                    }

                                                    onEditingFinished :
                                                    {
                                                        sessionController.displayWorkarea.setWidthContentWindowRes(parseInt(widthFullRes.text))
                                                    }
                                                }
                                            }

                                            Item {
                                                Layout.fillWidth: true
                                                Layout.fillHeight: true

                                                Layout.maximumWidth: 20

                                                Text {
                                                    text: qsTr("x")
                                                    height: 20
                                                    width: 10
                                                    font.pointSize: 10
                                                    font.weight: Font.ExtraLight
                                                    anchors.horizontalCenter: parent.horizontalCenter
                                                    anchors.verticalCenter: parent.verticalCenter
                                                }
                                            }

                                            Item {
                                                Layout.fillWidth: true
                                                Layout.fillHeight: true

                                                TextField {
                                                    id: heightFullRes
                                                    height: 20
                                                    width: parent.width
                                                    font.pointSize: 10
                                                    font.weight: Font.ExtraLight
                                                    anchors.verticalCenter: parent.verticalCenter
                                                    text: sessionController.displayWorkarea.contentWindowResolution.height
                                                    horizontalAlignment: Text.AlignHCenter
                                                    verticalAlignment: Text.AlignVCenter
                                                    rightPadding: 6
                                                    leftPadding: 6
                                                    padding: 2
                                                    validator: RegExpValidator {regExp: /[\d-]+/}
                                                    background: Rectangle {

                                                        color: "#797979"
                                                        opacity: 0.3

                                                    }

                                                    onEditingFinished :
                                                    {
                                                        sessionController.displayWorkarea.setHeightContentWindowRes(parseInt(heightFullRes.text))
                                                    }
                                                }
                                            }

                                            Item {
                                                Layout.fillWidth: true
                                                Layout.fillHeight: true
                                            }
                                        }
                                    }
                                }
                            }

                            Item {
                                id: actionTextItem
                                Layout.minimumHeight: 15
                                Layout.maximumHeight: 15
                                Layout.fillWidth: true
                                //Layout.preferredWidth: 50
                                Layout.alignment: Qt.AlignLeft & Qt.AlignTop

                                Text {
                                    id: actionText
                                    text: qsTr("Display's actions")
                                    //text: displayRepeater.itemAt(displayRepeater.display).myData.resolution.width
                                    //text: displayRepeater.itemAt(displayRepeater.display).myData
                                    //text: displaysWorkarea.model.resolutionList(displayRepeater.display).width
                                    Layout.bottomMargin: 0
                                    font.pointSize: 10
                                    font.weight: Font.ExtraLight
                                    Layout.fillWidth: true
                                    //Layout.preferredWidth: 50
                                    //Layout.minimumHeight: 15
                                    //Layout.maximumHeight: 15
                                    //anchors.left: parent.left
                                    //anchors.top: parent.top
                                    //Layout.alignment: Qt.AlignLeft & Qt.AlignTop

                                    //Layout.row: 0
                                    //Layout.column: 0

                                    //anchors.fill: parent
                                }
                            }

                            Item {
                                id: resolutionItem
                                //Layout.row: 1
                                //Layout.column: 0
                                Layout.topMargin: 5

                                Layout.fillWidth: true
                                height: 50
                                Layout.maximumHeight: 50
                                Layout.alignment: Qt.AlignTop
                                //anchors.left: actionTextItem.left
                                //anchors.top: actionTextItem.bottom

                                //Layout.alignment: Qt.AlignLeft & Qt.AlignTop
                                //Layout.anchors.top: actionText.bottom

                                RowLayout {
                                    id: columnResRow
                                    //rows: 1
                                    //columns: 3
                                    //anchors.fill: parent.width
                                    //Layout.fillWidth: true
                                    //Layout.fillHeight: true
                                    anchors.fill: parent // ????????????????????????????
                                    anchors.margins: 5
                                    //anchors.horizontalCenter: parent.horizontalCenter


                                    Item {
                                        id: columnRes1
                                        //width: 10
                                        //Layout.maximumWidth: 50
                                        //Layout.row: 0
                                        //Layout.column: 0
                                        //Layout.maximumWidth: 10
                                        Layout.minimumWidth: 10
                                        Layout.fillHeight: true
                                        Layout.alignment: Qt.AlignLeft
                                        //anchors.right: columnRes2.left
                                        //anchors.top: columnResGrid.top
                                    }

                                    Item {
                                        id: columnRes2

                                        Layout.fillWidth: true
                                        Layout.fillHeight: true
                                        //Layout.row: 0
                                        //Layout.column: 1
                                        //height: parent.height
                                        //Layout.horizontalCenter: parent.horizontalCenter

                                        ColumnLayout {
                                            id: resolutionColumnLayout
                                            Layout.fillHeight: true
                                            Layout.fillWidth: true

                                            property variant resolutionList

                                            Text {
                                                text: qsTr("Resolutions")
                                                Layout.preferredWidth: 70
                                                Layout.minimumWidth: 70
                                                Layout.maximumHeight: 20
                                                Layout.alignment: Qt.AlignTop
                                                //anchors.horizontalCenter: parent.horizontalCenter
                                                //anchors.top: parent.top
                                                //anchors.left: control.left
                                                //anchors.bottom: control.top
                                                //anchors.bottomMargin: 3
                                                //horizontalAlignment: Text.AlignLeft
                                                font.pointSize: 10
                                            }

                                            ComboBox {
                                                //id: resolutionComboBox
                                                id: control
                                                property alias comboboxModel: control.model
                                                //model: displaysController.resolutionList()
                                                //model: resolutionColumnLayout.resolutionList
                                                //model: sessionController.displaysController.resolutionList()
                                                //model: sessionController.displaysController.displayWorkarea.model.resolutionList(displayRepeater.display)
                                                //model: sessionController.displaysController.resolutionList() currentDisplayResolutionList
                                                //model: sessionController.displaysController.currentDisplayResolutionList
                                                model: sessionController.currentDisplayResolutionList
                                                textRole: 'resolutionStr'
                                                enabled: displayRepeater.display != -1 ? true : false
                                                //Layout.fillWidth: true
                                                height: 15
                                                focusPolicy: Qt.WheelFocus
                                                font.pointSize: 10
                                                Layout.alignment: Qt.AlignTop
                                                //anchors.horizontalCenter: parent.horizontalCenter
                                                //currentIndex: 0

                                                delegate: ItemDelegate {
                                                    id: itmCombobox
                                                    width: control.width
                                                    highlighted: control.highlightedIndex === index
                                                    //hoverEnabled: control.hoverEnabled

                                                    contentItem: Text {
                                                            id: itemText
                                                            text: control.textRole ? (Array.isArray(control.model) ? modelData[control.textRole] : model[control.textRole]) : modelData
                                                            font.weight: control.currentIndex === index ? Font.Bold : Font.Normal
                                                            font.pointSize: 10
                                                            //font.pointSize: control.font
                                                            //width: itmCombobox.width
                                                            //elide: Text.ElideRight
                                                            //verticalAlignment: Text.AlignVCenter
                                                            //horizontalAlignment: Text.AlignLeft
                                                        }
                                                    background: Rectangle {
                                                        anchors.left: itmCombobox.left
                                                        width: itmCombobox.width
                                                        //color: "transparent"
                                                        color: itmCombobox.hovered ? "#797979" : "transparent";
                                                        border.color: "black"
                                                        border.width: itmCombobox.hovered ? 1 : 0;
                                                    }
                                                }

                                                indicator: Canvas {
                                                    id: canvas
                                                    x: control.width - width - control.rightPadding
                                                    y: control.topPadding + (control.availableHeight - height) / 2
                                                    width: 12
                                                    height: 8
                                                    contextType: "2d"

                                                    Connections {
                                                        target: control
                                                        onPressedChanged: canvas.requestPaint()
                                                    }

                                                    onPaint: {
                                                        context.reset();
                                                        context.moveTo(0, 0);
                                                        context.lineTo(width, 0);
                                                        context.lineTo(width / 2, height);
                                                        context.closePath();
                                                        context.fillStyle = control.pressed ? "black" : "#797979";
                                                        context.fill();
                                                    }
                                                }

                                                background: Rectangle {
                                                    implicitWidth: 140
                                                    implicitHeight: 20
                                                    //color: "red"
                                                    color: control.down ? control.palette.mid : control.palette.button
                                                    border.color: "black" //control.palette.highlight
                                                    border.width: !control.editable && control.visualFocus ? 3 : 1
                                                    visible: !control.flat || control.down
                                                }

                                                contentItem: TextField {
                                                    leftPadding: !control.mirrored ? 12 : control.editable && activeFocus ? 3 : 1
                                                    rightPadding: control.mirrored ? 12 : control.editable && activeFocus ? 3 : 1
                                                    topPadding: 6 - control.padding
                                                    bottomPadding: 6 - control.padding

                                                    text: control.editable ? control.editText : control.displayText

                                                    enabled: control.editable
                                                    autoScroll: control.editable
                                                    readOnly: control.down
                                                    inputMethodHints: control.inputMethodHints
                                                    validator: control.validator

                                                    font: control.font
                                                    color: control.editable ? control.palette.text : control.palette.buttonText
                                                    selectionColor: control.palette.highlight
                                                    selectedTextColor: control.palette.highlightedText
                                                    verticalAlignment: Text.AlignVCenter

                                                    background: Rectangle {
                                                        visible: control.enabled && control.editable && !control.flat
                                                        border.width: parent && parent.activeFocus ? 2 : 1
                                                        border.color: parent && parent.activeFocus ? control.palette.highlight : control.palette.button
                                                        color: control.palette.base
                                                        //color: "red"
                                                    }
                                                }

                                                popup: Popup {
                                                    y: control.height
                                                    width: control.width
                                                    height: Math.min(contentItem.implicitHeight, control.Window.height - topMargin - bottomMargin)
                                                    topMargin: 6
                                                    bottomMargin: 6

                                                    contentItem: ListView {
                                                        id: popupList

                                                        clip: true
                                                        implicitHeight: contentHeight
                                                        anchors.fill: parent
                                                        model: control.delegateModel
                                                        currentIndex: control.highlightedIndex
                                                        highlightMoveDuration: 0

                                                        Rectangle {
                                                            id: popupRect
                                                            width: control.width
                                                            height: control.height
                                                            color: "transparent"
                                                            //border.color: "green"
                                                        }
                                                    }

//                                                    background: Rectangle {
//                                                        id: popupBackground
//                                                        //color: "red"
//                                                        //color: control.hovered ? "red" : control.palette.button
//                                                        color: control.palette.button
//                                                        //border.width: control.activeFocus ? 2 : 1
//                                                        //border.color: control.hovered? "red" : "blue"
//                                                    }
                                                }
                                            }
                                        }

                                        Connections {
                                            target: sessionController
                                            //target: sessionController.displaysController // editorPlaylist.currentDisplayWorkarea
                                            onCurrentDisplayChanged: {
                                                console.log("CurrentDisplayChanged") // sessionController.displaysController.displayWorkarea
                                                //resolutionColumnLayout.resolutionList = editorPlaylist.currentDisplayWorkarea.resolutionList()
                                                //resolutionColumnLayout.resolutionList = sessionController.displaysController.resolutionList()
                                                //control.comboboxModel = sessionController.displaysController.resolutionList()
                                                //control.comboboxModel = sessionController.displaysController.currentDisplayResolutionList
                                            }

                                            onCurrentDisplayResolutionListChanged: {
                                                console.log("CurrentDisplayResolutionListChanged")
                                                //control.comboboxModel = sessionController.displaysController.currentDisplayResolutionList
                                            }
                                        }
                                    }

                                    Item {
                                        id: columnRes3
                                        //Layout.maximumWidth: 10
                                        Layout.minimumWidth: 10
                                        Layout.fillHeight: true
                                        Layout.alignment: Qt.AlignLeft
                                        //Layout.row: 0
                                        //Layout.column: 2
                                        //anchors.left: columnRes2.rigth
                                        //anchors.top: columnResGrid.top
                                    }

                                }

                            }

                            Item {
                                id: overlapScreenItem
                                Layout.topMargin: 10
                                //Layout.rowSpan: 2
                                //anchors.fill: parent
                                Layout.fillWidth: true
                                Layout.preferredHeight: 190
                                Layout.alignment: Qt.AlignTop | Qt.AlignLeft
                                //anchors.verticalCenter: parent.verticalCenter
                                //.top: horizontalResolutionValue.bottom
                                //anchors.left: horizontalResolutionValue.left
                                //anchors.left: resolutionItem.left
                                //anchors.top: resolutionItem.bottom

                                //Layout.alignment: Qt.AlignLeft & Qt.AlignBottom

                                //Layout.row: 2
                                //Layout.column: 0

                                //Layout.row: 2

                                GridLayout {
                                    id: offsetButtons
                                    rows: 5
                                    columns: 5
                                    columnSpacing: 5
                                    rowSpacing: 5
                                    anchors.fill: parent


                                    property int wantedWidth: 50
                                    property int wantedHeight: 40
                                    //anchors.horizontalCenter: columnVerOffset.horizontalCenter

                                    Item {
                                        Layout.row: 0
                                        Layout.column: 0
                                        Layout.fillWidth: true
                                        Layout.fillHeight: true
//                                            Layout.maximumHeight: offsetButtons.wantedHeight
//                                            Layout.maximumWidth: offsetButtons.wantedWidth
//                                            Layout.minimumHeight: offsetButtons.wantedHeight
//                                            Layout.minimumWidth: offsetButtons.wantedWidth

                                        //Layout.alignment: Qt.AlignLeft
                                        Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                                    }

                                    Item {
                                        Layout.row: 0
                                        Layout.column: 1
                                        Layout.maximumHeight: offsetButtons.wantedHeight
                                        Layout.maximumWidth: offsetButtons.wantedWidth
                                        Layout.minimumHeight: offsetButtons.wantedHeight
                                        Layout.minimumWidth: offsetButtons.wantedWidth
                                        //Layout.alignment: Qt.AlignLeft
                                        Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                                    }

                                    Item {
                                        Layout.row: 0
                                        Layout.column: 2
                                        Layout.maximumHeight: offsetButtons.wantedHeight
                                        Layout.maximumWidth: offsetButtons.wantedWidth
                                        Layout.minimumHeight: offsetButtons.wantedHeight
                                        Layout.minimumWidth: offsetButtons.wantedWidth
                                        Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                                        //horizontalCenter: parent.horizontalCenter

                                        TextField {
                                            id: topOffsetValue
                                            anchors.bottom: parent.bottom
                                            height: 20
                                            width: offsetButtons.wantedWidth
                                            //Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                                            enabled: displayRepeater.display != -1 ? true : false
                                            text: qsTr("10")
                                            rightPadding: 6
                                            leftPadding: 6
                                            padding: 2
                                            validator: RegExpValidator {regExp: /[\d-]+/}
                                            //Layout.maximumHeight: 20
                                            //Layout.maximumWidth: 60
                                            //Layout.minimumHeight: 20
                                            //Layout.minimumWidth: 60

                                            //Layout.alignment: Qt.AlignHCenter | Qt.AlignBottom
                                            //anchors.horizontalCenter: parent.horizontalCenter
                                            //anchors.bottom: topOffsetButton.top
                                            //anchors.left: topOffsetButton.left
                                            horizontalAlignment: Text.AlignHCenter
                                            verticalAlignment: Text.AlignVCenter
                                            font.pointSize: 10
                                            background: Rectangle {

                                                color: "#797979"
                                                opacity: 0.3

                                            }

                                            onEditingFinished: {

                                                //console.log(displayRepeater.itemAt(displayRepeater.display).myData.position.top - topOffsetValue.text)
                                                //console.log(parseInt(parseInt(displayRepeater.itemAt(displayRepeater.display).myData.position.top) - parseInt(topOffsetValue.text)))
                                                //sessionController.displaysController.displayWorkarea.model.changePositionToTop(displayRepeater.display, parseInt(parseInt(displayRepeater.itemAt(displayRepeater.display).myData.position.top) - parseInt(topOffsetValue.text)))
                                                sessionController.displayWorkarea.model.changePositionToTop(displayRepeater.display, parseInt(parseInt(displayRepeater.itemAt(displayRepeater.display).myData.position.top) - parseInt(topOffsetValue.text)))
                                                bottomOffsetValue.text = displayRepeater.itemAt(displayRepeater.display).myData.position.bottom

                                                control.textRole = 'resolutionStr' //= displaysWorkarea.model.resolutionList(displayRepeater.display)
                                            }

    //                                        Keys.onEnterPressed: {
    //                                            console.log(displayRepeater.itemAt(displayRepeater.display).myData.position.top - topOffsetValue.text)

    //                                        }
                                        }
                                    }

                                    Item {
                                        Layout.row: 0
                                        Layout.column: 3
                                        Layout.maximumHeight: offsetButtons.wantedHeight
                                        Layout.maximumWidth: offsetButtons.wantedWidth
                                        Layout.minimumHeight: offsetButtons.wantedHeight
                                        Layout.minimumWidth: offsetButtons.wantedWidth
                                        Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                                    }

                                    Item {
                                        Layout.row: 0
                                        Layout.column: 4
                                        Layout.fillWidth: true
                                        Layout.fillHeight: true
//                                            Layout.maximumHeight: offsetButtons.wantedHeight
//                                            Layout.maximumWidth: offsetButtons.wantedWidth
//                                            Layout.minimumHeight: offsetButtons.wantedHeight
//                                            Layout.minimumWidth: offsetButtons.wantedWidth
                                        //Layout.alignment: Qt.AlignLeft
                                        Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                                    }

                                    Item {
                                        Layout.row: 1
                                        Layout.column: 0
                                        Layout.maximumHeight: offsetButtons.wantedHeight
                                        Layout.maximumWidth: offsetButtons.wantedWidth
                                        Layout.minimumHeight: offsetButtons.wantedHeight
                                        Layout.minimumWidth: offsetButtons.wantedWidth
                                        Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                                    }

                                    Item {
                                        Layout.row: 1
                                        Layout.column: 1
                                        Layout.maximumHeight: offsetButtons.wantedHeight
                                        Layout.maximumWidth: offsetButtons.wantedWidth
                                        Layout.minimumHeight: offsetButtons.wantedHeight
                                        Layout.minimumWidth: offsetButtons.wantedWidth
                                        Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                                    }

                                    Item {
                                        Layout.row: 1
                                        Layout.column: 2
                                        Layout.maximumHeight: offsetButtons.wantedHeight
                                        Layout.maximumWidth: offsetButtons.wantedWidth
                                        Layout.minimumHeight: offsetButtons.wantedHeight
                                        Layout.minimumWidth: offsetButtons.wantedWidth
                                        Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter

                                        Button {
                                            id: topOffsetButton
                                            anchors.fill: parent
                                            enabled: displayRepeater.display != -1 ? true : false
                                            //Layout.row: 1
                                            //Layout.column: 2
                                            text: qsTr("TO")
//                                                Layout.maximumHeight: 40
//                                                Layout.maximumWidth: 40
//                                                Layout.minimumHeight: 40
//                                                Layout.minimumWidth: 40

                                            onClicked: {
                                                //sessionController.displaysController.displayWorkarea.model.changePositionToTop(displayRepeater.display, 1)
                                                sessionController.displayWorkarea.model.changePositionToTop(displayRepeater.display, 1)
                                                topOffsetValue.text = displayRepeater.itemAt(displayRepeater.display).myData.position.top
                                                bottomOffsetValue.text = displayRepeater.itemAt(displayRepeater.display).myData.position.bottom

                                                //console.log("change top")
                                                //displaysController.changePositionToTop(10)
                                                //console.log("top of current display " + displaysController.topPosition)
                                                //console.log("top of current display " + editorPlaylist.currentDisplayWorkarea.model.get(displayRepeater.display).position.top)

                                                control.textRole = 'resolutionStr'
                                            }
                                        }
                                    }

                                    Item {
                                        Layout.row: 1
                                        Layout.column: 3
                                        Layout.maximumHeight: offsetButtons.wantedHeight
                                        Layout.maximumWidth: offsetButtons.wantedWidth
                                        Layout.minimumHeight: offsetButtons.wantedHeight
                                        Layout.minimumWidth: offsetButtons.wantedWidth
                                        Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                                    }

                                    Item {
                                        Layout.row: 1
                                        Layout.column: 4
                                        Layout.maximumHeight: offsetButtons.wantedHeight
                                        Layout.maximumWidth: offsetButtons.wantedWidth
                                        Layout.minimumHeight: offsetButtons.wantedHeight
                                        Layout.minimumWidth: offsetButtons.wantedWidth
                                        Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                                    }

                                    Item {
                                        Layout.row: 2
                                        Layout.column: 0
                                        Layout.maximumHeight: offsetButtons.wantedHeight
                                        Layout.maximumWidth: offsetButtons.wantedWidth
                                        Layout.minimumHeight: offsetButtons.wantedHeight
                                        Layout.minimumWidth: offsetButtons.wantedWidth
                                        Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter

                                        TextField {
                                            //Layout.row: 2
                                            //Layout.column: 0
                                            id: leftOffsetValue
                                            width: offsetButtons.wantedWidth
                                            height: 20
                                            anchors.verticalCenter: parent.verticalCenter
                                            enabled: displayRepeater.display != -1 ? true : false
                                            //Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                                            //topPadding: centerOffsetGrid.verticalCenter
                                            text: qsTr("5")
                                            rightPadding: 6
                                            leftPadding: 6
                                            padding: 2
                                            validator: RegExpValidator {regExp: /[\d-]+/}
                                            //Layout.maximumHeight: 20
                                            //Layout.maximumWidth: 60
                                            //Layout.minimumHeight: 20
                                            //Layout.minimumWidth: 60
                                            horizontalAlignment: Text.AlignHCenter
                                            verticalAlignment: Text.AlignVCenter
                                            font.pointSize: 10
                                            background: Rectangle {

                                                color: "#797979"
                                                opacity: 0.3

                                            }

                                            onEditingFinished: {
                                                //sessionController.displaysController.displayWorkarea.model.changePositionToLeft(displayRepeater.display , displayRepeater.itemAt(displayRepeater.display).myData.position.left - parseInt(leftOffsetValue.text))
                                                sessionController.displayWorkarea.model.changePositionToLeft(displayRepeater.display , displayRepeater.itemAt(displayRepeater.display).myData.position.left - parseInt(leftOffsetValue.text))
                                                rightOffsetValue.text = displayRepeater.itemAt(displayRepeater.display).myData.position.right

                                                control.textRole = 'resolutionStr'
                                            }
                                        }
                                    }

                                    Item {
                                        Layout.row: 2
                                        Layout.column: 1
                                        Layout.maximumHeight: offsetButtons.wantedHeight
                                        Layout.maximumWidth: offsetButtons.wantedWidth
                                        Layout.minimumHeight: offsetButtons.wantedHeight
                                        Layout.minimumWidth: offsetButtons.wantedWidth
                                        Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter

                                        Button {
                                            id: leftOffsetButton
                                            anchors.fill: parent
                                            enabled: displayRepeater.display != -1 ? true : false
                                            text: qsTr("LO")
                                            //Layout.maximumHeight: 40
                                            //Layout.maximumWidth: 40
                                            //Layout.minimumHeight: 40
                                            //Layout.minimumWidth: 40

                                            onClicked: {
                                                //sessionController.displaysController.displayWorkarea.model.changePositionToLeft(displayRepeater.display, 1)
                                                sessionController.displayWorkarea.model.changePositionToLeft(displayRepeater.display, 1)
                                                leftOffsetValue.text = displayRepeater.itemAt(displayRepeater.display).myData.position.left
                                                rightOffsetValue.text = displayRepeater.itemAt(displayRepeater.display).myData.position.right

                                                control.textRole = 'resolutionStr'
                                            }
                                        }
                                    }

                                    Item {
                                        id: centerOffsetGrid
                                        Layout.row: 2
                                        Layout.column: 2
                                        Layout.maximumHeight: offsetButtons.wantedHeight
                                        Layout.maximumWidth: offsetButtons.wantedWidth
                                        Layout.minimumHeight: offsetButtons.wantedHeight
                                        Layout.minimumWidth: offsetButtons.wantedWidth
                                        Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                                        //Layout.horizontalCenter: parent.horizontalCenter
                                        //Layout.verticalCenter: parent.verticalCenter
                                    }

                                    Item {
                                        Layout.row: 2
                                        Layout.column: 3
                                        Layout.maximumHeight: offsetButtons.wantedHeight
                                        Layout.maximumWidth: offsetButtons.wantedWidth
                                        Layout.minimumHeight: offsetButtons.wantedHeight
                                        Layout.minimumWidth: offsetButtons.wantedWidth
                                        Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter

                                        Button {
                                            id: rightOffsetButton
                                            //Layout.row: 2
                                            //Layout.column: 3
                                            anchors.fill: parent
                                            text: qsTr("RO")
                                            enabled: displayRepeater.display != -1 ? true : false
                                            //Layout.maximumHeight: 40
                                            //Layout.maximumWidth: 40
                                            //Layout.minimumHeight: 40
                                            //Layout.minimumWidth: 40

                                            onClicked: {
                                                //sessionController.displaysController.displayWorkarea.model.changePositionToRight(displayRepeater.display, 1)
                                                sessionController.displayWorkarea.model.changePositionToRight(displayRepeater.display, 1)
                                                leftOffsetValue.text = displayRepeater.itemAt(displayRepeater.display).myData.position.left
                                                rightOffsetValue.text = displayRepeater.itemAt(displayRepeater.display).myData.position.right

                                                control.textRole = 'resolutionStr'
                                            }
                                        }
                                    }

                                    Item {
                                        Layout.row: 2
                                        Layout.column: 4
                                        Layout.maximumHeight: offsetButtons.wantedHeight
                                        Layout.maximumWidth: offsetButtons.wantedWidth
                                        Layout.minimumHeight: offsetButtons.wantedHeight
                                        Layout.minimumWidth: offsetButtons.wantedWidth
                                        Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter

                                        TextField {
                                            //Layout.row: 2
                                            //Layout.column: 4
                                            id: rightOffsetValue
                                            width: offsetButtons.wantedWidth
                                            height: 20
                                            anchors.verticalCenter: parent.verticalCenter
                                            enabled: displayRepeater.display != -1 ? true : false
                                            //Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                                            //topPadding: centerOffsetGrid.verticalCenter
                                            text: qsTr("5")
                                            rightPadding: 6
                                            leftPadding: 6
                                            padding: 2
                                            validator: RegExpValidator {regExp: /[\d-]+/}
                                            //Layout.maximumHeight: 20
                                            //Layout.maximumWidth: 60
                                            //Layout.minimumHeight: 20
                                            //Layout.minimumWidth: 60
                                            horizontalAlignment: Text.AlignHCenter
                                            verticalAlignment: Text.AlignVCenter
                                            font.pointSize: 10
                                            background: Rectangle {

                                                color: "#797979"
                                                opacity: 0.3

                                            }

                                            onEditingFinished: {
                                                //sessionController.displaysController.displayWorkarea.model.changePositionToRight(displayRepeater.display , displayRepeater.itemAt(displayRepeater.display).myData.position.right - parseInt(rightOffsetValue.text))
                                                sessionController.displayWorkarea.model.changePositionToRight(displayRepeater.display , displayRepeater.itemAt(displayRepeater.display).myData.position.right - parseInt(rightOffsetValue.text))
                                                leftOffsetValue.text = displayRepeater.itemAt(displayRepeater.display).myData.position.left

                                                control.textRole = 'resolutionStr'
                                            }
                                        }
                                    }

                                    Item {
                                        Layout.row: 3
                                        Layout.column: 0
                                        Layout.maximumHeight: offsetButtons.wantedHeight
                                        Layout.maximumWidth: offsetButtons.wantedWidth
                                        Layout.minimumHeight: offsetButtons.wantedHeight
                                        Layout.minimumWidth: offsetButtons.wantedWidth
                                        Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                                    }

                                    Item {
                                        Layout.row: 3
                                        Layout.column: 1
                                        Layout.maximumHeight: offsetButtons.wantedHeight
                                        Layout.maximumWidth: offsetButtons.wantedWidth
                                        Layout.minimumHeight: offsetButtons.wantedHeight
                                        Layout.minimumWidth: offsetButtons.wantedWidth
                                        Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                                    }

                                    Item {
                                        Layout.row: 3
                                        Layout.column: 2
                                        Layout.maximumHeight: offsetButtons.wantedHeight
                                        Layout.maximumWidth: offsetButtons.wantedWidth
                                        Layout.minimumHeight: offsetButtons.wantedHeight
                                        Layout.minimumWidth: offsetButtons.wantedWidth
                                        Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter

                                        Button {
                                            id: bottomOffsetButton
                                            //Layout.row: 3
                                            //Layout.column: 2
                                            anchors.fill: parent
                                            text: qsTr("BO")
                                            enabled: displayRepeater.display != -1 ? true : false
                                            //Layout.maximumHeight: 40
                                            //Layout.maximumWidth: 40
                                            //Layout.minimumHeight: 40
                                            //Layout.minimumWidth: 40

                                            onClicked: {
                                                //sessionController.displaysController.displayWorkarea.model.changePositionToBottom(displayRepeater.display, 1)
                                                sessionController.displayWorkarea.model.changePositionToBottom(displayRepeater.display, 1)
                                                bottomOffsetValue.text = displayRepeater.itemAt(displayRepeater.display).myData.position.bottom
                                                topOffsetValue.text = displayRepeater.itemAt(displayRepeater.display).myData.position.top

                                                control.textRole = 'resolutionStr'
                                            }
                                        }
                                    }

                                    Item {
                                        Layout.row: 3
                                        Layout.column: 3
                                        Layout.maximumHeight: offsetButtons.wantedHeight
                                        Layout.maximumWidth: offsetButtons.wantedWidth
                                        Layout.minimumHeight: offsetButtons.wantedHeight
                                        Layout.minimumWidth: offsetButtons.wantedWidth
                                        Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                                    }

                                    Item {
                                        Layout.row: 3
                                        Layout.column: 4
                                        Layout.maximumHeight: offsetButtons.wantedHeight
                                        Layout.maximumWidth: offsetButtons.wantedWidth
                                        Layout.minimumHeight: offsetButtons.wantedHeight
                                        Layout.minimumWidth: offsetButtons.wantedWidth
                                        Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                                    }

                                    Item {
                                        Layout.row: 4
                                        Layout.column: 0
                                        Layout.fillWidth: true
                                        Layout.fillHeight: true
//                                            Layout.maximumHeight: offsetButtons.wantedHeight
//                                            Layout.maximumWidth: offsetButtons.wantedWidth
//                                            Layout.minimumHeight: offsetButtons.wantedHeight
//                                            Layout.minimumWidth: offsetButtons.wantedWidth
                                        Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                                    }

                                    Item {
                                        Layout.row: 4
                                        Layout.column: 1
                                        Layout.maximumHeight: offsetButtons.wantedHeight
                                        Layout.maximumWidth: offsetButtons.wantedWidth
                                        Layout.minimumHeight: offsetButtons.wantedHeight
                                        Layout.minimumWidth: offsetButtons.wantedWidth
                                        Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                                    }

                                    Item {
                                        Layout.row: 4
                                        Layout.column: 2
                                        Layout.maximumHeight: offsetButtons.wantedHeight
                                        Layout.maximumWidth: offsetButtons.wantedWidth
                                        Layout.minimumHeight: offsetButtons.wantedHeight
                                        Layout.minimumWidth: offsetButtons.wantedWidth
                                        Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter

                                        TextField {
                                            id: bottomOffsetValue
                                            height: 20
                                            width: offsetButtons.wantedWidth
                                            enabled: displayRepeater.display != -1 ? true : false
                                            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                                            anchors.top: parent.top
                                            text: qsTr("7")
                                            rightPadding: 6
                                            leftPadding: 6
                                            padding: 2
                                            validator: RegExpValidator {regExp: /[\d-]+/}
                                            //Layout.maximumHeight: 20
                                            //Layout.maximumWidth: 60
                                            //Layout.minimumHeight: 20
                                            //Layout.minimumWidth: 60
                                            horizontalAlignment: Text.AlignHCenter
                                            verticalAlignment: Text.AlignVCenter
                                            font.pointSize: 10
                                            background: Rectangle {

                                                color: "#797979"
                                                opacity: 0.3

                                            }

                                            onEditingFinished: {
                                                //sessionController.displaysController.displayWorkarea.model.changePositionToBottom(displayRepeater.display, displayRepeater.itemAt(displayRepeater.display).myData.position.bottom - parseInt(bottomOffsetValue.text))
                                                sessionController.displayWorkarea.model.changePositionToBottom(displayRepeater.display, displayRepeater.itemAt(displayRepeater.display).myData.position.bottom - parseInt(bottomOffsetValue.text))
                                                topOffsetValue.text = displayRepeater.itemAt(displayRepeater.display).myData.position.top

                                                control.textRole = 'resolutionStr'
                                            }
                                        }
                                    }

                                    Item {
                                        Layout.row: 4
                                        Layout.column: 3
                                        Layout.maximumHeight: offsetButtons.wantedHeight
                                        Layout.maximumWidth: offsetButtons.wantedWidth
                                        Layout.minimumHeight: offsetButtons.wantedHeight
                                        Layout.minimumWidth: offsetButtons.wantedWidth
                                        Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                                    }

                                    Item {
                                        Layout.row: 4
                                        Layout.column: 4
                                        Layout.fillWidth: true
                                        Layout.fillHeight: true
//                                            Layout.maximumHeight: offsetButtons.wantedHeight
//                                            Layout.maximumWidth: offsetButtons.wantedWidth
//                                            Layout.minimumHeight: offsetButtons.wantedHeight
//                                            Layout.minimumWidth: offsetButtons.wantedWidth
                                        Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                                    }
                                }
                            }
                        }
                    }
                }

                Item {
                    //color: "black"
                    id: changeBlendingItem
                    Layout.fillHeight: true
                    Layout.fillWidth: true
                    Layout.row: 1
                    Layout.column: 1
                    //Layout.column: 2

                    Frame {
                        id: changeBlendingFrame
                        anchors.fill: parent

                        ColumnLayout {
                            id: changeBlendingColumnLayout
                            anchors.fill: parent

                            Text {
                                id: changeBlendText
                                Layout.bottomMargin: 0
                                font.pointSize: 10
                                font.weight: Font.ExtraLight
                                Layout.fillWidth: true
                                Layout.preferredWidth: 50
                                Layout.alignment: Qt.AlignLeft & Qt.AlignTop
                                //anchors.left: parent.left
                                //anchors.top: parent.top
                                text: qsTr("Blend")
                            }

                            Item {
                                id: blendingItem
                                //anchors.top: changeDisplaysText.bottom
                                //anchors.left: changeDisplaysText.left
                                Layout.fillWidth: true
                                Layout.fillHeight: true
                                Layout.alignment: Qt.AlignLeft & Qt.AlignTop
                                //Layout.height: 220
                                //height: 380


                                GridLayout {
                                    id: blendingGrid
                                    rows: 3
                                    columns: 3
                                    height: 120*3+3*5
                                    width: 150*3+3*5
                                    anchors.horizontalCenter: parent.horizontalCenter
                                    anchors.verticalCenter: parent.verticalCenter
                                    //columnSpacing: 5
                                    //rowSpacing: 5
                                    //anchors.horizontalCenter: changeBlendingColumnLayout.horizontalCenter
                                    //anchors.fill: parent
                                    //anchors.top: changeBlendText.bottom

                                    Item {
                                        Layout.row: 0
                                        Layout.column: 0
                                        Layout.maximumHeight: 120
                                        Layout.maximumWidth: 150
                                        Layout.minimumHeight: 120
                                        Layout.minimumWidth: 150
                                    }

                                    Item {
                                        Layout.row: 0
                                        Layout.column: 1
                                        height: 120
                                        width: 150
                                        //anchors.bottom: centerBlendCell.top
                                        //anchors.left: centerBlendCell.left

                                        ColumnLayout {
                                            anchors.fill: parent
                                            spacing: 1

                                            Text {
                                                id: topBlendSize
                                                text: qsTr("Height of darkening region")
                                                //anchors.top: parent.top
                                            }

                                            TextField {
                                                id: topBlending
                                                height: 20
                                                width: 60
                                                enabled: displayRepeater.display != -1 ? true : false
                                                Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                                                text: qsTr("0")
                                                rightPadding: 6
                                                leftPadding: 6
                                                padding: 2
                                                Layout.maximumHeight: 20
                                                Layout.maximumWidth: 60
                                                Layout.minimumHeight: 20
                                                Layout.minimumWidth: 60
                                                //anchors.top: topBlendSize.bottom
                                                horizontalAlignment: Text.AlignHCenter
                                                font.pointSize: 10
                                                validator: RegExpValidator {regExp: /[\d-]+/}
                                                background: Rectangle {

                                                    color: "#797979"
                                                    opacity: 0.3

                                                }

                                                onEditingFinished: {
                                                    //displaysWorkarea.model.changePositionToBottom(displayRepeater.display, displayRepeater.itemAt(displayRepeater.display).myData.position.bottom - parseInt(bottomOffsetValue.text))
                                                    //sessionController.displaysController.displayWorkarea.model.changeTopBlending(displayRepeater.display, parseInt(topBlending.text))
                                                    sessionController.displayWorkarea.model.changeTopBlending(displayRepeater.display, parseInt(topBlending.text))
                                                }

                                            }

                                            Text {
                                                id: topBlendOpacity
                                                text: qsTr("Opacity of darkening region")
                                                //anchors.top: topBlending.bottom
                                            }

                                            TextField {
                                                id: topOpacity
                                                height: 20
                                                width: 60
                                                enabled: displayRepeater.display != -1 ? true : false
                                                Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                                                text: qsTr("0")
                                                rightPadding: 6
                                                leftPadding: 6
                                                padding: 2
                                                Layout.maximumHeight: 20
                                                Layout.maximumWidth: 60
                                                Layout.minimumHeight: 20
                                                Layout.minimumWidth: 60
                                                //anchors.top: topBlendOpacity.bottom
                                                horizontalAlignment: Text.AlignHCenter
                                                font.pointSize: 10
                                                validator: RegExpValidator {regExp: /[\d-.]+/}
                                                background: Rectangle {

                                                    color: "#797979"
                                                    opacity: 0.3

                                                }

                                                onEditingFinished: {
                                                    //displaysWorkarea.model.changePositionToBottom(displayRepeater.display, displayRepeater.itemAt(displayRepeater.display).myData.position.bottom - parseInt(bottomOffsetValue.text))
                                                    //sessionController.displaysController.displayWorkarea.model.changeTopOpacityBlending(displayRepeater.display, parseFloat(topOpacity.text))
                                                    sessionController.displayWorkarea.model.changeTopOpacityBlending(displayRepeater.display, parseFloat(topOpacity.text))
                                                }

                                            }

                                            Text {
                                                id: topBlendTransitionPositionText
                                                text: qsTr("Transition position of\ndarkening region")
                                                //anchors.top: topOpacity.bottom
                                            }

                                            TextField {
                                                id: topBlendTransitionPosition
                                                height: 20
                                                width: 60
                                                enabled: displayRepeater.display != -1 ? true : false
                                                //anchors.top: topBlendTransitionPositionText.bottom
                                                Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                                                text: qsTr("0")
                                                rightPadding: 6
                                                leftPadding: 6
                                                padding: 2
                                                Layout.maximumHeight: 20
                                                Layout.maximumWidth: 60
                                                Layout.minimumHeight: 20
                                                Layout.minimumWidth: 60

                                                horizontalAlignment: Text.AlignHCenter
                                                font.pointSize: 10
                                                validator: RegExpValidator {regExp: /[\d-.]+/}
                                                background: Rectangle {

                                                    color: "#797979"
                                                    opacity: 0.3

                                                }

                                                onEditingFinished: {
                                                    //displaysWorkarea.model.changePositionToBottom(displayRepeater.display, displayRepeater.itemAt(displayRepeater.display).myData.position.bottom - parseInt(bottomOffsetValue.text))
                                                    //sessionController.displaysController.displayWorkarea.model.changeTopPositionBlending(displayRepeater.display, parseFloat(topBlendTransitionPositionText.text))
                                                    sessionController.displayWorkarea.model.changeTopPositionBlending(displayRepeater.display, parseFloat(topBlendTransitionPositionText.text))
                                                }

                                            }

                                        }
                                    }

                                    Item {
                                        Layout.row: 0
                                        Layout.column: 2
                                        Layout.maximumHeight: 120
                                        Layout.maximumWidth: 150
                                        Layout.minimumHeight: 120
                                        Layout.minimumWidth: 150
                                    }

                                    Item {
                                        Layout.row: 1
                                        Layout.column: 0
                                        height: 120
                                        width: 150
                                        //anchors.right: centerBlendCell.left

                                        ColumnLayout {
                                            anchors.fill: parent
                                            spacing: 1

                                            Text {
                                                id: leftBlendSize
                                                text: qsTr("Width of darkening region")
                                                //anchors.top: parent.top
                                                Layout.alignment: Qt.AlignTop
                                            }

                                            TextField {
                                                id: leftBlending
                                                height: 20
                                                width: 60
                                                enabled: displayRepeater.display != -1 ? true : false
                                                Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                                                text: qsTr("0")
                                                rightPadding: 6
                                                leftPadding: 6
                                                padding: 2
                                                //anchors.top: leftBlendSize.bottom
                                                Layout.maximumHeight: 20
                                                Layout.maximumWidth: 60
                                                Layout.minimumHeight: 20
                                                Layout.minimumWidth: 60
                                                horizontalAlignment: Text.AlignHCenter
                                                font.pointSize: 10
                                                validator: RegExpValidator {regExp: /[\d-]+/}
                                                background: Rectangle {

                                                    color: "#797979"
                                                    opacity: 0.3

                                                }

                                                onEditingFinished: {
                                                    //displaysWorkarea.model.changePositionToBottom(displayRepeater.display, displayRepeater.itemAt(displayRepeater.display).myData.position.bottom - parseInt(bottomOffsetValue.text))
                                                    //sessionController.displaysController.displayWorkarea.model.changeLeftBlending(displayRepeater.display, parseInt(leftBlending.text))
                                                    sessionController.displayWorkarea.model.changeLeftBlending(displayRepeater.display, parseInt(leftBlending.text))
                                                }
                                            }

                                            Text {
                                                id: leftBlendOpacity
                                                text: qsTr("Opacity of darkening region")
                                                //anchors.top: leftBlending.bottom
                                                //Layout.alignment: Qt.AlignTop
                                            }

                                            TextField {
                                                id: leftOpacity
                                                height: 20
                                                width: 60
                                                enabled: displayRepeater.display != -1 ? true : false
                                                Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                                                text: qsTr("0")
                                                rightPadding: 6
                                                leftPadding: 6
                                                padding: 2
                                                Layout.maximumHeight: 20
                                                Layout.maximumWidth: 60
                                                Layout.minimumHeight: 20
                                                Layout.minimumWidth: 60
                                                //anchors.top: leftBlendOpacity.bottom
                                                horizontalAlignment: Text.AlignHCenter
                                                font.pointSize: 10
                                                validator: RegExpValidator {regExp: /[\d-.]+/}
                                                background: Rectangle {

                                                    color: "#797979"
                                                    opacity: 0.3

                                                }

                                                onEditingFinished: {
                                                    //displaysWorkarea.model.changePositionToBottom(displayRepeater.display, displayRepeater.itemAt(displayRepeater.display).myData.position.bottom - parseInt(bottomOffsetValue.text))
                                                    //sessionController.displaysController.displayWorkarea.model.changeLeftOpacityBlending(displayRepeater.display, parseFloat(leftOpacity.text))
                                                    sessionController.displayWorkarea.model.changeLeftOpacityBlending(displayRepeater.display, parseFloat(leftOpacity.text))
                                                }

                                            }

                                            Text {
                                                id: leftBlendTransitionPositionText
                                                text: qsTr("Transition position of\ndarkening region")
                                                //anchors.top: leftOpacity.bottom
                                            }

                                            TextField {
                                                id: leftBlendTransitionPosition
                                                height: 20
                                                width: 60
                                                enabled: displayRepeater.display != -1 ? true : false
                                                //anchors.top: leftBlendTransitionPositionText.bottom
                                                Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                                                text: qsTr("0")
                                                rightPadding: 6
                                                leftPadding: 6
                                                padding: 2
                                                Layout.maximumHeight: 20
                                                Layout.maximumWidth: 60
                                                Layout.minimumHeight: 20
                                                Layout.minimumWidth: 60

                                                horizontalAlignment: Text.AlignHCenter
                                                font.pointSize: 10
                                                validator: RegExpValidator {regExp: /[\d-.]+/}
                                                background: Rectangle {

                                                    color: "#797979"
                                                    opacity: 0.3

                                                }

                                                onEditingFinished: {
                                                    //displaysWorkarea.model.changePositionToBottom(displayRepeater.display, displayRepeater.itemAt(displayRepeater.display).myData.position.bottom - parseInt(bottomOffsetValue.text))
                                                    //sessionController.displaysController.displayWorkarea.model.changeLeftPositionBlending(displayRepeater.display, parseFloat(leftBlendTransitionPosition.text))
                                                    sessionController.displayWorkarea.model.changeLeftPositionBlending(displayRepeater.display, parseFloat(leftBlendTransitionPosition.text))
                                                }

                                            }
                                        }
                                    }

                                    Item {
                                        id: centerBlendCell
                                        Layout.row: 1
                                        Layout.column: 1
                                        Layout.maximumHeight: 120
                                        Layout.maximumWidth: 150
                                        Layout.minimumHeight: 120
                                        Layout.minimumWidth: 150
                                        //Layout.maximumHeight: 40
                                        //Layout.maximumWidth: 40
                                        //Layout.minimumHeight: 120
                                        //Layout.minimumWidth: 150

                                        //anchors.horizontalCenter: blendingGrid.horizontalCenter
                                    }

                                    Item {
                                        Layout.row: 1
                                        Layout.column: 2
                                        height: 120
                                        width: 150
                                        //anchors.left: centerBlendCell.right

                                        ColumnLayout {
                                            anchors.fill: parent
                                            spacing: 1

                                            Text {
                                                id: rightBlendSize
                                                text: qsTr("Width of darkening region")
                                                //anchors.top: parent.top
                                            }

                                            TextField {
                                                id: rightBlending
                                                height: 20
                                                width: 60
                                                enabled: displayRepeater.display != -1 ? true : false
                                                Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                                                text: qsTr("0")
                                                rightPadding: 6
                                                leftPadding: 6
                                                padding: 2
                                                Layout.maximumHeight: 20
                                                Layout.maximumWidth: 60
                                                Layout.minimumHeight: 20
                                                Layout.minimumWidth: 60
                                                //anchors.top: rightBlendSize.bottom
                                                horizontalAlignment: Text.AlignHCenter
                                                font.pointSize: 10
                                                validator: RegExpValidator {regExp: /[\d-]+/}
                                                background: Rectangle {

                                                    color: "#797979"
                                                    opacity: 0.3

                                                }

                                                onEditingFinished: {
                                                    //displaysWorkarea.model.changePositionToBottom(displayRepeater.display, displayRepeater.itemAt(displayRepeater.display).myData.position.bottom - parseInt(bottomOffsetValue.text))
                                                    //sessionController.displaysController.displayWorkarea.model.changeRightBlending(displayRepeater.display, parseInt(rightBlending.text))
                                                    sessionController.displayWorkarea.model.changeRightBlending(displayRepeater.display, parseInt(rightBlending.text))
                                                }
                                            }

                                            Text {
                                                id: rightBlendOpacity
                                                text: qsTr("Opacity of darkening region")
                                                //anchors.top: rightBlending.bottom
                                            }

                                            TextField {
                                                id: rightOpacity
                                                height: 20
                                                width: 60
                                                enabled: displayRepeater.display != -1 ? true : false
                                                Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                                                text: qsTr("0")
                                                rightPadding: 6
                                                leftPadding: 6
                                                padding: 2
                                                Layout.maximumHeight: 20
                                                Layout.maximumWidth: 60
                                                Layout.minimumHeight: 20
                                                Layout.minimumWidth: 60
                                                //anchors.top: rightBlendOpacity.bottom
                                                horizontalAlignment: Text.AlignHCenter
                                                font.pointSize: 10
                                                validator: RegExpValidator {regExp: /[\d-.]+/}
                                                background: Rectangle {

                                                    color: "#797979"
                                                    opacity: 0.3

                                                }

                                                onEditingFinished: {
                                                    //displaysWorkarea.model.changePositionToBottom(displayRepeater.display, displayRepeater.itemAt(displayRepeater.display).myData.position.bottom - parseInt(bottomOffsetValue.text))
                                                    //sessionController.displaysController.displayWorkarea.model.changeRightOpacityBlending(displayRepeater.display, parseFloat(rightOpacity.text))
                                                    sessionController.displayWorkarea.model.changeRightOpacityBlending(displayRepeater.display, parseFloat(rightOpacity.text))

                                                }

                                            }

                                            Text {
                                                id: rightBlendTransitionPositionText
                                                text: qsTr("Transition position of\ndarkening region")
                                                //anchors.top: rightOpacity.bottom
                                            }

                                            TextField {
                                                id: rightBlendTransitionPosition
                                                height: 20
                                                width: 60
                                                enabled: displayRepeater.display != -1 ? true : false
                                                //anchors.top: rightBlendTransitionPositionText.bottom
                                                Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                                                text: qsTr("0")
                                                rightPadding: 6
                                                leftPadding: 6
                                                padding: 2
                                                Layout.maximumHeight: 20
                                                Layout.maximumWidth: 60
                                                Layout.minimumHeight: 20
                                                Layout.minimumWidth: 60

                                                horizontalAlignment: Text.AlignHCenter
                                                font.pointSize: 10
                                                validator: RegExpValidator {regExp: /[\d-.]+/}
                                                background: Rectangle {

                                                    color: "#797979"
                                                    opacity: 0.3

                                                }

                                                onEditingFinished: {
                                                    //displaysWorkarea.model.changePositionToBottom(displayRepeater.display, displayRepeater.itemAt(displayRepeater.display).myData.position.bottom - parseInt(bottomOffsetValue.text))
                                                    //sessionController.displaysController.displayWorkarea.model.changeRightPositionBlending(displayRepeater.display, parseFloat(rightBlendTransitionPosition.text))
                                                    sessionController.displayWorkarea.model.changeRightPositionBlending(displayRepeater.display, parseFloat(rightBlendTransitionPosition.text))
                                                }

                                            }
                                        }

                                    }

                                    Item {
                                        Layout.row: 2
                                        Layout.column: 0
                                        Layout.maximumHeight: 120
                                        Layout.maximumWidth: 150
                                        Layout.minimumHeight: 120
                                        Layout.minimumWidth: 150
                                    }

                                    Item {
                                        Layout.row: 2
                                        Layout.column: 1
                                        height: 120
                                        width: 150
                                        //anchors.top: centerBlendCell.bottom
                                        //anchors.left: centerBlendCell.left

                                        ColumnLayout {
                                            anchors.fill: parent
                                            spacing: 1

                                            Text {
                                                id: bottomBlendSize
                                                text: qsTr("Height of darkening region")
                                                //anchors.top: parent.top
                                            }

                                            TextField {
                                                id: bottomBlending
                                                height: 20
                                                width: 60
                                                enabled: displayRepeater.display != -1 ? true : false
                                                Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                                                text: qsTr("0")
                                                rightPadding: 6
                                                leftPadding: 6
                                                padding: 2
                                                Layout.maximumHeight: 20
                                                Layout.maximumWidth: 60
                                                Layout.minimumHeight: 20
                                                Layout.minimumWidth: 60
                                                //anchors.top: bottomBlendSize.bottom
                                                horizontalAlignment: Text.AlignHCenter
                                                font.pointSize: 10
                                                validator: RegExpValidator {regExp: /[\d-]+/}
                                                background: Rectangle {

                                                    color: "#797979"
                                                    opacity: 0.3

                                                }

                                                onEditingFinished: {
                                                    //displaysWorkarea.model.changePositionToBottom(displayRepeater.display, displayRepeater.itemAt(displayRepeater.display).myData.position.bottom - parseInt(bottomOffsetValue.text))
                                                    //sessionController.displaysController.displayWorkarea.model.changeBottomBlending(displayRepeater.display, parseInt(bottomBlending.text))
                                                    sessionController.displayWorkarea.model.changeBottomBlending(displayRepeater.display, parseInt(bottomBlending.text))

                                                }
                                            }

                                            Text {
                                                id: bottomBlendOpacity
                                                text: qsTr("Opacity of darkening region")
                                                //anchors.top: bottomBlending.bottom
                                            }

                                            TextField {
                                                id: bottomOpacity
                                                height: 20
                                                width: 60
                                                enabled: displayRepeater.display != -1 ? true : false
                                                Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                                                text: qsTr("0")
                                                rightPadding: 6
                                                leftPadding: 6
                                                padding: 2
                                                Layout.maximumHeight: 20
                                                Layout.maximumWidth: 60
                                                Layout.minimumHeight: 20
                                                Layout.minimumWidth: 60
                                                //anchors.top: bottomBlendOpacity.bottom
                                                horizontalAlignment: Text.AlignHCenter
                                                font.pointSize: 10
                                                validator: RegExpValidator {regExp: /[\d-.]+/}
                                                background: Rectangle {

                                                    color: "#797979"
                                                    opacity: 0.3

                                                }

                                                onEditingFinished: {
                                                    //displaysWorkarea.model.changePositionToBottom(displayRepeater.display, displayRepeater.itemAt(displayRepeater.display).myData.position.bottom - parseInt(bottomOffsetValue.text))
                                                    //sessionController.displaysController.displayWorkarea.model.changeBottomOpacityBlending(displayRepeater.display, parseFloat(bottomOpacity.text))
                                                    sessionController.displayWorkarea.model.changeBottomOpacityBlending(displayRepeater.display, parseFloat(bottomOpacity.text))
                                                }

                                            }

                                            Text {
                                                id: bottomBlendTransitionPositionText
                                                text: qsTr("Transition position of\ndarkening region")
                                                //anchors.top: bottomOpacity.bottom
                                            }

                                            TextField {
                                                id: bottomBlendTransitionPosition
                                                height: 20
                                                width: 60
                                                enabled: displayRepeater.display != -1 ? true : false
                                                //anchors.top: bottomBlendTransitionPositionText.bottom
                                                Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                                                text: qsTr("0")
                                                rightPadding: 6
                                                leftPadding: 6
                                                padding: 2
                                                Layout.maximumHeight: 20
                                                Layout.maximumWidth: 60
                                                Layout.minimumHeight: 20
                                                Layout.minimumWidth: 60

                                                horizontalAlignment: Text.AlignHCenter
                                                font.pointSize: 10
                                                validator: RegExpValidator {regExp: /[\d-.]+/}
                                                background: Rectangle {

                                                    color: "#797979"
                                                    opacity: 0.3

                                                }

                                                onEditingFinished: {
                                                    //displaysWorkarea.model.changePositionToBottom(displayRepeater.display, displayRepeater.itemAt(displayRepeater.display).myData.position.bottom - parseInt(bottomOffsetValue.text))
                                                    //sessionController.displaysController.displayWorkarea.model.changeBottomPositionBlending(displayRepeater.display, parseFloat(bottomBlendTransitionPosition.text))
                                                    sessionController.displayWorkarea.model.changeBottomPositionBlending(displayRepeater.display, parseFloat(bottomBlendTransitionPosition.text))

                                                }

                                            }

                                    }
                                    }

                                    Item {
                                        Layout.row: 2
                                        Layout.column: 2
                                        Layout.maximumHeight: 120
                                        Layout.maximumWidth: 150
                                        Layout.minimumHeight: 120
                                        Layout.minimumWidth: 150
                                    }

                                }
                            }
                        }
                    }
                }

                Item {
                    id: saveItem
                    //color: "yellow"
                    //Layout.fillHeight: true
                    Layout.fillWidth: true
                    Layout.row: 2
                    Layout.column: 0
                    Layout.columnSpan: 2
                    height: 25
                    //Layout.maximumWidth: 200

                    Button {
                        id: saveButton
                        width: 60
                        height: 25
                        anchors.bottom: saveItem.bottom
                        anchors.right: saveItem.right
                        //anchors.horizontalCenter: saveItem.horizontalCenter
                        text: qsTr("Save")
                        font.pointSize: 10

                        onClicked: {
                            //editorPlaylist.saveCurrentSession()
                            sessionController.saveSession()
                            saveMessageBox.open()
//                            if(editorPlaylist.currentDisplayWorkarea.saveToFile())
//                            {
//                                saveMessageBox.open();

//                            }
                        }
                    }
                }
            }

        }

        Page {
            id: playlistPageTab

            signal sendVideoIndex(int videoIndex)
            //visible: true
            padding: 5

            Item {
                id: selectedItemContent
                property var displaysModel: editorPlaylist.currentDisplayWorkarea.model
            }

            GridLayout {
                anchors.fill: parent
                id: playlistGridLayout
                rows: 3
                columns: 3
                rowSpacing: 10

                Item { //Rectangle {
                    //color: "red"
                    id: displaysVideoItem //displaysVideoGridLayout
                    Layout.fillHeight: true
                    Layout.fillWidth: true
                    Layout.row: 0
                    Layout.column: 0
                    Layout.columnSpan: 3
                    Layout.maximumHeight: 200

                    ColumnLayout {
                        id: displaysVideoGridLayout
                        anchors.fill: parent
                        //rows: 1
                        //columns: 2
                        //columnSpacing: 20

                        Text {
                            id: displaysVideoText
                            text: qsTr("Displays")
                            Layout.alignment: Qt.AlignHCenter | Qt.AlignTop
                            horizontalAlignment: Text.AlignLeft
                            font.pointSize: 12

                            //Layout.row: 0
                            //Layout.column: 0
                            //anchors.top: parent.top
                        }

                        Frame {
                            Layout.maximumHeight: 180
                            //Layout.row: 0
                            //Layout.column: 0
                            Layout.fillHeight: true
                            Layout.fillWidth: true
                            //Layout.alignment: Qt.AlignTop
                            //anchors.top: displaysVideoText.bottom
                            anchors.topMargin: 5

                            Repeater {
                                id: displayContentRepeater
                                //id: displayRepeater
                                //anchors.fill: parent
                                width: parent.width - 30
                                height: parent.height
                                anchors.top: parent.top
                                anchors.left: parent.left
                                //clip: true

                                property int contentDisplay: -1

                                //model: editorPlaylist.currentDisplayWorkarea.model
                                //model: editorPlaylist.currentDisplayWorkarea.displayWorkarea.model
                                //model: sessionController.displaysController.displayWorkarea.model
                                model: sessionController.displayWorkarea.model
                                clip: true

                                delegate:
                                    Rectangle {
                                    id: selectionContentRect
                                    border.width: 1
                                    border.color: "black"
                                    color: "transparent"
//                                    x: ((model.position.x - sessionController.displaysController.displayWorkarea.startPos.x) / sessionController.displaysController.displayWorkarea.fullResolution.width) * displayContentRepeater.width // * 0.9 //(model.position.x/displaysWorkarea.fullResolution.width) * displayContentRepeater.width
//                                    y: ((model.position.y - sessionController.displaysController.displayWorkarea.startPos.y) / sessionController.displaysController.displayWorkarea.fullResolution.height) * displayContentRepeater.height // * 0.9 //(model.position.y/displaysWorkarea.fullResolution.height) * displayContentRepeater.height
//                                    width: (model.resolution.width/sessionController.displaysController.displayWorkarea.fullResolution.width) *displayContentRepeater.width
//                                    height: (model.resolution.height/sessionController.displaysController.displayWorkarea.fullResolution.height) *displayContentRepeater.height
                                    //x: ((model.position.x - sessionController.displayWorkarea.startPos.x) / sessionController.displayWorkarea.fullResolution.width) * displayContentRepeater.width // * 0.9 //(model.position.x/displaysWorkarea.fullResolution.width) * displayContentRepeater.width
                                    //y: ((model.position.y - sessionController.displayWorkarea.startPos.y) / sessionController.displayWorkarea.fullResolution.height) * displayContentRepeater.height // * 0.9 //(model.position.y/displaysWorkarea.fullResolution.height) * displayContentRepeater.height
                                    //width: (model.resolution.width/sessionController.displayWorkarea.fullResolution.width) *displayContentRepeater.width
                                    //height: (model.resolution.height/sessionController.displayWorkarea.fullResolution.height) *displayContentRepeater.height

                                    property var xDrawPos: sessionController.displayWorkarea.startPos.x < 0 ? sessionController.displayWorkarea.startPos.x : 0
                                    property var yDrawPos: sessionController.displayWorkarea.startPos.y < 0 ? sessionController.displayWorkarea.startPos.y : 0

//                                    x: (model.position.x === 0 && model.position.y === 0) ? ((model.position.x) / sessionController.displayWorkarea.fullResolution.width) * displayContentRepeater.width // * (1.0 - (sessionController.displayWorkarea.xOffset/displayRepeater.width))
//                                                                                          : ((model.position.x + sessionController.displayWorkarea.xOffset) / sessionController.displayWorkarea.fullResolution.width) * displayContentRepeater.width // * (1.0 - (sessionController.displayWorkarea.xOffset/displayRepeater.width))
//                                    y: (model.position.x === 0 && model.position.y === 0) ? ((model.position.y - selectionContentRect.yDrawPos) / sessionController.displayWorkarea.fullResolution.height) * displayContentRepeater.height // * (1.0 - (sessionController.displayWorkarea.yOffset/displayRepeater.height))
//                                                                                          : ((model.position.y + sessionController.displayWorkarea.yOffset - selectionContentRect.yDrawPos) / sessionController.displayWorkarea.fullResolution.height) * displayContentRepeater.height // * (1.0 - (sessionController.displayWorkarea.yOffset/displayRepeater.height))
//                                    width: (model.position.x === 0 && model.position.y === 0) ? (model.resolution.width/(sessionController.displayWorkarea.fullResolution.width)) * (displayContentRepeater.width - (sessionController.displayWorkarea.xOffset)) //* (1.0 - (sessionController.displayWorkarea.xOffset/displayRepeater.width))
//                                                                                              : (model.resolution.width/(sessionController.displayWorkarea.fullResolution.width - (sessionController.displayWorkarea.startPos.x - model.position.x)))* (displayContentRepeater.width - (sessionController.displayWorkarea.xOffset)) //* (1.0 - (sessionController.displayWorkarea.xOffset/displayRepeater.width))
//                                    height: (model.position.x === 0 && model.position.y === 0) ? ((model.resolution.height)/sessionController.displayWorkarea.fullResolution.height) * (displayContentRepeater.height - Math.abs(sessionController.displayWorkarea.yOffset)) //* (1.0 - (sessionController.displayWorkarea.yOffset/displayRepeater.height))
//                                                                                               : (model.resolution.height/(sessionController.displayWorkarea.fullResolution.height - (sessionController.displayWorkarea.startPos.y - model.position.y))) * (displayContentRepeater.height - Math.abs(sessionController.displayWorkarea.yOffset)) // * (1.0 - (sessionController.displayWorkarea.yOffset/displayRepeater.height))

                                    x: ((model.position.x) / sessionController.displayWorkarea.fullResolution.width) * displayContentRepeater.width //* 0.3 //((model.position.x - editorPlaylist.currentDisplayWorkarea.startPos.x) / editorPlaylist.currentDisplayWorkarea.fullResolution.width) * displayContentRepeater.width * 0.3  //(model.position.x/displaysWorkarea.fullResolution.width) * displayContentRepeater.width
                                    y: ((model.position.y) / sessionController.displayWorkarea.fullResolution.height) * displayContentRepeater.height //* 0.3 //((model.position.y - editorPlaylist.currentDisplayWorkarea.startPos.y) / editorPlaylist.currentDisplayWorkarea.fullResolution.height) * displayContentRepeater.height * 0.3 //(model.position.y/displaysWorkarea.fullResolution.height) * displayContentRepeater.height
                                    width: (model.resolution.width/sessionController.displayWorkarea.fullResolution.width)*displayContentRepeater.width // + (sessionController.displayWorkarea.startPos.right - model.position.x)
                                    height: (model.resolution.height/sessionController.displayWorkarea.fullResolution.height)*displayContentRepeater.height // (sessionController.displayWorkarea.startPos.bottom - model.position.y)

                                    z: mouseAreaContent.drag.active || mouseAreaContent.pressed ? 0 : -1
                                    //property int currentIndex : index
                                    property variant myDataContent
                                    //property alias displayCellIndex: index

                                    Text {
                                        id: idDisplayTextPlaylist
                                        //text: (model.idDisplay)
                                        text: model.idDisplay
                                        //text: displaysWorkarea.gridModel.displayId
                                        //text: displaysWorkarea.fullResolution.width
                                        //text: (model.resolution.width/displaysWorkarea.fullResolution.width)*displayRepeater.width
                                        //text: (model.position.x/displaysWorkarea.fullResolution.width)*displayRepeater.width
                                        //text: model.resolution.width
                                        font.pointSize: 20
                                        horizontalAlignment: Text.Center
                                        width: 20
                                        height: 50
                                        anchors.horizontalCenter: parent.horizontalCenter
                                        anchors.verticalCenter: parent.verticalCenter
                                        //Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                                    }

                                    Text {
                                        id: primaryScreenPlaylist
                                        text: qsTr("(primary screen)")
                                        font.pointSize: 10
                                        height: 50
                                        verticalAlignment: Text.Center
                                        anchors.left: (idDisplayTextPlaylist.right)
                                        anchors.verticalCenter: idDisplayTextPlaylist.verticalCenter
                                        visible: model.primaryScreen ? true : false
                                    }

                                    MouseArea {
                                        id: mouseAreaContent
                                        anchors.fill: parent
                                        onClicked: {
                                            if(displayContentRepeater.contentDisplay == -1)
                                            {
                                                displayContentRepeater.itemAt(index).color = "green"
                                                //console.log("check")
                                                displayContentRepeater.contentDisplay = index
                                                //console.log("check")
                                                //sessionController.displaysController.selectDisplay(index)
                                                sessionController.selectDisplay(index)
                                                selectionContentRect.myDataContent = model
                                                listViewCurrentVideoPlaylist.currentIndex = currentVideoPlaylistItem.currentIndexForAllPlaylists
                                                //subItemsRect.currentVideoIndex = sessionController.getPlaylistCollectionIndexFromPlaylistName(model.playlistName)
                                                //listViewVideoPlaylist.contentItem.children[index].mySubItemsRect.mySubItemRepeater.itemAt(videoIndex).backgroundColor = "white"
                                                //listViewVideoPlaylist.contentItem.children[index].mySubItemsRect.currentVideoIndex = -1
                                                console.log("playlistCollectionList size " + sessionController.playlistCollectionList.sizeOfList())
                                                for(var i = 0; i < sessionController.playlistCollectionList.sizeOfList(); i++)
                                                {
                                                    var videoIndex = sessionController.getPlaylistCollectionIndex(i)
                                                    console.log("videoIndex " + videoIndex)
                                                    console.log("listViewVideoPlaylist.contentItem.children[i].mySubItemsRect.currentVideoIndex " + listViewVideoPlaylist.contentItem.children[i].mySubItemsRect.currentVideoIndex)
                                                    listViewVideoPlaylist.contentItem.children[i].mySubItemsRect.currentVideoIndex = sessionController.getPlaylistCollectionIndex(i)
                                                    console.log("listViewVideoPlaylist.contentItem.children[i].mySubItemsRect.currentVideoIndex " + listViewVideoPlaylist.contentItem.children[i].mySubItemsRect.currentVideoIndex)
                                                    //console.log("listViewVideoPlaylist.contentItem.children[i].mySubItemsRect.mySubItemRepeater.itemAt(videoIndex).backgroundColor "+listViewVideoPlaylist.contentItem.children[i].mySubItemsRect.mySubItemRepeater.itemAt(videoIndex).backgroundColor)
                                                    listViewVideoPlaylist.contentItem.children[i].mySubItemsRect.mySubItemRepeater.itemAt(videoIndex).backgroundColor = "#797979"
                                                    //console.log("listViewVideoPlaylist.contentItem.children[i].mySubItemsRect.mySubItemRepeater.itemAt(videoIndex).backgroundColor "+listViewVideoPlaylist.contentItem.children[i].mySubItemsRect.mySubItemRepeater.itemAt(videoIndex).backgroundColor)
                                                }
                                            }

                                            else
                                            {
                                                displayContentRepeater.itemAt(displayContentRepeater.contentDisplay).color = "transparent"
                                                displayContentRepeater.itemAt(index).color = "green"
                                                //console.log("check")
                                                displayContentRepeater.contentDisplay = index
                                                //sessionController.displaysController.selectDisplay(index)
                                                sessionController.selectDisplay(index)
                                                //console.log("check")
                                                selectionContentRect.myDataContent = model
                                                listViewCurrentVideoPlaylist.currentIndex = currentVideoPlaylistItem.currentIndexForAllPlaylists
                                                console.log("playlistCollectionList size " + sessionController.playlistCollectionList.sizeOfList())

                                                for(var i = 0; i < sessionController.playlistCollectionList.sizeOfList(); i++)
                                                {
                                                    for(var j = 0; j < sessionController.playlistCollectionList.sizeOfVideoplaylist(i); j++)
                                                    {
                                                        //console.log("i " + i + " j "+ j)
                                                        //console.log(listViewVideoPlaylist.contentItem.children[i].mySubItemsRect.mySubItemRepeater.itemAt(j).backgroundColor)
                                                        listViewVideoPlaylist.contentItem.children[i].mySubItemsRect.mySubItemRepeater.itemAt(j).backgroundColor = "white"
                                                    }
                                                }

                                                for(var k = 0; k < sessionController.playlistCollectionList.sizeOfList(); k++)
                                                {
                                                    var videoIndexK = sessionController.getPlaylistCollectionIndex(k)
                                                    console.log("videoIndex " + videoIndexK)
                                                    console.log("listViewVideoPlaylist.contentItem.children[i].mySubItemsRect.currentVideoIndex " + listViewVideoPlaylist.contentItem.children[k].mySubItemsRect.currentVideoIndex)
                                                    listViewVideoPlaylist.contentItem.children[k].mySubItemsRect.currentVideoIndex = sessionController.getPlaylistCollectionIndex(k)
                                                    console.log("listViewVideoPlaylist.contentItem.children[i].mySubItemsRect.currentVideoIndex " + listViewVideoPlaylist.contentItem.children[k].mySubItemsRect.currentVideoIndex)
                                                    //console.log("listViewVideoPlaylist.contentItem.children[i].mySubItemsRect.mySubItemRepeater.itemAt(videoIndex).backgroundColor "+listViewVideoPlaylist.contentItem.children[k].mySubItemsRect.mySubItemRepeater.itemAt(videoIndexK).backgroundColor)
                                                    listViewVideoPlaylist.contentItem.children[k].mySubItemsRect.mySubItemRepeater.itemAt(videoIndexK).backgroundColor = "#797979"
                                                    //console.log("listViewVideoPlaylist.contentItem.children[i].mySubItemsRect.mySubItemRepeater.itemAt(videoIndex).backgroundColor "+listViewVideoPlaylist.contentItem.children[k].mySubItemsRect.mySubItemRepeater.itemAt(videoIndexK).backgroundColor)
                                                }

                                            }
                                        }
                                    }
                                }

                            }
                        }

                        //                        Text {
                        //                            id: mainVideoDisplayText
                        //                            text: qsTr("Main Display")
                        //                            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                        //                            horizontalAlignment: Text.AlignLeft
                        //                            font.pointSize: 12

                        //                            Layout.row: 0
                        //                            Layout.column: 1
                        //                            Layout.maximumWidth: 200
                        //                            anchors.top: parent.top
                        //                        }

                        //                        Frame {
                        //                            Layout.row: 0
                        //                            Layout.column: 1
                        //                            Layout.fillHeight: true
                        //                            Layout.fillWidth: true
                        //                            anchors.top: mainVideoDisplayText.bottom
                        //                            anchors.topMargin: 5
                        //                            Layout.maximumWidth: 150
                        //                            Layout.maximumHeight: 150
                        //                        }
                    }
                }

                Item {
                    id: selectionVideo
                    property variant myVideoContent
                    property int displayCellIndex: -1
                    property int videoIndex: -1
                }

                Item { //Rectangle {
                    //color: "yellow"
                    id: videoPlaylistItem
                    Layout.fillHeight: true
                    //Layout.fillWidth: true
                    Layout.row: 1
                    Layout.column: 0
                    //Layout.minimumWidth: 500
                    Layout.minimumWidth: 400
                    //Layout.preferredWidth: 400

                    property bool isLoop: false
                    signal videoLoop(bool isLoop)
                    signal deleteVideo(int index)
                    signal deletePlaylist(int index)

                    Frame {
                        id: videoPlaylistFrame
                        anchors.fill: parent

                        ColumnLayout {
                            id: videoPlaylistColumnLayout
                            anchors.rightMargin: 0
                            anchors.bottomMargin: 0
                            anchors.leftMargin: 0
                            anchors.topMargin: 0
                            anchors.fill: parent

                            Text {
                                id: videoPlaylistText
                                text: qsTr("Playlists")
                                Layout.alignment: Qt.AlignTop
                                //anchors.top: videoPlaylistColumnLayout.top
                                //anchors.left: videoPlaylistColumnLayout.left
                                font.pointSize: 10
                                anchors.margins: 5
                                height: 25
                                width: 50
                            }

                            RowLayout {
                                id: addAndLoopItem
                                Layout.fillWidth: true
                                Layout.minimumWidth: videoPlaylistColumnLayout.width
                                //Layout.maximumWidth: videoPlaylistColumnLayout.width
                                height: 40

                                Button {
                                    id: addVideo
                                    height: 40
                                    width: 60
                                    anchors.topMargin: 5
                                    Layout.maximumWidth: 60
                                    Layout.alignment: Qt.AlignLeft
                                    //anchors.right: deleteVideo.left
                                    //Layout.right: addAndLoopCenterItem.left
                                    //Layout.row: 0
                                    //Layout.column: 0
                                    text: qsTr("+")

                                    onClicked: {
                                        //console.log(displayContentRepeater.contentDisplay);
                                        //displaysWorkarea.model.addVideo(displayContentRepeater.contentDisplay, "")
                                        fileDialog.open();
                                    }
                                }

                                Button {
                                    id: deleteVideo
                                    height: 40
                                    width: 60
                                    anchors.topMargin: 5
                                    Layout.maximumWidth: 60
                                    Layout.alignment: Qt.AlignLeft
                                    //anchors.right: addAndLoopCenterItem.left
                                    //Layout.right: addAndLoopCenterItem.left
                                    //Layout.row: 0
                                    //Layout.column: 1
                                    text: qsTr("-")

                                    onClicked: {
                                        sessionController.deletePlaylist(listViewVideoPlaylist.currentIndex)
                                    }
                                }

                                Item {
                                    id: addAndLoopCenterItem
                                    //Layout.fillHeight: true
                                    //Layout.row: 0
                                    //Layout.column: 2
                                    height: 40
                                    Layout.fillWidth: true
                                    //width: listViewVideoPlaylist.width + 150
                                    Layout.maximumWidth: listViewVideoPlaylist.width - (addVideo.width + deleteVideo.width + loopPlaylist.width)
                                    anchors.horizontalCenter: parent.horizontalCenter + (deleteVideo.horizontalCenter)
                                }
                            }

                            ListView {
                                id: listViewVideoPlaylist
                                Layout.fillHeight: true
                                Layout.fillWidth: true
                                Layout.maximumWidth: parent.width
                                //width: parent.width - 50
                                //anchors.top: addAndLoopItem.bottom
                                anchors.topMargin: 3
                                //anchors.left: videoPlaylistText.left
                                //anchors.horizontalCenter: parent.horizontalCenter
                                clip: true
                                boundsBehavior: Flickable.StopAtBounds
                                cacheBuffer: 10000

                                ScrollBar.vertical: ScrollBar {}

                                model: sessionController.playlistCollectionModel

                                delegate: listViewVideoPlaylistDelegate

                                Connections {
                                    target: sessionController
                                    onCurrentSessionChanged: {
                                        //console.log("Current Session Changed from playlistCollectionListModel")
                                        //listViewVideoPlaylist.model = sessionController.playlistCollectionModel
                                    }
                                }

                            }

                            Component {
                                id: listViewVideoPlaylistDelegate

                                Item {
                                    id: delegate

                                    property int itemHeight: 40
                                    property alias expandedItemCount: subItemRepeater.count

                                    property variant myModel: model

                                    // Flag to indicate if this delegate is expanded
                                    property bool expanded: false

                                    property alias mySubItemsRect: subItemsRect

                                    property var currentDelegateVideoIndex: -1
                                    //x: 0; y: 0;
                                    width: listViewVideoPlaylist.width
                                    height: headerItemRect.height + subItemsRect.height

                                    // Top level list item.
                                    ListItem {
                                        id: headerItemRect
                                        //x: 0; y: 0
                                        width: parent.width
                                        height: parent.itemHeight
                                        text: model.playlistName
                                        onClicked: {
                                            expanded = !expanded;
                                            //console.log("index " + index)
                                            if(expanded)
                                            {
                                                console.log("subItemsRect.currentVideoIndex " + subItemsRect.currentVideoIndex)
                                                listViewVideoPlaylist.currentIndex = index
                                                //subItemsRect.currentVideoIndex = sessionController.getPlaylistCollectionIndex()
                                                //console.log("index "+ sessionController.getPlaylistCollectionIndexFromPlaylistName(model.playlistName))
                                                //subItemsRect.currentVideoIndex = sessionController.getPlaylistCollectionIndexFromPlaylistName(model.playlistName)
                                                console.log("subItemsRect.currentVideoIndex " + subItemsRect.currentVideoIndex)
                                                subItemRepeater.itemAt(subItemsRect.currentVideoIndex).backgroundColor = "#797979"
                                            }
                                            else
                                            {
                                                listViewVideoPlaylist.currentIndex = -1
                                                //index = -1
                                            }
                                            //console.log("index " + index)
                                            //console.log("listViewVideoPlaylist.currentIndex " + listViewVideoPlaylist.currentIndex)
                                            //delegate.myPlaylist = model.playlist(0)
                                            //console.log("Playlist "+ model.videoPlaylist)
                                        }

                                        backgroundColor: !expanded ? "white" : "#C0C0C0"

                                        //                bgImage: container.bgImage
                                        //                bgImagePressed: container.bgImagePressed
                                        //                bgImageActive: container.bgImageActive
                                        //                fontName: container.headerItemFontName

                                        fontSize: 10
                                        fontColor: "black"
                                        //fontBold: true
                                    }

                                    // Subitems are in a column whose height depends
                                    // on the expanded status. When not expandend, it is zero.
                                    Item {
                                        id: subItemsRect
                                        property int itemHeight: delegate.itemHeight

                                        property int currentVideoIndex: -1

                                        y: headerItemRect.height
                                        width: parent.width
                                        height: expanded ? expandedItemCount * itemHeight : 0
                                        clip: true

                                        opacity: 1
                                        Behavior on height {
                                            // Animate subitem expansion. After the final height is reached,
                                            // ensure that it is visible to the user.
                                            SequentialAnimation {
                                                NumberAnimation { duration: 10; easing.type: Easing.InOutQuad }
                                                ScriptAction { script: parent.positionViewAtIndex(index, ListView.Contain) }
                                            }
                                        }

                                        property alias mySubItemRepeater: subItemRepeater

                                        Column {
                                            width: parent.width

                                            // Repeater creates each sub-ListItem using attributes
                                            // from the model.
                                            Repeater {
                                                id: subItemRepeater
                                                //model: model.videoPlaylist
                                                //model: delegate.myPlaylist.videoPlaylist
                                                model: delegate.myModel.videoplaylistModel
                                                //model: VideoPlaylistModel {
                                                //    playlist: model.videoplaylist
                                                //    //playlist: model.videoPlaylist //delegate.myPlaylist //videoPlaylist //videoPlaylist //delegate.myPlaylist.videoPlaylist
                                                //} //videoPlaylist
                                                width: subItemsRect.width

                                                //property int currentVideoIndex: -1

                                                delegate:
                                                    ListItem {
                                                    id: subListItem
                                                    width: delegate.width
                                                    height: subItemsRect.itemHeight
                                                    text: model.filename
                                                    //bgImage: container.bgImageSubItem
                                                    //fontName: container.subItemFontName
                                                    fontSize: 9 //container.subItemFontSize
                                                    fontColor: "black" //container.subItemFontColor
                                                    textIndent: 20 //container.indent
                                                    //backgroundColor: index == subItemsRect.currentVideoIndex ? "#797979" : "white"
                                                    onClicked: {
                                                        if(subItemsRect.currentVideoIndex == -1 && displayContentRepeater.contentDisplay != -1)
                                                        {
                                                            console.log("add section")
                                                            console.log("subItemsRect.currentVideoIndex " + subItemsRect.currentVideoIndex)
                                                            subItemsRect.currentVideoIndex = index
                                                            subItemRepeater.itemAt(index).backgroundColor = "#797979"

                                                            //displaysController.addVideo(model.source)
                                                            //sessionController.displaysController.addVideo(model.source)
                                                            if(displayContentRepeater.model.get(displayContentRepeater.contentDisplay).primaryScreen !== true)
                                                            {
                                                                //console.log("selected display primary")
                                                                sessionController.addVideo(model.source)
                                                                console.log("add")

                                                                contentWindow.repeater.itemAt(displayContentRepeater.contentDisplay).myVideo.playlist.removeItems(
                                                                            0, contentWindow.repeater.itemAt(displayContentRepeater.contentDisplay).myVideo.playlist.itemCount)

                                                                contentWindow.repeater.itemAt(displayContentRepeater.contentDisplay).myVideo.playlist.addItems(
                                                                            sessionController.getSourcesForPlaylist())

                                                                if(contentWindow.repeater.itemAt(displayContentRepeater.contentDisplay).myVideo.playlist.itemCount == 1)
                                                                    listViewCurrentVideoPlaylist.currentIndex = 0
                                                            }

                                                        }
                                                        else
                                                        {
                                                            //                                                                console.log("else")
                                                            //                                                                console.log("currentVideoIndex " + subItemsRect.currentVideoIndex)
                                                            //                                                                console.log("index " + index)
                                                            //                                                                console.log("source " + model.source)
                                                            subItemRepeater.itemAt(subItemsRect.currentVideoIndex).backgroundColor = "white"
                                                            subItemsRect.currentVideoIndex = index
                                                            subItemRepeater.itemAt(index).backgroundColor = "#797979"

                                                            //displaysController.changeVideoInList(model.source)
                                                            if(displayContentRepeater.model.get(displayContentRepeater.contentDisplay).primaryScreen !== true) {
                                                                sessionController.changeVideoInList(model.source)
                                                                console.log("change")
                                                                listViewCurrentVideoPlaylist.model.updatePlaylist()
                                                            }
                                                        }
                                                    }

                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                Item {
                    //id:
                    id: currentVideoPlaylistItem
                    Layout.fillHeight: true
                    Layout.fillWidth: true
                    Layout.row: 1
                    Layout.column: 1
                    Layout.preferredWidth: 100

                    property bool isLoop: false
                    signal videoLoop(bool isLoop)
                    signal deleteVideo(int index)

                    signal itemUp(int index)
                    signal itemDown(int index)

                    property int currentIndexForAllPlaylists: 0

                    Frame {
                        //id: videoControlFrame
                        anchors.fill: parent

                        ColumnLayout {
                            id: videoCurrentPlaylistColumnLayout
                            anchors.rightMargin: 0
                            anchors.bottomMargin: 0
                            anchors.leftMargin: 0
                            anchors.topMargin: 0
                            anchors.fill: parent
                            Layout.fillWidth: true

                            Item {
                                //id: name
                                Layout.fillWidth: true
                                Layout.minimumHeight: 25
                                Layout.maximumHeight: 25
                                height: 25
                                width: 50

                                Text {
                                    id: videoCurrentPlaylistText
                                    text: qsTr("Current videos")
                                    Layout.alignment: Qt.AlignLeft | Qt.AlignTop
                                    //anchors.top: videoCurrentPlaylistColumnLayout.top
                                    //anchors.left: videoCurrentPlaylistColumnLayout.left
                                    font.pointSize: 10
                                    anchors.margins: 5
                                    height: 25
                                    width: 50
                                }
                            }

                            Item {
                                Layout.fillWidth: true
                                //Layout.maximumWidth: videoCurrentPlaylistColumnLayout.width
                                //Layout.minimumWidth: videoCurrentPlaylistColumnLayout.width
                                Layout.minimumHeight: 40
                                Layout.maximumHeight: 40

                                RowLayout {
                                    id: currentVideoListControl
                                    anchors.fill: parent
                                    //Layout.fillWidth: true
                                    //anchors.fill: parent
                                    //columnSpan: 1

                                    Button {
                                        id: deleteCurrentVideo
                                        height: 40
                                        width: 40
                                        anchors.topMargin: 5
                                        Layout.maximumWidth: 40
                                        Layout.alignment: Qt.AlignRight
                                        Layout.topMargin: 0
                                        Layout.bottomMargin: 0
                                        Layout.leftMargin: 0
                                        Layout.rightMargin: 0
                                        //anchors.left: listViewCurrentVideoPlaylist.left
                                        //anchors.top: videoCurrentPlaylistText.bottom
                                        //Layout.right: addAndLoopCenterItem.left
                                        text: qsTr("-")

                                        onClicked: {
                                            if(listViewCurrentVideoPlaylist.currentIndex != -1 && listViewCurrentVideoPlaylist.model.rowCount() > 0)
                                            {
                                                //String source = listViewCurrentVideoPlaylist.model.get(listViewCurrentVideoPlaylist.currentIndex).source;

                                                currentVideoPlaylistItem.deleteVideo(listViewCurrentVideoPlaylist.currentIndex)
                                                //sessionController.deleteVideo(listViewCurrentVideoPlaylist.currentIndex)
                                            }
                                        }
                                    }

                                    Item {
                                        id: voidItemCurrentVideo
                                        //anchors.left: deleteCurrentVideo.right
                                        Layout.topMargin: 0
                                        Layout.bottomMargin: 0
                                        Layout.leftMargin: 0
                                        Layout.rightMargin: 0
                                        height: 40
                                        Layout.alignment: Qt.AlignRight
                                        Layout.minimumWidth: parent.width - 40*5
                                        Layout.maximumWidth: parent.width - 40*5
                                        //width: parent.width - 40*5
                                        //width: parent.width - 200
                                    }

                                    Button {
                                        id: upCurrentVideoPosition
                                        height: 40
                                        width: 40
                                        anchors.topMargin: 5
                                        Layout.maximumWidth: 40
                                        Layout.alignment: Qt.AlignRight
                                        Layout.topMargin: 0
                                        Layout.bottomMargin: 0
                                        Layout.leftMargin: 0
                                        Layout.rightMargin: 0
                                        //anchors.right: downCurrentVideoPosition.left
                                        //anchors.top: videoCurrentPlaylistText.bottom
                                        //Layout.right: addAndLoopCenterItem.left
                                        //Layout.row: 0
                                        //Layout.column: 1
                                        text: qsTr("up")
                                        icon.source: "qrc:/src/icons2/icons8-up-100.png"

                                        onClicked: {
                                            if(listViewCurrentVideoPlaylist.currentIndex != -1 && listViewCurrentVideoPlaylist.model.rowCount() > 0)
                                            {
                                                currentVideoPlaylistItem.itemUp(listViewCurrentVideoPlaylist.currentIndex)
                                                //sessionController.itemPosUp(listViewCurrentVideoPlaylist.currentIndex)
                                            }

                                            //console.log("dis: " + displayContentRepeater.contentDisplay);
                                            //console.log("index vid: " + listViewVideoPlaylist.currentIndex);

                                            //displaysWorkarea.model.deleteVideo(displayContentRepeater.contentDisplay, listViewVideoPlaylist.currentIndex)
                                            //listViewVideoPlaylist.currentIndex = -1
                                            //videoPlaylistItem.deleteVideo(listViewVideoPlaylist.currentIndex)
                                        }
                                    }

                                    Button {
                                        id: downCurrentVideoPosition
                                        height: 40
                                        width: 40
                                        anchors.topMargin: 5
                                        Layout.maximumWidth: 40
                                        Layout.alignment: Qt.AlignRight
                                        Layout.topMargin: 0
                                        Layout.bottomMargin: 0
                                        Layout.leftMargin: 0
                                        Layout.rightMargin: 0
                                        //anchors.right: loopCurrentVideo.left
                                        //anchors.top: videoCurrentPlaylistText.bottom
                                        //Layout.right: addAndLoopCenterItem.left
                                        //Layout.row: 0
                                        //Layout.column: 1
                                        text: qsTr("down")
                                        icon.source: "qrc:/src/icons2/icons8-down-100.png"

                                        onClicked: {
                                            if(listViewCurrentVideoPlaylist.currentIndex != -1 && listViewCurrentVideoPlaylist.model.rowCount() > 0)
                                            {
                                                currentVideoPlaylistItem.itemDown(listViewCurrentVideoPlaylist.currentIndex)
                                                //sessionController.itemPosDown(listViewCurrentVideoPlaylist.currentIndex)
                                            }
                                        }
                                    }

                                    Button {
                                        id: loopCurrentVideo
                                        height: 40
                                        width: 40
                                        anchors.topMargin: 5
                                        Layout.maximumWidth: 40
                                        Layout.alignment: Qt.AlignRight
                                        Layout.topMargin: 0
                                        Layout.bottomMargin: 0
                                        Layout.leftMargin: 0
                                        Layout.rightMargin: 0
                                        //Layout.right: parent.right
                                        //anchors.right: parent.right
                                        //anchors.top: .bottom
                                        //Layout.right: addAndLoopCenterItem.left
                                        //Layout.row: 0
                                        //Layout.column: 1
                                        text: qsTr("loop")
                                        icon.source: "qrc:/src/icons2/Media-Controls-Repeat-icon.png"

                                        SystemPalette { id: myPalette; colorGroup: SystemPalette.Inactive }

                                        background: Rectangle {
                                            id: loopButtonRect
                                            implicitWidth: 60
                                            implicitHeight: 40
                                            //opacity: enabled ? 1 : 0.3
                                            //border.color: control.down ? "#FA8072" : "#696969"
                                            //border.width: 1
                                            color: myPalette.midlight //QPalette::Button
                                        }

                                        onClicked: {
                                            if(currentVideoPlaylistItem.isLoop)
                                            {
                                                currentVideoPlaylistItem.isLoop = false
                                                currentVideoPlaylistItem.videoLoop(currentVideoPlaylistItem.isLoop)
                                                loopButtonRect.color = myPalette.midlight
                                            }
                                            else
                                            {
                                                currentVideoPlaylistItem.isLoop = true
                                                currentVideoPlaylistItem.videoLoop(currentVideoPlaylistItem.isLoop)
                                                //loopButtonRect.color = "green"
                                                loopButtonRect.color = "#797979"
                                            }

                                        }
                                    }
                                }
                            }

                            ListView {
                                id: listViewCurrentVideoPlaylist
                                Layout.fillHeight: true
                                Layout.fillWidth: true
                                Layout.maximumWidth: parent.width - 10
                                //width: parent.width - 50
                                //anchors.top: currentVideoListControl.bottom
                                anchors.topMargin: 5
                                //anchors.left: videoPlaylistText.left
                                //anchors.horizontalCenter: parent.horizontalCenter
                                clip: true
                                boundsBehavior: Flickable.StopAtBounds

                                ScrollBar.vertical: ScrollBar {}

                                model: sessionController.currentVideoPlaylistModel

                                currentIndex: currentVideoPlaylistItem.currentIndexForAllPlaylists

                                //model: displaysWorkarea.model.playlistModel(displayContentRepeater.contentDisplay)

                                delegate: Item {
                                    id: itemPlaylist
                                    height: 40
                                    width: parent.width

                                    Rectangle {
                                        id: wrapperPlaylist
                                        anchors.fill: parent
                                        //Layout.fillWidth: changeDisplaysColumnLayout
                                        //color: ListView.isCurrentItem ? "red" : "lightblue"
                                        border {
                                            color: "black"
                                            width: 1
                                        }
                                    }

                                    Text {
                                        //Layout.leftMargin: 5
                                        //anchors.horizontalCenter: parent.horizontalCenter
                                        //anchors.verticalCenter: parent.verticalCenter
                                        //anchors.leftMargin: 5
                                        //anchors.leftMargin: 5
                                        anchors {
                                            left: parent.left
                                            top: parent.top
                                            right: parent.right
                                            topMargin: 10
                                            bottomMargin: 10
                                            leftMargin: 8
                                            rightMargin: 8
                                            verticalCenter: parent.verticalCenter
                                        }

                                        renderType: Text.NativeRendering
                                        text: model.filename
                                        font.pointSize: 10
                                        elide: Text.ElideRight
                                    }

                                    MouseArea {
                                            anchors.fill: parent
                                            onClicked: {
                                                for(var a=0; a < contentWindow.repeater.model.rowCount() ; a++) {
                                                    contentWindow.repeater.itemAt(a).myVideo.stop()
                                                }
                                                startStopVideo.icon.source = "qrc:/src/icons2/Media-Controls-Play-icon.png"

                                                listViewCurrentVideoPlaylist.currentIndex = index
                                                currentVideoPlaylistItem.currentIndexForAllPlaylists = listViewCurrentVideoPlaylist.currentIndex
                                                contentWindow.currentPlaylistIndexChanged()
                                                //console.log("currentVideoPlaylistItem.currentIndexForAllPlaylists " + currentVideoPlaylistItem.currentIndexForAllPlaylists)
                                            }
                                    }
                                }


                                highlightFollowsCurrentItem: true
                                highlightMoveDuration: 1
                                highlightResizeVelocity : -1

                                highlight: Rectangle {
                                    border.color: "#797979"
                                    border.width: 3
                                    color: "#797979"
                                    opacity: 0.3
                                    width: listViewCurrentVideoPlaylist.width
                                    //height: parent.height
                                    //y: listViewCurrentVideoPlaylist.currentItem.y
                                    z: Infinity
                                }
                            }
                        }

                        Connections {
                            target: contentWindow.repeater.itemAt(1).myVideo.playlist
                            onCurrentIndexChanged: {
                                console.log("playlist.currentIndex "+contentWindow.repeater.itemAt(1).myVideo.playlist.currentIndex)
                                //currentVideoPlaylistItem.currentIndexForAllPlaylists
                                listViewCurrentVideoPlaylist.currentIndex = contentWindow.repeater.itemAt(1).myVideo.playlist.currentIndex
                            }
                        }
                    }
                }

                Item { //Rectangle {
                    //color: "blue"
                    id: controlVideoItem
                    Layout.fillHeight: true
                    Layout.fillWidth: true
                    Layout.row: 1
                    Layout.column: 2
                    //Layout.columnSpan: 2
                    Layout.minimumWidth: 250
                    Layout.maximumWidth: 250

                    signal playVideo(int videoIndex)
                    signal pausedVideo(int videoIndex)
                    signal forwardVideo(int videoIndex)
                    signal rewindVideo(int videoIndex)
                    signal nextVideo(int videoIndex)
                    signal prevVideo(int videoIndex)
                    property bool paused: false

                    Frame {
                        id: videoControlFrame
                        anchors.fill: parent

                        Item {
                            id: volumeSliderItem
                            height: 120
                            width: 40
                            anchors.bottom: videoControlsGridLayout.top
                            anchors.horizontalCenter: videoControlsGridLayout.horizontalCenter
                            visible: false
                            anchors.bottomMargin: 5

                            property var videoVolume: 100

                            Slider {
                                id: volumeSlider
                                //anchors.fill: parent
                                height: 95
                                width: 10
                                from: 0
                                to: 100
                                value: volumeSliderItem.videoVolume//parseInt(volumeSliderItem.videoVolume)
                                anchors.horizontalCenter: parent.horizontalCenter
                                orientation: Qt.Vertical
                                padding: 5
                                stepSize: 1

                                handle: Item {}

                                onValueChanged: {
                                    volumeSliderItem.videoVolume = volumeSlider.value //QtMultimedia.convertVolume(volumeSlider.value,
                                                                                      //        QtMultimedia.LogarithmicVolumeScale,
                                                                                      //        QtMultimedia.LinearVolumeScale)
                                }
                            }

                            Text {
                                id: volumeSliderText
                                text: (volumeSliderItem.videoVolume) //contentWindow.repeater.itemAt(1).myVideo == null ? "0" : contentWindow.repeater.itemAt(1).myVideo.volume
                                font.pointSize: 10
                                height: 20
                                width: volumeSliderItem.width
                                anchors.bottom: parent.bottom
                                rightPadding: 6
                                leftPadding: 6
                                padding: 2
                                horizontalAlignment: Text.AlignHCenter
                                verticalAlignment: Text.AlignVCenter

                                Rectangle {
                                    anchors.fill: parent
                                    color: "#797979"
                                    opacity: 0.3
                                }

                                onTextChanged: {
                                    //contentWindow.repeater.itemAt(1).myVideo.volume = parseInt(volumeSliderItem.videoVolume)
                                    contentWindow.repeater.itemAt(1).myVideo.volume = QtMultimedia.convertVolume((volumeSlider.value/100),
                                                                                                                 QtMultimedia.LogarithmicVolumeScale,
                                                                                                                 QtMultimedia.LinearVolumeScale)
                                }
                            }
                        }

                        GridLayout {
                            id: videoControlsGridLayout
                            //anchors.fill: parent
                            Layout.minimumHeight: 80
                            Layout.maximumHeight: 80
                            //Layout.minimumWidth: parent.width
                            //Layout.maximumWidth: parent.width
                            Layout.alignment: Qt.AlignCenter
                            //height: 80
                            //width: parent.width
                            anchors.horizontalCenter: parent.horizontalCenter
                            anchors.verticalCenter: parent.verticalCenter
                            rows: 2
                            //columns: 6
                            columns: 5


                            Item {
                                Layout.row: 0
                                Layout.column: 0
                                Layout.minimumHeight: 40
                                Layout.minimumWidth: 40
                                Layout.maximumHeight: 40
                                Layout.maximumWidth: 40
                                Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
                            }

                            Item {
                                Layout.row: 0
                                Layout.column: 1
                                Layout.minimumHeight: 40
                                Layout.minimumWidth: 40
                                Layout.maximumHeight: 40
                                Layout.maximumWidth: 40
                                Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
                            }

                            Button {
                                id: volumeButton
                                Layout.row: 0
                                Layout.column: 2
                                Layout.maximumHeight: 40
                                Layout.maximumWidth: 40
                                Layout.minimumHeight: 40
                                Layout.minimumWidth: 40
                                //width: 40
                                //height: 40
                                text: qsTr("Vol")
                                icon.source: "qrc:/src/icons2/Media-Controls-High-Volume-icon.png"
                                Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                                //font.wordSpacing: -0.3
                                //Layout.alignment: Qt.AlignHCenter //& Qt.AlignBottom
                                //anchors.bottom: parent.verticalCenter
                                //anchors.horizontalCenter: parent.horizontalCenter
                                property bool isVisible: false
                                onClicked: {
                                    volumeSliderItem.visible = volumeButton.isVisible ? volumeButton.isVisible = false : volumeButton.isVisible = true
                                }
                            }

                            Item {
                                Layout.row: 0
                                Layout.column: 3
                                Layout.minimumHeight: 40
                                Layout.minimumWidth: 40
                                Layout.maximumHeight: 40
                                Layout.maximumWidth: 40
                                Layout.alignment: Qt.AlignLeft | Qt.AlignVCenter
                            }

                            Item {
                                Layout.row: 0
                                Layout.column: 4
                                Layout.minimumHeight: 40
                                Layout.minimumWidth: 40
                                Layout.maximumHeight: 40
                                Layout.maximumWidth: 40
                            }

                            Button {
                                id: rewindVideo
                                Layout.row: 1
                                Layout.column: 0
                                Layout.maximumHeight: 40
                                Layout.maximumWidth: 40
                                Layout.alignment: Qt.AlignRight | Qt.AlignTop
                                width: 40
                                height: 40
                                text: qsTr("Rewind")
                                icon.source: "qrc:/src/icons2/Media-Controls-Rewind-icon.png"
                                //anchors.right: prevVideo.left
                                //anchors.top: volumeButton.bottom

                                onClicked: {
                                    controlVideoItem.rewindVideo(listViewCurrentVideoPlaylist.currentIndex)

                                    //                                    contentWindow.repeater.itemAt(contentWindow.contentVideomanager.displayIndex).myVideo.seek(
                                    //                                                contentWindow.repeater.itemAt(contentWindow.contentVideomanager.displayIndex).myVideo.position - 5000)
                                }
                            }

                            Button {
                                id: prevVideo
                                Layout.row: 1
                                Layout.column: 1
                                Layout.maximumHeight: 40
                                Layout.maximumWidth: 40
                                Layout.alignment: Qt.AlignRight | Qt.AlignTop
                                width: 40
                                height: 40
                                text: qsTr("Prev")
                                icon.source: "qrc:/src/icons2/Media-Controls-Skip-To-Start-icon.png"
                                //anchors.right: startStopVideo.left
                                //anchors.top: volumeButton.bottom

                                onClicked: {
                                    if(listViewCurrentVideoPlaylist.currentIndex >= 0 && listViewCurrentVideoPlaylist.currentIndex < listViewCurrentVideoPlaylist.count)
                                    {
                                        controlVideoItem.prevVideo(listViewCurrentVideoPlaylist.currentIndex)
                                        //                                        contentWindow.repeater.itemAt(contentWindow.contentVideomanager.displayIndex).myVideo.stop()
                                        //                                        listViewVideoPlaylist.currentIndex = listViewVideoPlaylist.currentIndex - 1
                                        //                                        contentWindow.contentVideomanager.displayIndex = displayContentRepeater.contentDisplay
                                        //                                        contentWindow.contentVideomanager.videoIndex = listViewVideoPlaylist.currentIndex
                                    }
                                }
                            }

                            Button {
                                id: startStopVideo
                                Layout.row: 1
                                Layout.column: 2
                                Layout.maximumHeight: 40
                                Layout.maximumWidth: 40
                                Layout.alignment: Qt.AlignHCenter | Qt.AlignTop
                                width: 40
                                height: 40
                                text: qsTr("Start")
                                icon.source: "qrc:/src/icons2/Media-Controls-Play-icon.png"
                                //anchors.horizontalCenter: parent.horizontalCenter
                                //anchors.top: volumeButton.bottom

                                onClicked: {
                                    if(contentWindow.repeater.itemAt(1).myVideo.playbackState === MediaPlayer.PlayingState)
                                    {
                                        console.log("pause video")
                                        controlVideoItem.pausedVideo(listViewCurrentVideoPlaylist.currentIndex)
                                        startStopVideo.icon.source = "qrc:/src/icons2/Media-Controls-Play-icon.png"
                                        //controlVideoItem.sendMessage(listViewVideoPlaylist.currentIndex)
                                        //controlVideoItem.paused == false
                                    }
                                    else  //if(contentWindow.repeater.itemAt(0).myVideo.playbackState == MediaPlayer.PlayingState)
                                    {
                                        console.log("play video")
                                        controlVideoItem.playVideo(listViewCurrentVideoPlaylist.currentIndex)
                                        startStopVideo.icon.source = "qrc:/src/icons2/Media-Controls-Pause-icon.png"
                                        //controlVideoItem.paused == false
                                    }
                                }

                            }

                            Button {
                                id: nextVideo
                                Layout.row: 1
                                Layout.column: 3
                                Layout.maximumHeight: 40
                                Layout.maximumWidth: 40
                                Layout.alignment: Qt.AlignLeft | Qt.AlignTop
                                width: 40
                                height: 40
                                text: qsTr("Next")
                                icon.source: "qrc:/src/icons2/Media-Controls-End-icon.png"
                                //anchors.left: startStopVideo.right
                                //anchors.top: volumeButton.bottom

                                onClicked: {
                                    if(listViewCurrentVideoPlaylist.currentIndex >= 0 && listViewCurrentVideoPlaylist.currentIndex < listViewCurrentVideoPlaylist.count)
                                    {
                                        controlVideoItem.nextVideo(listViewCurrentVideoPlaylist.currentIndex)
                                        //                                        contentWindow.repeater.itemAt(contentWindow.contentVideomanager.displayIndex).myVideo.stop()
                                        //                                        listViewVideoPlaylist.currentIndex = listViewVideoPlaylist.currentIndex + 1
                                        //                                        contentWindow.contentVideomanager.displayIndex = displayContentRepeater.contentDisplay
                                        //                                        contentWindow.contentVideomanager.videoIndex = listViewVideoPlaylist.currentIndex
                                    }
                                }
                            }

                            Button {
                                id: forwardVideo
                                Layout.row: 1
                                Layout.column: 4
                                Layout.maximumHeight: 40
                                Layout.maximumWidth: 40
                                Layout.alignment: Qt.AlignLeft | Qt.AlignTop
                                width: 40
                                height: 40
                                text: qsTr("Forward")
                                icon.source: "qrc:/src/icons2/Media-Controls-Fast-Forward-icon.png"
                                //anchors.left: nextVideo.right
                                //anchors.top: volumeButton.bottom

                                onClicked: {
                                    controlVideoItem.forwardVideo(listViewCurrentVideoPlaylist.currentIndex)
                                    //                                    contentWindow.repeater.itemAt(contentWindow.contentVideomanager.displayIndex).myVideo.seek(
                                    //                                                contentWindow.repeater.itemAt(contentWindow.contentVideomanager.displayIndex).myVideo.position + 5000)
                                }
                            }
                        }
                    }
                }

                Item {
                    id: savePlaylistItem
                    //color: "yellow"
                    //Layout.fillHeight: true
                    Layout.fillWidth: true
                    Layout.row: 2
                    Layout.column: 0
                    Layout.columnSpan: 3
                    height: 25
                    //Layout.maximumWidth: 200

                    Button {
                        id: savePlaylistButton
                        width: 60
                        height: 25
                        anchors.bottom: savePlaylistItem.bottom
                        anchors.right: savePlaylistItem.right
                        //anchors.horizontalCenter: saveItem.horizontalCenter
                        text: qsTr("Save")
                        font.pointSize: 10

                        onClicked: {
                            //editorPlaylist.saveCurrentSession()
                            sessionController.saveSession()
                            saveMessageBox.open()
//                            if(editorPlaylist.currentDisplayWorkarea.saveToFile())
//                            {
//                                saveMessageBox.open();

//                            }
                        }
                    }

                    Button {
                        id: contWindow
                        //                                Layout.row: 1
                        //                                Layout.column: 5
                        //                                Layout.maximumHeight: 40
                        //                                Layout.maximumWidth: 60
                        width: 120
                        height: 25
                        text: qsTr("Content window")
                        anchors.bottom: savePlaylistItem.bottom
                        anchors.right: savePlaylistButton.left
                        anchors.rightMargin: 10
                        font.pointSize: 10
                        //anchors.right: videoControlFrame.right
                        //anchors.bottom: videoControlFrame.bottom
                        //anchors.right: parent.right
                        //anchors.bottom: parent.bottom

                        onClicked: {
                            contentWindow.visible == true ? contentWindow.visible = false : contentWindow.visible = true
                        }
                    }

                }
            }
        }
    }

    Connections {
        //target: videoPlaylistItem
        target: sessionController
        onCurrentSessionLoaded : {
            for(var i = 0; i < sessionController.displayWorkarea.model.rowCount(); i++)
            {
                console.log("i " + i)
                if(sessionController.displayWorkarea.model.get(i).videoPlaylist.size() > 0)
                {
                    //console.log("videoplaylist not empty")
                    //console.log(sessionController.displayWorkarea.model.get(i).videoPlaylist.size() )
                    contentWindow.repeater.itemAt(i).myVideo.playlist.removeItems(
                                0, contentWindow.repeater.itemAt(i).myVideo.playlist.itemCount)

                    contentWindow.repeater.itemAt(i).myVideo.playlist.addItems(
                                sessionController.displayWorkarea.model.getSourcesForPlaylist(i))
                }
                console.log("Item count " + contentWindow.repeater.itemAt(i).myVideo.playlist.itemCount)
            }
        }
    }

    onVisibleChanged:{
        if (visible)
        {
            //console.log("contentWindow.visible " + contentWindow.visible)
            //console.log("contentWindow.width " + contentWindow.width)
            //console.log("onVisibleChanged")
            contentWindow.show();
            contentWindow.visible = true
            //contentWindow.show();
            //contentWindow.close();
            //contentWindow.show();
            //console.log("contentWindow.visible " + contentWindow.visible)
            //console.log("contentWindow.width " + contentWindow.width)
            //volumeSlider.value = contentWindow.repeater.itemAt(0).myVideo.volume
            //console.log("contentWindow.repeater.itemAt(0).myVideo.volume " + contentWindow.repeater.itemAt(0).myVideo.volume)
            //volumeSliderText.text = contentWindow.repeater.itemAt(0).myVideo.volume

           // is being displayed
            for(var i = 0; i < sessionController.displayWorkarea.model.rowCount(); i++)
            {
                //console.log("i " + i)
            }
        }
        else
        {
           // is being hidden
        }
    }

    onClosing: {
        for(var a=0; a < contentWindow.repeater.model.rowCount() ; a++) {
            contentWindow.repeater.itemAt(a).myVideo.stop()
        }
        contentWindow.close()
        sessionController.deleteDataFromCurrentSession()
    }

    FileDialog {
        id: fileDialog
        title: "Please choose a file"
        selectFolder: true
        //folder: shortcuts.home
        onAccepted: {
            console.log("You choose: " + fileDialog.fileUrl)
            //sessionController.playlistCollectionModel.playlistCollectionList.appendItem(fileDialog.fileUrl)
            sessionController.playlistCollectionList.appendItem(fileDialog.fileUrl)
        }
        onRejected: {
            console.log("Canceled")
            //Qt.quit()
        }
    }

    ContentWindow {
        id: contentWindow
        title: qsTr("Content")
        //currentDisplayWorkarea: editorPlaylist.currentDisplayWorkarea.displayWorkarea
        currentDisplayWorkarea: sessionController.displayWorkarea
        visible: false

        onVisibleChanged: {
            contentWindow.currentDisplayWorkarea = sessionController.displayWorkarea
        }

        Connections {
                    target: controlVideoItem

                    //onSendMessage: console.log("Hello by Comp 1")
                    onPlayVideo: {
                        //var a = 0;
                        for(var a=0; a < contentWindow.repeater.model.rowCount() ; a++) {
                            console.log("Item count " + contentWindow.repeater.itemAt(a).myVideo.playlist.itemCount)
                            if(contentWindow.repeater.itemAt(a).myVideo.playbackState !== MediaPlayer.PausedState)
                            {
                                contentWindow.repeater.itemAt(a).myVideo.playlist.currentIndex = videoIndex // get(videoIndex).play()

//                                if(a === 0)
//                                {
//                                    contentWindow.repeater.itemAt(a).myVideo.volume = 100
//                                }
//                                else
//                                {
//                                    contentWindow.repeater.itemAt(a).myVideo.volume = 0
//                                }
                            }
                            contentWindow.repeater.itemAt(a).myVideo.play()
                        }
                    }

                    onPausedVideo: {
                        //var a = 0;
                        for(var a=0; a < contentWindow.repeater.model.rowCount() ; a++) {
                            console.log("paused video")
                            contentWindow.repeater.itemAt(a).myVideo.pause()
                        }
                    }

                    onRewindVideo: {
                        //var a = 0;
                        for(var a=0; a < contentWindow.repeater.model.rowCount() ; a++) {
                            //console.log(displaysWorkarea.model.rowCount())
                            console.log("rewind video")
                            contentWindow.repeater.itemAt(a).myVideo.seek(
                                        contentWindow.repeater.itemAt(a).myVideo.position - 5000)

//                            contentWindow.repeater.itemAt(contentWindow.contentVideomanager.displayIndex).myVideo.seek(
//                                        contentWindow.repeater.itemAt(contentWindow.contentVideomanager.displayIndex).myVideo.position - 5000)
                        }
                    }

                    onForwardVideo: {
                        //var a = 0;
                        for(var a=0; a < contentWindow.repeater.model.rowCount() ; a++) {
                            //console.log(displaysWorkarea.model.rowCount())
                            console.log("forward video")
                            contentWindow.repeater.itemAt(a).myVideo.seek(
                                        contentWindow.repeater.itemAt(a).myVideo.position + 5000)
                        }
                    }

                    onPrevVideo: {
                        for(var a=0; a < contentWindow.repeater.model.rowCount() ; a++) {
                            contentWindow.repeater.itemAt(a).myVideo.stop()
                            startStopVideo.icon.source = "qrc:/src/icons2/Media-Controls-Play-icon.png"
                            //contentWindow.repeater.itemAt(a).myVideo.playlist.currentIndex -= 1
                        }
                        if(listViewCurrentVideoPlaylist.currentIndex > 0)
                        {
                            listViewCurrentVideoPlaylist.currentIndex = listViewCurrentVideoPlaylist.currentIndex - 1
                            contentWindow.contentVideomanager.displayIndex = displayContentRepeater.contentDisplay
                            contentWindow.contentVideomanager.videoIndex = listViewCurrentVideoPlaylist.currentIndex
                        }
                    }

                    onNextVideo: {
                        for(var a=0; a < contentWindow.repeater.model.rowCount() ; a++) {
                            contentWindow.repeater.itemAt(a).myVideo.stop()
                            startStopVideo.icon.source = "qrc:/src/icons2/Media-Controls-Play-icon.png"
                            //contentWindow.repeater.itemAt(a).myVideo.playlist.currentIndex += 1
                        }
                        if(listViewCurrentVideoPlaylist.currentIndex < listViewCurrentVideoPlaylist.model.rowCount()-1)
                        {
                            listViewCurrentVideoPlaylist.currentIndex = listViewCurrentVideoPlaylist.currentIndex + 1
                            contentWindow.contentVideomanager.displayIndex = displayContentRepeater.contentDisplay
                            contentWindow.contentVideomanager.videoIndex = listViewCurrentVideoPlaylist.currentIndex
                        }
                    }
                }

        Connections {
            //target: videoPlaylistItem
            target: currentVideoPlaylistItem
            onVideoLoop : {
                for(var a=0; a < contentWindow.repeater.model.rowCount() ; a++) {
                    if(isLoop)
                        contentWindow.repeater.itemAt(a).myVideo.loops = MediaPlayer.Infinite
                    else
                        contentWindow.repeater.itemAt(a).myVideo.loops = 1
                }
            }

            onDeleteVideo: {
                //editorPlaylist.currentDisplayWorkarea.model.get(displayContentRepeater.contentDisplay).videoPlaylist
                //var playlistIndex = editorPlaylist.currentDisplayWorkarea.deleteHighlightFromPlaylist(displayContentRepeater.contentDisplay, index, editorPlaylist.currentPlaylistCollection)

                //var myListItemObject = listViewVideoPlaylist.contentItem.children[playlistIndex]
                //console.log("myListItemObject videoIndex in playlist " + myListItemObject.mySubItemsRect.currentVideoIndex)

                //var videoIndex = listViewVideoPlaylist.itemAtIndex(playlistIndex).mySubItemsRect.currentVideoIndex
                for(var a=0; a < contentWindow.repeater.model.rowCount() ; a++) {
                    contentWindow.repeater.itemAt(a).myVideo.stop()
                    startStopVideo.icon.source = "qrc:/src/icons2/Media-Controls-Play-icon.png"
                }

                var videoIndex = listViewVideoPlaylist.contentItem.children[index].mySubItemsRect.currentVideoIndex
                console.log("videoIndex in playlist " + videoIndex)

                //console.log("backgroundColor " + listViewVideoPlaylist.contentItem.children[index].mySubItemsRect.mySubItemRepeater.itemAt(videoIndex).backgroundColor)
                listViewVideoPlaylist.contentItem.children[index].mySubItemsRect.mySubItemRepeater.itemAt(videoIndex).backgroundColor = "white"
                listViewVideoPlaylist.contentItem.children[index].mySubItemsRect.currentVideoIndex = -1

                sessionController.deleteVideo(index)

                //listViewVideoPlaylist.itemAt(parseInt(playlistIndex)).mySubItemsRect.mySubItemRepeater.itemAt(videoIndex).backgroundColor = "white"
                //listViewVideoPlaylist.itemAt(parseInt(playlistIndex)).mySubItemsRect.currentVideoIndex = -1

                contentWindow.repeater.itemAt(displayContentRepeater.contentDisplay).myVideo.playlist.removeItems(
                            0, contentWindow.repeater.itemAt(displayContentRepeater.contentDisplay).myVideo.playlist.itemCount)

                //console.log("remove from playlist")
                //console.log("Items count after remove " + contentWindow.repeater.itemAt(displayContentRepeater.contentDisplay).myVideo.playlist.itemCount)

                contentWindow.repeater.itemAt(displayContentRepeater.contentDisplay).myVideo.playlist.addItems(
                            sessionController.getSourcesForPlaylist())

                //listViewCurrentVideoPlaylist.model.updatePlaylist()
            }

            onItemUp: {
                console.log("index " + index)

                for(var a=0; a < contentWindow.repeater.model.rowCount() ; a++) {
                    contentWindow.repeater.itemAt(a).myVideo.stop()
                    startStopVideo.icon.source = "qrc:/src/icons2/Media-Controls-Play-icon.png"
                }

                sessionController.itemPosUp(index)

                contentWindow.repeater.itemAt(displayContentRepeater.contentDisplay).myVideo.playlist.removeItems(
                            0, contentWindow.repeater.itemAt(displayContentRepeater.contentDisplay).myVideo.playlist.itemCount)
                console.log("remove from playlist")
                console.log("Items count after remove " + contentWindow.repeater.itemAt(displayContentRepeater.contentDisplay).myVideo.playlist.itemCount)

                contentWindow.repeater.itemAt(displayContentRepeater.contentDisplay).myVideo.playlist.addItems(
                            sessionController.getSourcesForPlaylist())

                console.log("Items count after add " + contentWindow.repeater.itemAt(displayContentRepeater.contentDisplay).myVideo.playlist.itemCount)

                listViewCurrentVideoPlaylist.model.updatePlaylist()
            }

            onItemDown: {
                console.log("index " + index)
                for(var a=0; a < contentWindow.repeater.model.rowCount() ; a++) {
                    contentWindow.repeater.itemAt(a).myVideo.stop()
                    startStopVideo.icon.source = "qrc:/src/icons2/Media-Controls-Play-icon.png"
                }

                sessionController.itemPosDown(index)

                contentWindow.repeater.itemAt(displayContentRepeater.contentDisplay).myVideo.playlist.removeItems(
                            0, contentWindow.repeater.itemAt(displayContentRepeater.contentDisplay).myVideo.playlist.itemCount)
                console.log("remove from playlist")
                console.log("Items count after remove " + contentWindow.repeater.itemAt(displayContentRepeater.contentDisplay).myVideo.playlist.itemCount)

                contentWindow.repeater.itemAt(displayContentRepeater.contentDisplay).myVideo.playlist.addItems(
                            sessionController.getSourcesForPlaylist())

                console.log("Items count after add " + contentWindow.repeater.itemAt(displayContentRepeater.contentDisplay).myVideo.playlist.itemCount)

                listViewCurrentVideoPlaylist.model.updatePlaylist()
            }
        }

        Connections {
            target: contentWindow.repeater
            onModelChanged: {
                console.log("contentWindow.repeater model changed")
            }
        }

        onNextVideoOnPlaylist: {
            //console.log("track onStopped")
            // && videoPlaylistItem.isLoop !== true
            if(videoPlaylistItem.isLoop !== true)
            {
                //console.log("status changed")
                currentVideoPlaylistItem.currentIndexForAllPlaylists = currentVideoPlaylistItem.currentIndexForAllPlaylists+1
                listViewCurrentVideoPlaylist.currentIndex = listViewCurrentVideoPlaylist.currentIndex + 1
            }
        }

        onCurrentPlaylistIndexChanged: {
            for(var i=0; i < sessionController.displayWorkarea.model.rowCount(); i++)
            {
                contentWindow.repeater.itemAt(i).myVideo.playlist.currentIndex = currentVideoPlaylistItem.currentIndexForAllPlaylists
            }
        }
    }


    MessageDialog {
        id: saveMessageBox
        title: "Save"
        icon: StandardIcon.NoIcon
        text: "Current configuration save to file"// + sessionController.currentSessionFileName()
        standardButtons: StandardButton.Ok
        //Component.onCompleted: visible = true
        onAccepted: {
            saveMessageBox.close()
            //visible = false
        }
    }

}















































































/*##^## Designer {
    D{i:0;height:700;width:1300}D{i:39;anchors_width:40}D{i:38;anchors_width:40}D{i:40;anchors_width:40}
D{i:42;anchors_width:40}
}
 ##^##*/
