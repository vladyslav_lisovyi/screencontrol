import QtQuick 2.0
import QtQuick.Controls 2.5
import QtQuick.Window 2.3
import QtMultimedia 5.12
import QtGraphicalEffects 1.0

import VideoManager 1.0

ApplicationWindow {
    id: contentWindow

    signal closeContentWindow

    signal nextVideoOnPlaylist
    signal currentPlaylistIndexChanged

    //property alias contentLeftPosition : contentRect.anchors.leftMargin
    //property alias contentID: contentRect.color
    property int leftPosition: 20

    property int displayContentIndex: -1
    property int videoContentIndex: -1
    property int a: -1
    property alias contentVideomanager: videomanagerId
    property alias repeater: contentRepeater

    property variant currentDisplayWorkarea

    //property variant video: contentRepeater.itemAt(videomanagerId.displayIndex)

    //property alias video: videoDelegate.videoDel

    //minimumHeight: 600
    //minimumWidth: 800
    //x: 0
    //y: 0

    height: contentWindow.currentDisplayWorkarea.contentWindowResolution.height
    width: contentWindow.currentDisplayWorkarea.contentWindowResolution.width
    minimumHeight: contentWindow.currentDisplayWorkarea.contentWindowResolution.height
    minimumWidth: contentWindow.currentDisplayWorkarea.contentWindowResolution.width
    x: contentWindow.currentDisplayWorkarea.startPos.x
    y: contentWindow.currentDisplayWorkarea.startPos.y
    flags: Qt.Window | Qt.FramelessWindowHint

    //visibility: "Maximized"

    color: "black"

    Videomanager {
        id: videomanagerId
        displayIndex: -1
        videoIndex: -1
        //displayIndex: displaysWorkarea.model.getDisplayIndexFromFile()
        //videoIndex: displaysWorkarea.model.getVideoindexFromFile()
    }


    Repeater {
        id: contentRepeater
        anchors.fill: parent
        clip: true

        //model: displaysWorkarea.model
        //model: currentDisplayWorkarea.model
        //model: sessionController.displayWorkarea.model
        model: sessionController.displayWorkarea.model

        delegate: Item {
            id: videoItem
            property alias myVideo: video

            Video {
                id: video
                //x: (model.position.x - contentWindow.currentDisplayWorkarea.startPos.x)
                //y: (model.position.y - contentWindow.currentDisplayWorkarea.startPos.y)
                x: (model.position.x - sessionController.displayWorkarea.startPos.x)
                y: (model.position.y - sessionController.displayWorkarea.startPos.y)
                width : model.resolution.width
                height : model.resolution.height

                playlist: Playlist {}
                //playlist: Playlist.addItems(displaysWorkarea.model.playlist())
                //playlist: displaysWorkarea.model.playlist()
                //source: displaysWorkarea.model.playlistModel(index).get(videomanagerId.videoIndex).source
                volume: model.index === 1 ? 100 : 0

                onStatusChanged: {
                    if(status == MediaPlayer.EndOfMedia)
                        contentWindow.nextVideoOnPlaylist()
                }


                layer.enabled: true

                OpacityMask {
                    source: video
                    maskSource: maskRight //videoRect //mask
                }

                LinearGradient {
                    id: maskRight
                    //anchors.fill: video
                    x: video.width - model.blendingRight
                    height: parent.height
                    width: model.blendingRight
                    opacity: model.blendingOpacityRight

                    //width: parent.height
                    //height: parent.width
                    //anchors.centerIn: parent
                    //rotation: 90

                    start: Qt.point(0, video.height/2) // arg1 0
                    end: Qt.point(maskRight.width, video.height/2) // arg1 video.width - mask.width

                    gradient: Gradient {
                        GradientStop { position: 0.0; color: "transparent"} // 0.0
                        GradientStop { position: model.blendingPositionRight; } // 1.0 // 0.1
                        //GradientStop { position: 1.0; color: "transparent" }
                    }
                }

                OpacityMask {
                    source: video
                    maskSource: maskLeft //videoRect //mask
                }

                LinearGradient {
                    id: maskLeft
                    //anchors.fill: video
                    height: parent.height
                    width: model.blendingLeft
                    opacity: model.blendingOpacityLeft

                    //width: parent.height
                    //height: parent.width
                    //anchors.centerIn: parent
                    //rotation: 90

                    start: Qt.point(0, video.height/2)
                    end: Qt.point(maskLeft.width, video.height/2)

                    gradient: Gradient {
                        GradientStop { position: model.blendingPositionLeft; } // 0.0 // 0,9
                        GradientStop { position: 1.0; color: "transparent"} // 1.0
                        //GradientStop { position: 1.0; color: "transparent" }
                    }
                }

                OpacityMask {
                    source: video
                    maskSource: maskTop //videoRect //mask
                }

                LinearGradient {
                    id: maskTop
                    //anchors.fill: video
                    height: model.blendingTop
                    width: parent.width
                    opacity: model.blendingOpacityTop

                    //width: parent.height
                    //height: parent.width
                    //anchors.centerIn: parent
                    //rotation: 90

                    start: Qt.point(video.width/2, 0)
                    end: Qt.point(video.width/2, maskTop.height)

                    gradient: Gradient {
                        GradientStop { position: model.blendingPositionTop; } // 0.0 // 0.9
                        GradientStop { position: 1.0; color: "transparent"} // 1.0
                        //GradientStop { position: 1.0; color: "transparent" }
                    }
                }

                OpacityMask {
                    source: video
                    maskSource: maskBottom //videoRect //mask
                }

                LinearGradient {
                    id: maskBottom
                    //anchors.fill: video
                    y: video.height - model.blendingBottom //400
                    height: model.blendingBottom
                    width: parent.width
                    opacity: model.blendingOpacityBottom

                    //width: parent.height
                    //height: parent.width
                    //anchors.centerIn: parent
                    //rotation: 90

                    start: Qt.point(video.width/2, 0)
                    end: Qt.point(video.width/2, maskBottom.height)

                    gradient: Gradient {
                        GradientStop { position: 0.0; color: "transparent"} // 0.0
                        GradientStop { position: model.blendingPositionBottom; } // 1.0 // 0.1
                        //GradientStop { position: 1.0; color: "transparent" }
                    }
                }
            }
        }
    }

    onClosing: {
        for(var a=0; a < contentRepeater.model.rowCount() ; a++)
        {
            contentRepeater.itemAt(a).myVideo.stop()
        }
    }

}
