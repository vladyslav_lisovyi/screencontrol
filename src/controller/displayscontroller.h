#ifndef DISPLAYSCONTROLLER_H
#define DISPLAYSCONTROLLER_H

#include <QObject>
#include "src/model/displaysworkarea.h"

class DisplaysController : public QObject
{
    Q_OBJECT
    Q_PROPERTY(DisplaysWorkarea* displayWorkarea READ displayWorkarea WRITE setDisplayWorkarea NOTIFY displayWorkareaChanged)
    Q_PROPERTY(DisplayCell* currentDisplay READ currentDisplay NOTIFY currentDisplayChanged)
    Q_PROPERTY(VideoPlaylist* currentVideoPlaylist READ currentVideoPlaylist NOTIFY currentVideoPlaylistChanged) // WRITE setVideoPlaylist
    Q_PROPERTY(QAbstractItemModel* currentDisplayResolutionList READ currentDisplayResolutionList NOTIFY currentDisplayResolutionListChanged)
    Q_PROPERTY(int topPosition READ topPos)
public:
    //explicit DisplaysController(QObject *parent = nullptr);
    DisplaysController();
    ~DisplaysController();
    DisplaysController(DisplaysWorkarea* displayWorkarea);
    DisplaysController(const DisplaysController& displaysController);
    DisplaysController& operator=(const DisplaysController& displaysController);

    DisplaysWorkarea* displayWorkarea() const { return _displaysWorkarea.get(); }
    DisplayCell* currentDisplay() const { return _currentDisplay.get(); }
    VideoPlaylist* currentVideoPlaylist() const { return _currentVideoplaylist.get(); }
    QAbstractItemModel* currentDisplayResolutionList() const { return _currentDisplayResolutionList.get(); }

    int topPos() const {return _currentDisplay->position().top();}

    Q_INVOKABLE
    int resolutionIndex();

    Q_INVOKABLE
    QAbstractItemModel* resolutionList();

    Q_INVOKABLE
    void addVideo(QString src);
    Q_INVOKABLE
    void deleteVideo(int videoIndex);
    Q_INVOKABLE
    void changeVideoInList(QString src);
    Q_INVOKABLE
    QList<QUrl> getSourcesForPlaylist();
    Q_INVOKABLE
    void itemPosUp(int videoIndex);
    Q_INVOKABLE
    void itemPosDown(int videoIndex);

    Q_INVOKABLE
    void changePositionToTop(int offset);

signals:
    void displayWorkareaChanged();
    void currentDisplayChanged();
    void currentVideoPlaylistChanged();
    void currentDisplayResolutionListChanged();

public slots:
    void setDisplayWorkarea(DisplaysWorkarea* displayWorkarea);
    void selectDisplay(int displayIndex);
    //void setVideoPlaylist(VideoPlaylist* playlist);

private:
    QSharedPointer<DisplaysWorkarea> _displaysWorkarea;
    QSharedPointer<DisplayCell> _currentDisplay;
    QSharedPointer<VideoPlaylist> _currentVideoplaylist;
    QSharedPointer<QAbstractItemModel> _currentDisplayResolutionList;
};

#endif // DISPLAYSCONTROLLER_H
