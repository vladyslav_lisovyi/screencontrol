#include "playlistcollectioncontroller.h"

//PlaylistCollectionController::PlaylistCollectionController(QObject *parent) : QObject(parent)
//{

//}

PlaylistCollectionController::PlaylistCollectionController()
{
    this->_currentPlaylistCollection = QSharedPointer<PlaylistCollectionList>(new PlaylistCollectionList());
    //_currentPlaylistCollection = new PlaylistCollectionList();
}

PlaylistCollectionController::PlaylistCollectionController(PlaylistCollectionList *playlistCollection)
{
    this->_currentPlaylistCollection = QSharedPointer<PlaylistCollectionList>(new PlaylistCollectionList());
    //this->_currentPlaylistCollection = playlistCollection;
    setPlaylistCollection(playlistCollection);
}

PlaylistCollectionController::~PlaylistCollectionController()
{
    _currentPlaylistCollection.clear();
    //delete  _currentPlaylistCollection;
}

PlaylistCollectionController::PlaylistCollectionController(const PlaylistCollectionController &playlistCollectionController)
{
    this->_currentPlaylistCollection = playlistCollectionController._currentPlaylistCollection;
    emit playlistCollectionChanged();
}

PlaylistCollectionController &PlaylistCollectionController::operator=(const PlaylistCollectionController &playlistCollectionController)
{
    if(this == &playlistCollectionController)
        return *this;
    this->_currentPlaylistCollection = playlistCollectionController._currentPlaylistCollection;
    emit playlistCollectionChanged();
    return *this;
}

void PlaylistCollectionController::setPlaylistCollection(PlaylistCollectionList *playlistCollection)
{
    this->_currentPlaylistCollection = QSharedPointer<PlaylistCollectionList>(playlistCollection);//(new PlaylistCollectionList(*playlistCollection));
    //this->_currentPlaylistCollection = playlistCollection;
    emit playlistCollectionChanged();
}

