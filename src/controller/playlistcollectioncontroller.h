#ifndef PLAYLISTCOLLECTIONCONTROLLER_H
#define PLAYLISTCOLLECTIONCONTROLLER_H

#include <QObject>
#include <QSharedPointer>
#include "src/model/playlistcollectionlist.h"

class PlaylistCollectionController : public QObject
{
    Q_OBJECT
    Q_PROPERTY(PlaylistCollectionList* playlistCollectionList READ playlistCollection WRITE setPlaylistCollection NOTIFY playlistCollectionChanged)
public:
    //explicit PlaylistCollectionController(QObject *parent = nullptr);
    PlaylistCollectionController();
    PlaylistCollectionController(PlaylistCollectionList* playlistCollection);
    ~PlaylistCollectionController();
    PlaylistCollectionController(const PlaylistCollectionController& playlistCollectionController);
    PlaylistCollectionController& operator=(const PlaylistCollectionController& playlistCollectionController);

    PlaylistCollectionList* playlistCollection() const { return _currentPlaylistCollection.data(); }
    //PlaylistCollectionList* playlistCollection() const { return _currentPlaylistCollection; }

signals:
    void playlistCollectionChanged();

public slots:
    void setPlaylistCollection(PlaylistCollectionList* playlistCollection);

private:
    QSharedPointer<PlaylistCollectionList> _currentPlaylistCollection;
    //PlaylistCollectionList* _currentPlaylistCollection;
};

#endif // PLAYLISTCOLLECTIONCONTROLLER_H
