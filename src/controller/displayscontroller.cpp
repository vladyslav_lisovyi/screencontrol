#include "displayscontroller.h"

//DisplaysController::DisplaysController(QObject *parent) : QObject(parent)
//{

//}

DisplaysController::DisplaysController()
{
    this->_displaysWorkarea = nullptr;
    this->_currentDisplay = QSharedPointer<DisplayCell>(new DisplayCell());
    this->_currentVideoplaylist = QSharedPointer<VideoPlaylist>(new VideoPlaylist());
    this->_currentDisplayResolutionList = QSharedPointer<QAbstractItemModel>(nullptr);
}

DisplaysController::~DisplaysController()
{
    this->_displaysWorkarea.clear();
    this->_currentDisplay.clear();
    this->_currentVideoplaylist.clear();
    this->_currentDisplayResolutionList.clear();
}

DisplaysController::DisplaysController(DisplaysWorkarea *displayWorkarea)
{
    setDisplayWorkarea(displayWorkarea);
    this->_currentDisplay = nullptr;
    this->_currentVideoplaylist = nullptr;
    this->_currentDisplayResolutionList = nullptr;
}

DisplaysController::DisplaysController(const DisplaysController &displaysController)
{
    _displaysWorkarea = displaysController._displaysWorkarea;
    _currentDisplay = displaysController._currentDisplay;
    _currentVideoplaylist = displaysController._currentVideoplaylist;
    _currentDisplayResolutionList = displaysController._currentDisplayResolutionList;
    emit displayWorkareaChanged();
}

DisplaysController &DisplaysController::operator=(const DisplaysController &displaysController)
{
    if(this == &displaysController)
        return *this;
    _displaysWorkarea = displaysController._displaysWorkarea;
    _currentDisplay = displaysController._currentDisplay;
    _currentVideoplaylist = displaysController._currentVideoplaylist;
    _currentDisplayResolutionList = displaysController._currentDisplayResolutionList;
    emit displayWorkareaChanged();
    return *this;
}

int DisplaysController::resolutionIndex()
{
    if(_currentDisplay->displayResolutionList()->rowCount() == 0)
        return -1;
    return _currentDisplay->getResolutionIndex();
}

QAbstractItemModel *DisplaysController::resolutionList()
{
    if(_currentDisplay->getDisplayNumber() == -1)
        return new DisplayResolutionList();
    return _currentDisplay->displayResolutionList().get();
}

void DisplaysController::addVideo(QString src)
{
    qDebug() << "DisplaysController::addVideo";
    _currentDisplay->addVideo(src);
    emit currentVideoPlaylistChanged(); // ????????
}

void DisplaysController::deleteVideo(int videoIndex)
{
    qDebug() << "DisplaysController::deleteVideo";
    if(videoIndex >= 0 || videoIndex < _currentDisplay->getPlaylist()->list().size())
        _currentDisplay->removeVideo(videoIndex);
    emit currentVideoPlaylistChanged(); // ????????
}

void DisplaysController::changeVideoInList(QString src)
{
    qDebug() << "DisplaysController::changeVideoInList";
    QStringList splitSource = src.split(QRegExp("[/]+"));
    if(splitSource[0] != "file:")
        src = QString("file:///"+src);
    _currentDisplay->changeVideo(src);
    emit currentVideoPlaylistChanged(); // ????????
}

QList<QUrl> DisplaysController::getSourcesForPlaylist()
{
    QList<QUrl> resultList = {};
    for(int i = 0; i < _currentDisplay->getPlaylist()->list().size(); i++)
    {
        qDebug() << "source: " << _currentDisplay->getPlaylist()->list()[i].getSource();
        QUrl url = QUrl(_currentDisplay->getPlaylist()->list()[i].getSource());
        //qDebug() << "Source from playlist " << item.getPlaylist()->list()[i].getSource();
        //qDebug() << "Url " << url.toString();
        resultList.append(url);
    }
    return resultList;
}

void DisplaysController::itemPosUp(int videoIndex)
{
    qDebug() << "size of currentPlaylist " << _currentDisplay->getPlaylist()->list().size();
    if(videoIndex > 0 || videoIndex < _currentDisplay->getPlaylist()->list().size()-1)
        _currentDisplay->getPlaylist()->upVideo(videoIndex);
    emit currentVideoPlaylistChanged(); // ????????
}

void DisplaysController::itemPosDown(int videoIndex)
{
    qDebug() << "size of currentPlaylist " << _currentDisplay->getPlaylist()->list().size();
    if(videoIndex > 0 || videoIndex < _currentDisplay->getPlaylist()->list().size()-1)
        _currentDisplay->getPlaylist()->downVideo(videoIndex);
    emit currentVideoPlaylistChanged(); // ????????
}

void DisplaysController::changePositionToTop(int offset)
{
    //_currentDisplay.
    QRect oldPosition = _currentDisplay->position();
    oldPosition.moveTo(oldPosition.x(), oldPosition.y()-offset);
    _currentDisplay->setPosition(oldPosition);
   // QRect oldPosition = _displayCell[index].position();
    //oldPosition.moveTo(oldPosition.x(), oldPosition.y()-offset);
    //_displayCell[index].setPosition(oldPosition);
}

void DisplaysController::setDisplayWorkarea(DisplaysWorkarea *displayWorkarea)
{
    this->_displaysWorkarea = QSharedPointer<DisplaysWorkarea>(new DisplaysWorkarea(displayWorkarea)); // FIX IT

    emit displayWorkareaChanged();
}

void DisplaysController::selectDisplay(int displayIndex)
{
    //DisplayCell cell = _displaysWorkarea->model()->displayCells()->displayCells()[displayIndex];
    //this->_currentDisplay = QSharedPointer<DisplayCell>(new DisplayCell(_displaysWorkarea->model()->displayCells()->displayCells()[displayIndex]));
    //DisplayCell cell = _displaysWorkarea->model()->displayCells()->displayCells()[displayIndex];
    this->_currentDisplay = _displaysWorkarea->model()->displayCells()->displayCells()[displayIndex]; // new DisplayCell
    emit currentDisplayChanged();

    this->_currentDisplayResolutionList = _currentDisplay->displayResolutionList();
    emit currentDisplayResolutionListChanged();

    //this->_currentVideoplaylist = QSharedPointer<VideoPlaylist>(new VideoPlaylist(_currentDisplay->getPlaylist().get()));
    //this->_currentVideoplaylist = _currentDisplay->getPlaylist();
    //emit currentVideoPlaylistChanged();
}
