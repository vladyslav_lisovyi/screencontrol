#ifndef SESSIONCONTROLLER_H
#define SESSIONCONTROLLER_H

#include <QObject>
#include "src/model/sessionlist.h"
#include "src/controller/displayscontroller.h"
#include "src/model/playlistcollectionlist.h"
#include "src/model/playlistcollectionmodel.h"

class SessionController : public QObject
{
    Q_OBJECT
    Q_PROPERTY(SessionList* sessionList READ sessionList WRITE setSessionList NOTIFY sessionListChanged)
    Q_PROPERTY(Session* currentSession READ currentSession NOTIFY currentSessionChanged)
    //Q_PROPERTY(DisplaysController* displaysController READ displaysController)
    //Q_PROPERTY(PlaylistCollectionController* playlistCollectionController READ playlistCollectionController)

    Q_PROPERTY(DisplaysWorkarea* displayWorkarea READ displayWorkarea NOTIFY displayWorkareaChanged) // WRITE setDisplayWorkarea
    Q_PROPERTY(DisplayCell* currentDisplay READ currentDisplay NOTIFY currentDisplayChanged)
    Q_PROPERTY(VideoPlaylist* currentVideoPlaylist READ currentVideoPlaylist NOTIFY currentVideoPlaylistChanged) // WRITE setVideoPlaylist
    Q_PROPERTY(QAbstractItemModel* currentDisplayResolutionList READ currentDisplayResolutionList NOTIFY currentDisplayResolutionListChanged)

    Q_PROPERTY(QAbstractItemModel* currentVideoPlaylistModel READ currentVideoPlaylistModel NOTIFY currentVideoPlaylistModelChanged)

    Q_PROPERTY(QAbstractItemModel* playlistCollectionModel READ playlistCollectionModel NOTIFY playlistCollectionModelChanged)
    Q_PROPERTY(PlaylistCollectionList* playlistCollectionList READ playlistCollection NOTIFY playlistCollectionChanged)
    //Q_PROPERTY(QSharedPointer<PlaylistCollectionList> playlistCollectionList READ playlistCollection NOTIFY playlistCollectionChanged)
public:
    //explicit SessionController(QObject *parent = nullptr);
    SessionController();
    ~SessionController();

    Session* currentSession() const;

    SessionList* sessionList() const;

    //DisplaysController* displaysController() const { return _displaysController.data(); }
    //PlaylistCollectionController* playlistCollectionController() const { return _playlistCollectionController.get();}

    ///////----DisplaysController--------------------------------------------------

    DisplaysWorkarea* displayWorkarea() const { return _displaysWorkarea.data(); }
    DisplayCell* currentDisplay() const { return _currentDisplay.data(); }
    VideoPlaylist* currentVideoPlaylist() const { return _currentVideoplaylist.data(); }
    QAbstractItemModel* currentDisplayResolutionList() const { return _currentDisplayResolutionList.data(); }

    VideoPlaylistModel* currentVideoPlaylistModel() const {return _currentVideoplaylistModel.data(); }

    Q_INVOKABLE
    QString currentSessionFileName()
    {
        if(_currentSession || _currentSession->filename() != "")
            return _currentSession->filename();
        return "";
    }

    Q_INVOKABLE
    int resolutionIndex(); // add code

    Q_INVOKABLE
    void addVideo(QString src); // add code
    Q_INVOKABLE
    void deleteVideo(int videoIndex); // add code
    Q_INVOKABLE
    void changeVideoInList(QString src); // add code
    Q_INVOKABLE
    QList<QUrl> getSourcesForPlaylist(); // add code
    Q_INVOKABLE
    void itemPosUp(int videoIndex); // add code
    Q_INVOKABLE
    void itemPosDown(int videoIndex); // add code

    Q_INVOKABLE
    int currentDisplayIndex() { return _displayIndex; }

    ///////------------------------------------------------------

    //////--PlaylistCollectionList-----------------------------------------------------

    QAbstractItemModel* playlistCollectionModel() const { return _playlistCollectionModel.get(); }
    //QSharedPointer<PlaylistCollectionList> playlistCollection() const {return _playlistCollection; }
    PlaylistCollectionList* playlistCollection() const {return _playlistCollection.data(); }

    Q_INVOKABLE
    int getPlaylistCollectionIndexFromCurrentVideoplaylist(QString source); // get index by current video source
    Q_INVOKABLE
    int getPlaylistCollectionIndexFromPlaylistName(QString playlistName); // get index by playlist name

    Q_INVOKABLE
    int getPlaylistCollectionIndex(int playlistIndex); // get index for all playlistCollection by his index in the list

    Q_INVOKABLE
    void deletePlaylist(int index);

    //////-------------------------------------------------------

    Q_INVOKABLE
    void deleteDataFromCurrentSession();

signals:
    void sessionListChanged();
    void currentSessionChanged();

    void displayWorkareaChanged();
    void currentDisplayChanged();
    void currentVideoPlaylistChanged();
    void currentDisplayResolutionListChanged();

    void playlistCollectionChanged();
    void playlistCollectionModelChanged();

    void currentVideoPlaylistModelChanged();

    void currentSessionSaved();
    void currentSessionLoaded();

    void configurationException();
public slots:
    void setSessionList(SessionList* sessionList);
    void setCurrentSession(int indexInSessionList);
    void setNewSession(int indexInSessionList);
    void loadSession(int indexInSessionList); // init currentSession
    void saveSession();
    int addNewSession(QString filename);

    void setDisplayWorkarea(QSharedPointer<DisplaysWorkarea> displayWorkarea); // add code
    void selectDisplay(int displayIndex);

    void setPlaylistCollection(QSharedPointer<PlaylistCollectionList> playlistCollection); // add code

private:
    QSharedPointer<Session> _currentSession;
    QSharedPointer<SessionList> _sessionList;
    //QSharedPointer<DisplaysController> _displaysController; // delete
    //QSharedPointer<PlaylistCollectionController> _playlistCollectionController; // delete

    QSharedPointer<DisplaysWorkarea> _displaysWorkarea;
    QSharedPointer<DisplayCell> _currentDisplay;
    QSharedPointer<VideoPlaylist> _currentVideoplaylist;
    QSharedPointer<QAbstractItemModel> _currentDisplayResolutionList;

    QSharedPointer<PlaylistCollectionModel> _playlistCollectionModel;
    QSharedPointer<PlaylistCollectionList> _playlistCollection;

    QSharedPointer<VideoPlaylistModel> _currentVideoplaylistModel;

    int _displayIndex = -1;
};

#endif // SESSIONCONTROLLER_H
