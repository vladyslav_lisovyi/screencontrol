#include "sessioncontroller.h"

SessionController::SessionController()
{
    //_currentSession = QSharedPointer<Session>(nullptr);
    _sessionList = QSharedPointer<SessionList>(new SessionList());
    bool saves = _sessionList->checkAvailableSaves();
    qDebug() << "Saves available: " << saves;
    _sessionList->loadingSessionsFromFolder();
    _currentSession = nullptr;
    //_displaysController = QSharedPointer<DisplaysController>(new DisplaysController());
    //_playlistCollectionController = QSharedPointer<PlaylistCollectionController>(new PlaylistCollectionController());

    this->_displaysWorkarea = nullptr; //QSharedPointer<DisplaysWorkarea>(new DisplaysWorkarea()); //nullptr;
    this->_currentDisplay = nullptr ;//QSharedPointer<DisplayCell>(new DisplayCell());
    this->_currentVideoplaylist =  nullptr;//QSharedPointer<VideoPlaylist>(new VideoPlaylist());
    this->_currentDisplayResolutionList = nullptr ;// QSharedPointer<QAbstractItemModel>(nullptr);

    //this->_playlistCollection = nullptr;
    this->_playlistCollection = nullptr;//QSharedPointer<PlaylistCollectionList>(new PlaylistCollectionList());
    //this->_playlistCollectionModel = nullptr;

    this->_playlistCollectionModel = nullptr;// QSharedPointer<PlaylistCollectionModel>(new PlaylistCollectionModel());

    this->_currentVideoplaylistModel = nullptr; //QSharedPointer<VideoPlaylistModel>(new VideoPlaylistModel());
}

SessionController::~SessionController()
{
    this->_sessionList.clear();
    this->_currentSession.clear();
    //this->_displaysController.clear();

    this->_displaysWorkarea.clear();
    this->_currentDisplay.clear();
    this->_currentVideoplaylist.clear();
    this->_currentDisplayResolutionList.clear();

    //this->_playlistCollection.clear();
    //this->_playlistCollectionModel.clear();

    this->_currentVideoplaylistModel.clear();
}

Session* SessionController::currentSession() const
{
    return _currentSession.get();
}

void SessionController::loadSession(int indexInSessionList)
{
    if(indexInSessionList >= 0 || indexInSessionList < _sessionList->sessions().size())
    {
        setCurrentSession(indexInSessionList);
        if(_currentSession->displayWorkarea()->model() == nullptr)
        {
            qDebug() << "load from file";
            try
            {
                _currentSession->LoadSettings();
            }
            catch (ConfigurationException &e)
            {
                emit configurationException();
                return;
            }
        }
        setDisplayWorkarea(_currentSession->displayWorkarea());
        setPlaylistCollection(_currentSession->playlistCollectionList());
    }

    emit currentSessionLoaded();
    emit currentSessionChanged();
}

void SessionController::saveSession()
{
    if(!_currentSession || _currentSession == nullptr)
        return;
    _currentSession->SaveSettings();
    emit currentSessionSaved();
}

int SessionController::addNewSession(QString filename)
{
    if(filename != "")
    {
        int newItemIndex = _sessionList->createSave(filename);
        setNewSession(newItemIndex);
        //setCurrentSession(newItemIndex);
        return newItemIndex;
    }
    return false;
}

void SessionController::setDisplayWorkarea(QSharedPointer<DisplaysWorkarea> displayWorkarea)
{
    // add code
    _displaysWorkarea = displayWorkarea;
    _currentSession->setDisplayWorkarea(_displaysWorkarea);
    emit displayWorkareaChanged();
}

void SessionController::selectDisplay(int displayIndex)
{
    _currentDisplay = nullptr;
    //_currentDisplayResolutionList.clear();
    _currentVideoplaylist= nullptr;
    //_currentVideoplaylistModel.clear();

    _currentDisplay = _displaysWorkarea->model()->displayCells()->displayCells()[displayIndex]; // new DisplayCell
    _displayIndex = displayIndex;
    emit currentDisplayChanged();

    _currentDisplayResolutionList = _currentDisplay->displayResolutionList();
    emit currentDisplayResolutionListChanged();

    _currentVideoplaylist = _currentDisplay->getPlaylist();
    emit currentVideoPlaylistChanged();

    _currentVideoplaylistModel = QSharedPointer<VideoPlaylistModel>(new VideoPlaylistModel());
    _currentVideoplaylistModel->setPlaylist(_currentVideoplaylist);
    emit currentVideoPlaylistModelChanged();
}

void SessionController::setPlaylistCollection(QSharedPointer<PlaylistCollectionList> playlistCollection)
{
    // add code
    //_playlistCollection = QSharedPointer<PlaylistCollectionList>(playlistCollection);
    _playlistCollection = playlistCollection;
    emit playlistCollectionChanged();
    _playlistCollectionModel = QSharedPointer<PlaylistCollectionModel>(new PlaylistCollectionModel(), &QObject::deleteLater);
    //_playlistCollectionModel->setPlaylistCollection(_playlistCollection.get());
    _playlistCollectionModel->setPlaylistCollection(_playlistCollection);
    emit playlistCollectionModelChanged();
}

SessionList* SessionController::sessionList() const
{
    return _sessionList.get();
}

int SessionController::resolutionIndex()
{
    if(_currentDisplay->displayResolutionList()->rowCount() == 0)
        return -1;
    return _currentDisplay->getResolutionIndex();
}

void SessionController::addVideo(QString src)
{
    qDebug() << "SessionController::addVideo";
    _currentDisplay->addVideo(src);
    emit currentVideoPlaylistChanged(); // ????????
    emit currentVideoPlaylistModelChanged();
}

void SessionController::deleteVideo(int videoIndex)
{
    qDebug() << "SessionController::deleteVideo";
    if(videoIndex >= 0 || videoIndex < _currentDisplay->getPlaylist()->list().size())
        _currentDisplay->removeVideo(videoIndex);
    emit currentVideoPlaylistChanged(); // ????????
    emit currentVideoPlaylistModelChanged();
}

void SessionController::changeVideoInList(QString src)
{
    qDebug() << "SessionController::changeVideoInList";
    QStringList splitSource = src.split(QRegExp("[/]+"));
    //qDebug() << "splitSource " << splitSource;
    if(splitSource[0] != "file:")
        src = QString("file:///"+src);
    _currentDisplay->changeVideo(src);
    //_currentVideoplaylist = _currentDisplay->getPlaylist();
    emit currentVideoPlaylistChanged(); // ????????
    emit currentVideoPlaylistModelChanged();
}

QList<QUrl> SessionController::getSourcesForPlaylist()
{
    QList<QUrl> resultList = {};
    for(int i = 0; i < _currentDisplay->getPlaylist()->list().size(); i++)
    {
        qDebug() << "source: " << _currentDisplay->getPlaylist()->list()[i].getSource();
        QUrl url = QUrl(_currentDisplay->getPlaylist()->list()[i].getSource());
        //qDebug() << "Source from playlist " << item.getPlaylist()->list()[i].getSource();
        //qDebug() << "Url " << url.toString();
        resultList.append(url);
    }
    return resultList;
}

void SessionController::itemPosUp(int videoIndex)
{
    qDebug() << "size of currentPlaylist " << _currentDisplay->getPlaylist()->list().size();
    if(videoIndex > 0 && videoIndex <= _currentDisplay->getPlaylist()->list().size()-1)
        _currentDisplay->getPlaylist()->upVideo(videoIndex);
    emit currentVideoPlaylistChanged(); // ????????
    emit currentVideoPlaylistModelChanged();
}

void SessionController::itemPosDown(int videoIndex)
{
    qDebug() << "size of currentPlaylist " << _currentDisplay->getPlaylist()->list().size();
    if(videoIndex >= 0 && videoIndex < _currentDisplay->getPlaylist()->list().size()-1)
        _currentDisplay->getPlaylist()->downVideo(videoIndex);
    emit currentVideoPlaylistChanged(); // ????????
    emit currentVideoPlaylistModelChanged();
}

int SessionController::getPlaylistCollectionIndexFromCurrentVideoplaylist(QString source)
{
    if(!_currentVideoplaylist)
        return -1;
    if(_currentVideoplaylist->size() == 0)
        return -1;
    for(int j=0; j < _playlistCollection->playlistColletion().size(); j++)
    {
        QStringList sourceList = source.split(QRegExp("[/]+"));
        QString currentPlaylistName = sourceList[sourceList.size()-1];
        if(currentPlaylistName == _playlistCollection->playlistColletion()[j].playlistName())
        {
            for(int i=0; i < _playlistCollection->playlistColletion()[j].videoplaylist()->list().size(); i++)
            {
                if(_playlistCollection->playlistColletion()[j].videoplaylist()->list()[i].getSource() == source)
                    return i;
            }
        }
    }
}

int SessionController::getPlaylistCollectionIndexFromPlaylistName(QString playlistName)
{
    if(!_currentVideoplaylist)
        return -1;
    if(_currentVideoplaylist->size() == 0)
        return -1;
    for(int j=0; j < _currentVideoplaylist->list().size(); j++)
    {
        QStringList sourceList = _currentVideoplaylist->list()[j].getSource().split(QRegExp("[/]+"));
        QString currentPlaylistName = sourceList[sourceList.size()-2];
        if(playlistName == currentPlaylistName)
        {
            for(int i=0; i < _playlistCollection->playlistColletion()[j].videoplaylist()->list().size(); i++)
            {
                if(_playlistCollection->playlistColletion()[j].videoplaylist()->list()[i].getSource() == _currentVideoplaylist->list()[j].getSource())
                {
                    return i;
                }
            }
        }
    }
    return -1;
}

int SessionController::getPlaylistCollectionIndex(int playlistIndex)
{
    if(!_currentVideoplaylist)
        return -1;
    if(_currentVideoplaylist->size() == 0)
        return -1;
    QString playlistName = _playlistCollection->playlistColletion()[playlistIndex].playlistName();
    for(int i = 0; i < _currentVideoplaylist->size(); i++)
    {
        QStringList sourceList = _currentVideoplaylist->list()[i].getSource().split(QRegExp("[/]+"));
        QString currentPlaylistName = sourceList[sourceList.size()-2];
        if(playlistName == currentPlaylistName)
        {
            for(int j=0; j < _playlistCollection->playlistColletion()[playlistIndex].videoplaylist()->list().size(); j++)
            {
                QString playlistSource = _playlistCollection->playlistColletion()[playlistIndex].videoplaylist()->list()[j].getSource();
                QString videoSource = _currentVideoplaylist->list()[i].getSource();
                if(_playlistCollection->playlistColletion()[playlistIndex].videoplaylist()->list()[j].getSource() == _currentVideoplaylist->list()[i].getSource())
                {
                    return j;
                }
            }
        }
    }
}

void SessionController::deletePlaylist(int index)
{
    if(!_playlistCollection || _playlistCollection->playlistColletion().size() == 0)
        return;
    if(index < 0 || index > _playlistCollection->playlistColletion().size()-1)
        return;

    QString playlistName = _playlistCollection->playlistColletion()[index].playlistName();

    if(!_displaysWorkarea || _displaysWorkarea->model()->displayCells()->displayCells().size() == 0)
        return;
    for(int i = 0; i < _displaysWorkarea->model()->displayCells()->displayCells().size(); i++)
    {
        if(!_displaysWorkarea->model()->displayCells()->displayCells()[i]->getPlaylist() || _displaysWorkarea->model()->displayCells()->displayCells()[i]->getPlaylist()->size() != 0)
        {
            for(int j = 0; j < _displaysWorkarea->model()->displayCells()->displayCells()[i]->getPlaylist()->list().size(); j++)
            {
                QStringList sourceList = _displaysWorkarea->model()->displayCells()->displayCells()[i]->getPlaylist()->list()[j].getSource().split(QRegExp("[/]+"));
                QString sourcePlaylistName = sourceList[sourceList.size()-2];
                if(sourcePlaylistName == playlistName)
                    _displaysWorkarea->model()->displayCells()->displayCells()[i]->getPlaylist()->removeItem(j);
                emit currentVideoPlaylistChanged();
                emit currentVideoPlaylistModelChanged();
            }
        }
    }
    _playlistCollection->removeItem(index);
    emit playlistCollectionChanged();
    emit playlistCollectionModelChanged();
}

void SessionController::deleteDataFromCurrentSession()
{

    _currentVideoplaylistModel.clear();
    emit currentVideoPlaylistModelChanged();

    _currentDisplayResolutionList.clear();
    emit currentDisplayResolutionListChanged();

    _playlistCollectionModel.clear();
    emit playlistCollectionModelChanged();

    _playlistCollectionModel.clear();
    emit playlistCollectionModelChanged();

    _displaysWorkarea.clear();
    emit displayWorkareaChanged();

    _currentSession.clear();
    emit currentSessionChanged();

    //_currentSession = nullptr;
}

void SessionController::setSessionList(SessionList* sessionList)
{
    QSharedPointer<SessionList> list = QSharedPointer<SessionList>(sessionList);
    _sessionList = list;

    emit sessionListChanged();
}

void SessionController::setCurrentSession(int indexInSessionList)
{
    if(indexInSessionList >= 0 || indexInSessionList < _sessionList->sessions().size())
    {
        //Session* currentSession = _sessionList->sessions().at(indexInSessionList);
        //Session> session = QSharedPointer<Session>(new Session(_sessionList->sessions()[indexInSessionList])); // must be new pointer
        _currentSession = _sessionList->sessions()[indexInSessionList];
        //_displaysController = QSharedPointer<DisplaysController>(new DisplaysController());
        //_displaysController->setDisplayWorkarea(_currentSession->displayWorkarea().get()); // _currentSession.displayWorkarea().get()
        //qDebug() << "_displaysController changed";
        //qDebug() << "_displaysController " << _displaysController->displayWorkarea()->

        //_playlistCollectionController->setPlaylistCollection(_currentSession->playlistCollectionList().get());
        //_displaysController = QSharedPointer<DisplaysController>(new DisplaysController(_currentSession->displayWorkarea()));
    }
    emit currentSessionChanged();
}

void SessionController::setNewSession(int indexInSessionList)
{
    if(indexInSessionList >= 0 || indexInSessionList < _sessionList->sessions().size())
    {
        //Session* currentSession = _sessionList->sessions().at(indexInSessionList);
        //Session> session = QSharedPointer<Session>(new Session(_sessionList->sessions()[indexInSessionList])); // must be new pointer

        _currentSession = _sessionList->sessions()[indexInSessionList];
        //_displaysWorkarea = QSharedPointer<DisplaysWorkarea>(new DisplaysWorkarea(true), &QObject::deleteLater);
        //_currentSession->setDisplayWorkarea(_displaysWorkarea);
        //emit displayWorkareaChanged();
        setDisplayWorkarea(QSharedPointer<DisplaysWorkarea>(new DisplaysWorkarea(true), &QObject::deleteLater));
        _currentSession->setDisplayWorkarea(_displaysWorkarea);

        //QSharedPointer<PlaylistCollectionList> list = QSharedPointer<PlaylistCollectionList>(new PlaylistCollectionList());
        //_playlistCollectionModel = QSharedPointer<PlaylistCollectionListModel>(new PlaylistCollectionListModel());
        //qDebug() << "_playlistCollection before clear " << _playlistCollection;
        //_playlistCollection.clear();
        //qDebug() << "SessionController _playlistCollection.clear()";
        //qDebug() << "_playlistCollection after clear " << _playlistCollection;
        //_playlistCollection = QSharedPointer<PlaylistCollectionList>(new PlaylistCollectionList(), &QObject::deleteLater);
        //_playlistCollection->appendItem("file:///C:/jj3/JoJo no Kimyou na Bouken (2015)");
        //QSharedPointer<PlaylistCollectionList> list = QSharedPointer<PlaylistCollectionList>(new PlaylistCollectionList());
        //setPlaylistCollection(list.get());
        //_currentSession->setPlaylistCollectionList(_playlistCollection);
        //emit playlistCollectionChanged();
        //_playlistCollectionModel = QSharedPointer<PlaylistCollectionModel>(new PlaylistCollectionModel(), &QObject::deleteLater);
        //_playlistCollectionModel->setPlaylistCollection(_playlistCollection.get());
        //emit playlistCollectionModelChanged();
        setPlaylistCollection(QSharedPointer<PlaylistCollectionList>(new PlaylistCollectionList(), &QObject::deleteLater));
        _currentSession->setPlaylistCollectionList(_playlistCollection);
    }
    emit currentSessionChanged();
}
