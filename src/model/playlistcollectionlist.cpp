#include "playlistcollectionlist.h"

#include <QDebug>

PlaylistCollectionList::PlaylistCollectionList(QObject *parent) : QObject(parent)
{
    qDebug() << "new explicit playlistcollectionlist";
    _playlistColletion = {};
}

//PlaylistCollectionList::PlaylistCollectionList()
//{
//    qDebug() << "new playlistcollectionlist";
//    _playlistColletion = {};
//}

PlaylistCollectionList::PlaylistCollectionList(const PlaylistCollectionList &playlistCollection)
{
    qDebug() << "copy constr";
    _playlistColletion = playlistCollection._playlistColletion;
    qDebug() << "copy constr end";
}

PlaylistCollectionList &PlaylistCollectionList::operator=(const PlaylistCollectionList &playlistCollection)
{
    qDebug() << "PlaylistCollectionList operator=";
    if(this == &playlistCollection)
        return *this;
    _playlistColletion = playlistCollection._playlistColletion;
    qDebug() << "operator= end";
    return *this;
}

PlaylistCollectionList::~PlaylistCollectionList()
{
    qDebug() << "~PlaylistCollectionList()";
    qDebug() << "PlaylistCollectionList size " << _playlistColletion.size();
    //qDebug() << "PlaylistCollectionList " << _playlistColletion[0].get();
    for(int i=0; i < _playlistColletion.size(); i++)
    {
        qDebug() << "adr: " << &_playlistColletion[i];
        //_playlistColletion[i].clear();
    }
}

bool PlaylistCollectionList::setItemAt(int index, const PlaylistCollection item) // fix
{
    if(index < 0 || index >= this->_playlistColletion.size())
        return false;

    //const Display& oldItem = *this->_displays.at(index);
    const PlaylistCollection oldItem = this->_playlistColletion.at(index);
//    if(oldItem.playlistName() == item.playlistName())
//        return false;

    //this->_displays[index] = QSharedPointer<Display>(new Display(item));
    this->_playlistColletion[index] = item;
    return true;
}

QList<PlaylistCollection> PlaylistCollectionList::playlistColletion() const
{
    return this->_playlistColletion;
}

QString PlaylistCollectionList::getPlaylistName(int index)
{
    return _playlistColletion.at(index).playlistName();
}

void PlaylistCollectionList::initPlaylistCollectionList(PlaylistCollectionList *collection) // fix
{
    if(this == collection)
        return;// *this;

    _playlistColletion = collection->_playlistColletion;
}

int PlaylistCollectionList::sizeOfList()
{
    return _playlistColletion.size();
}

int PlaylistCollectionList::sizeOfVideoplaylist(int playlistCollectionIndex)
{
    if(playlistCollectionIndex < 0 || playlistCollectionIndex > _playlistColletion.size()-1)
        return -1;
    return _playlistColletion[playlistCollectionIndex].videoplaylist()->size();
}

void PlaylistCollectionList::appendItem(QString source)
{
    emit preItemAppended(source);

    QStringList list = source.split(QRegExp("[/]+"));
    if(list[0] == "file:")
        qDebug() << " \"file:\" is exist ";
    QSharedPointer<VideoPlaylist> playlist = QSharedPointer<VideoPlaylist>(new VideoPlaylist());
    playlist->initVideoPlaylistByFolder(source);
    //QSharedPointer<PlaylistCollection> collection = QSharedPointer<PlaylistCollection>(new PlaylistCollection(*playlist.get()));
    PlaylistCollection collection = PlaylistCollection();
    //collection.setPlaylistName(playlist->playlistName());
    collection.setVideoplaylist(playlist);
    _playlistColletion.append(collection);

//    QSharedPointer<VideoPlaylist> playlist = QSharedPointer<VideoPlaylist>(new VideoPlaylist());
//    playlist->initVideoPlaylistByFolder(source);
//    qDebug() << "initVideoPlaylistByFolder";
//    _playlistColletion.append(playlist);
//    qDebug() << "append";

    emit postItemAppended();
//    VideoContent item;
//    item.setSource(file);
//    mList.append(item);

    //emit itemAppended(file);
}

void PlaylistCollectionList::removeItem(int index)
{
    if(index < 0 || index > _playlistColletion.size() - 1)
        return;
    emit preItemRemoved(index);

    _playlistColletion.removeAt(index);

    emit postItemRemoved();
}
