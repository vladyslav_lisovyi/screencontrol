#ifndef PHYSICALDISPLAY_H
#define PHYSICALDISPLAY_H

#include <QString>
#include <QObject>
#include <Windows.h>

class PhysicalDisplay
{
    Q_GADGET
    Q_PROPERTY(QString deviceID READ getDeviceId)
public:
    PhysicalDisplay();
    ~PhysicalDisplay() {}
    PhysicalDisplay(DISPLAY_DEVICE adapter, DISPLAY_DEVICE device);
    PhysicalDisplay(const PhysicalDisplay& display);
    QString getDeviceId() const;
    QString getDeviceName() const;
    QString getDeviceString() const;
    QString getDeviceKey() const;
    unsigned long getDeviceState() const;
    DISPLAY_DEVICE getDisplayDevice() const;
    DISPLAY_DEVICE getAdapterDevice() const;

    void setDeviceId(QString deviceId);
    void setDeviceName(QString deviceName);
    void setDeviceString(QString deviceString);
    void setDeviceKey(QString deviceKey);
    void setDeviceState(unsigned long deviceState);
    void setDisplayDevice(DISPLAY_DEVICE device);
    void setAdapterDevice(DISPLAY_DEVICE adapter);
private:
    QString _deviceId;
    QString _deviceName;
    QString _deviceString;
    QString _deviceKey;
    unsigned long _deviceState;
    DISPLAY_DEVICE _display_device;
    DISPLAY_DEVICE _adapter_device;
};

#endif // PHYSICALDISPLAY_H
