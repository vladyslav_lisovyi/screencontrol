#include "displayworkareamodel.h"

#include <QDebug>

DisplayWorkareaModel::DisplayWorkareaModel(QObject *parent)
    : QAbstractListModel(parent)
{
    _displayCells = QSharedPointer<DisplayCells>(new DisplayCells());
}

DisplayWorkareaModel::DisplayWorkareaModel(int size, QObject *parent): QAbstractListModel(parent)
{
    _displayCells = QSharedPointer<DisplayCells>(new DisplayCells(size));
}

DisplayWorkareaModel::~DisplayWorkareaModel()
{
    qDebug() << "~DisplayWorkareaModel";
    _displayCells.clear();
}

int DisplayWorkareaModel::rowCount(const QModelIndex &parent) const
{
    // For list models only the root node (an invalid parent) should return the list's size. For all
    // other (valid) parents, rowCount() should return 0 so that it does not become a tree model.
    if (parent.isValid())
        return 0;

    // FIXME: Implement me!
    return _displayCells->displayCells().size();
}

QVariant DisplayWorkareaModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid() || !_displayCells)
        return QVariant();

    // FIXME: Implement me!
    // return QVariant((item.display()->getPhysicalDisplay()->getDeviceId()));
    const QSharedPointer<DisplayCell> item = _displayCells->displayCells()[index.row()];
    //qDebug() << "display number " << item.getDisplayNumber();
    switch (role) {
    case Position:
        return QVariant(item->position());
    case Resolution:
        return QVariant(item->resolution());
    case availableDisplay:
        return QVariant(item->display());
    case idDisplay:
        return QVariant((item->getDisplayNumber()));
    case physID:
        return QVariant((item->display()->getPhysicalDisplay()->getDeviceId()));
    case blendingTop:
        return QVariant(item->BlendingTop());
    case blendingLeft:
        return QVariant(item->BlendingLeft());
    case blendingRight:
        return QVariant(item->BlendingRight());
    case blendingBottom:
        return QVariant(item->BlendingBottom());

    case blendingPositionTop:
        return QVariant(item->getBlendingPositionTop());
    case blendingPositionBottom:
        return QVariant(item->getBlendingPositionBottom());
    case blendingPositionLeft:
        return QVariant(item->getBlendingPositionLeft());
    case blendingPositionRight:
        return QVariant(item->getBlendingPositionRight());

    case blendingOpacityTop:
        return QVariant(item->getBlendingOpacityTop());
    case blendingOpacityBottom:
        return QVariant(item->getBlendingOpacityBottom());
    case blendingOpacityLeft:
        return QVariant(item->getBlendingOpacityLeft());
    case blendingOpacityRight:
        return QVariant(item->getBlendingOpacityRight());

    case videoPlaylist:
        return QVariant::fromValue(item->getPlaylist().get());

    case primaryScreen:
        return QVariant(item->primaryScreen());
    }
    return QVariant();
}

bool DisplayWorkareaModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if(!_displayCells)
        return false;

    QSharedPointer<DisplayCell> item = _displayCells->displayCells().at(index.row());
    switch (role) {
    case Position:
        item->setPosition(value.toRect());
        break;
    case Resolution:
        item->setResolution(value.toRect());
        break;
    // return NAME, NOT DISPLAY
//    case availableDisplay:
//        //QObject *object = qvariant_cast<QObject*>(value);
//        //Display* display = qobject_cast<Display*>(object);
//        QSharedPointer<Display> display = value.value<QSharedPointer<Display>>();
//        //Display display = value.value<Display>();//qvariant_cast<Display>(value);
//        item.setDisplay(display);//value
//        break;
    }

    //if (data(index, role) != value) {
    if (_displayCells->setItemAt(index.row(), item)) {
        // FIXME: Implement me!
        emit dataChanged(index, index, QVector<int>() << role);
        return true;
    }
    return false;
}

Qt::ItemFlags DisplayWorkareaModel::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return Qt::NoItemFlags;

    return Qt::ItemIsEditable; // FIXME: Implement me!
}

QHash<int, QByteArray> DisplayWorkareaModel::roleNames() const
{
    QHash<int, QByteArray> names;
    names[Position] = "position";
    names[Resolution] = "resolution";
    names[availableDisplay] = "display";
    names[idDisplay] = "idDisplay";
    names[physID] = "physID";
    names[blendingTop] = "blendingTop";
    names[blendingBottom] = "blendingBottom";
    names[blendingLeft] = "blendingLeft";
    names[blendingRight] = "blendingRight";

    names[blendingPositionTop] = "blendingPositionTop";
    names[blendingPositionBottom] = "blendingPositionBottom";
    names[blendingPositionLeft] = "blendingPositionLeft";
    names[blendingPositionRight] = "blendingPositionRight";

    names[blendingOpacityTop] = "blendingOpacityTop";
    names[blendingOpacityBottom] = "blendingOpacityBottom";
    names[blendingOpacityLeft] = "blendingOpacityLeft";
    names[blendingOpacityRight] = "blendingOpacityRight";

    names[videoPlaylist] = "videoPlaylist";

    names[primaryScreen] = "primaryScreen";
    return names;

}

DisplayCells *DisplayWorkareaModel::displayCells() const
{
    return _displayCells.get();
}

void DisplayWorkareaModel::setDisplayCells(DisplayCells *displayCells)
{
    beginResetModel();

    if(_displayCells)
        _displayCells->disconnect(this);

    _displayCells = QSharedPointer<DisplayCells>(displayCells);

    if (_displayCells) {
        connect(_displayCells.get(), &DisplayCells::preItemAppended, this, [=]() {
            const int index = _displayCells->displayCells().size();
            beginInsertRows(QModelIndex(), index, index);
        });
        connect(_displayCells.get(), &DisplayCells::postItemAppended, this, [=]() {
            endInsertRows();
        });

        connect(_displayCells.get(), &DisplayCells::preItemRemoved, this, [=](int index) {
            beginRemoveRows(QModelIndex(), index, index);
        });
        connect(_displayCells.get(), &DisplayCells::postItemRemoved, this, [=]() {
            endRemoveRows();
        });
    }

    endResetModel();
}

void DisplayWorkareaModel::addDisplayCell(const DisplayCell &displayCell)
{
    beginInsertRows(QModelIndex(), rowCount(), rowCount());
    QSharedPointer<DisplayCell> cell =QSharedPointer<DisplayCell>(new DisplayCell(displayCell));
    this->_displayCells->appendDisplayCell(*(cell.get()));
    endInsertRows();
}

void DisplayWorkareaModel::initDisplayIdForEditor()
{
    QVector<QSharedPointer<DisplayCell>> copyOfDisplayVector = _displayCells.get()->displayCells();
//    DisplayCell cell1 = DisplayCell(1920, 0, 1024, 768);
//    DisplayCell cell2 = DisplayCell(5469, 0, 1024, 768);
//    DisplayCell cell3 = DisplayCell(3489, 0, 1024, 768);
//    DisplayCell cell4 = DisplayCell(7890, 0, 1024, 768);
//    QVector<DisplayCell> copyOfDisplayVector = {};
//    copyOfDisplayVector.append(cell1);
//    copyOfDisplayVector.append(cell2);
//    copyOfDisplayVector.append(cell3);
//    copyOfDisplayVector.append(cell4);
//    _displayCells->appendDisplayCell(cell1);
//    _displayCells->appendDisplayCell(cell2);
//    _displayCells->appendDisplayCell(cell3);
//    _displayCells->appendDisplayCell(cell4);

    //qSort(_displayCells->displayCells().begin(), _displayCells->displayCells().end(), [](const DisplayCell& a, const DisplayCell& b) { return a.position().x() < b.position().x(); });
    qSort(copyOfDisplayVector.begin(), copyOfDisplayVector.end(), [](const QSharedPointer<DisplayCell> a, const QSharedPointer<DisplayCell> b) { return a->position().x() < b->position().x(); });
    for(int i=0; i < _displayCells.get()->displayCells().size(); i++)
    {
        copyOfDisplayVector[i]->setDisplayNumber(i+1);
        _displayCells->setItemAt(i, copyOfDisplayVector[i]);
    }


}

void DisplayWorkareaModel::changePositionToRight(int index, int offset)
{
    if(!_displayCells)
        return;

    QSharedPointer<DisplayCell> item = _displayCells->displayCells().at(index);
    qDebug() << item->position().x();
    qDebug() << item->position().right();
    //qDebug() << item.position().width()+offset;
    _displayCells->changePositionToRight(index, offset);
    QModelIndex indexModel = createIndex(index,0);
    emit dataChanged(indexModel, indexModel);
    qDebug() << item->position().x();
    qDebug() << item->position().right();
}

void DisplayWorkareaModel::changePositionToLeft(int index, int offset)
{
    if(!_displayCells)
        return;

    QSharedPointer<DisplayCell> item = _displayCells->displayCells().at(index);
    qDebug() << item->position().x();
    qDebug() << item->position().right();
    //qDebug() << item.position().width()+offset;
    _displayCells->changePositionToLeft(index, offset);
    QModelIndex indexModel = createIndex(index,0);
    emit dataChanged(indexModel, indexModel);
    qDebug() << item->position().x();
    qDebug() << item->position().right();
}

void DisplayWorkareaModel::changePositionToTop(int index, int offset)
{
    if(!_displayCells)
        return;

    QSharedPointer<DisplayCell> item = _displayCells->displayCells().at(index);
    qDebug() << index;
    qDebug() << item->position().y();
    qDebug() << item->position().bottom();
    //qDebug() << item.position().width()+offset;
    _displayCells->changePositionToTop(index, offset);
    QModelIndex indexModel = createIndex(index,0);
    emit dataChanged(indexModel, indexModel);
    qDebug() << item->position().y();
    qDebug() << item->position().bottom();
}

void DisplayWorkareaModel::changePositionToBottom(int index, int offset)
{
    if(!_displayCells)
        return;

    QSharedPointer<DisplayCell> item = _displayCells->displayCells().at(index);
    qDebug() << item->position().y();
    qDebug() << item->position().bottom();
    //qDebug() << item.position().width()+offset;
    _displayCells->changePositionToBottom(index, offset);
    QModelIndex indexModel = createIndex(index,0);
    emit dataChanged(indexModel, indexModel);
    qDebug() << item->position().y();
    qDebug() << item->position().bottom();
}

QVariantMap DisplayWorkareaModel::get(int row)
{
    QHash<int,QByteArray> names = roleNames();
        QHashIterator<int, QByteArray> i(names);
        QVariantMap res;
        while (i.hasNext()) {
            i.next();
            QModelIndex idx = index(row, 0);
            QVariant data = idx.data(i.key());
            res[i.value()] = data;
            //cout << i.key() << ": " << i.value() << endl;
        }
        return res;
}

QAbstractItemModel* DisplayWorkareaModel::resolutionList(int index)
{
    if(!_displayCells)
    {
        //DisplayResolutionList* res = new DisplayResolutionList();
        return new DisplayResolutionList();
    }

    if(index < 0)
        return nullptr;

    QSharedPointer<DisplayCell> item = _displayCells->displayCells().at(index);
    return item->displayResolutionList().get();
}

//int DisplayWorkareaModel::resolutionIndex(int index)
//{
//    if(!_displayCells)
//    {
//        //DisplayResolutionList* res = new DisplayResolutionList();
//        return -1;
//    }

//    if(index < 0)
//        return -1;

//    DisplayCell item = _displayCells->displayCells().at(index);
//    return item.getResolutionIndex();
//}

//QAbstractItemModel *DisplayWorkareaModel::playlistModel(int index)
//{
////    if(!_displayCells)
////    {
////        //DisplayResolutionList* res = new DisplayResolutionList();
////        return new VideoPlaylistModel();
////    }

////    if(index < 0)
////        return nullptr;

////    DisplayCell item = _displayCells->displayCells().at(index);
////    return item.videoPlaylistModel().get();
//    return nullptr;
//}

//void DisplayWorkareaModel::addVideo(int index, QString src)
//{
//    qDebug() << "DisplayWorkareaModel::addVideo";
//    DisplayCell item = _displayCells->displayCells().at(index);
//    item.addVideo(src);
//    //item.videoPlaylistModel()->addVideo(src);


////    QModelIndex indexModel = createIndex(index,0);
////    emit dataChanged(indexModel, indexModel);

//    //beginInsertRows(QModelIndex(), rowCount(), rowCount());
//    //DisplayCell item = _displayCells->displayCells().at(index);
//    //item.videoPlaylistModel()->playlist()->appendItem(src);
//    //endInsertRows();

////    DisplayCell item = _displayCells->displayCells().at(index);
////    item.videoPlaylistModel()->playlist()->appendItem(src);
//}

//void DisplayWorkareaModel::deleteVideo(int index, int videoIndex)
//{
//    DisplayCell item = _displayCells->displayCells().at(index);
//    item.removeVideo(videoIndex);

//    //QStringList deletedVideoList = src.split(QRegExp("[/]+"));
////    for(int i=0; i < item.getPlaylist()->list().size(); i++)
////    {
////        QStringList list = item.getPlaylist()->list()[i].getSource().split(QRegExp("[/]+"));
////        if(list[list.size()-2] == deletedVideoList[deletedVideoList.size()-2])

////    }
//    //item.removeVideo(videoIndex);
//    //qDebug() << "item found";
//    //item.videoPlaylistModel()->removeVideo(videoIndex);
//}

//void DisplayWorkareaModel::changeVideoInList(int index, QString src)
//{
//    qDebug() << "DisplayWorkareaModel::changeVideoInList";
//    DisplayCell item = _displayCells->displayCells().at(index);
//    QStringList splitSource = src.split(QRegExp("[/]+"));
//    if(splitSource[0] != "file:")
//        src = QString("file:///"+src);
//    item.changeVideo(src);

//    //QModelIndex indexModel = createIndex(index,0);
//    //emit dataChanged(indexModel, indexModel);
//}

QList<QUrl> DisplayWorkareaModel::getSourcesForPlaylist(int index)
{
    QSharedPointer<DisplayCell> item = _displayCells->displayCells().at(index);
    QList<QUrl> resultList = {};
    for(int i = 0; i < item->getPlaylist()->list().size(); i++)
    {
        qDebug() << "source: " << item->getPlaylist()->list()[i].getSource();
        QUrl url = QUrl(item->getPlaylist()->list()[i].getSource());
        //qDebug() << "Source from playlist " << item.getPlaylist()->list()[i].getSource();
        //qDebug() << "Url " << url.toString();
        resultList.append(url);
    }
//    qDebug() << "Size of res list " << resultList.size();
//    for(int i = 0; i < resultList.size(); i++)
//    {
//        //QUrl url = QUrl(item.getPlaylist()->list()[i].getSource());
//        qDebug() << "source " << i << " " << resultList[i].toString();
//        //qDebug() << "Url " << url.toString();
//        //resultList.append(url);
//    }
    return resultList;
}

//void DisplayWorkareaModel::itemPosUp(int index, int videoIndex)
//{
//    DisplayCell item = _displayCells->displayCells().at(index);
//    qDebug() << "size of currentPlaylist " << item.getPlaylist()->list().size();
//    if(videoIndex > 0 || videoIndex < item.getPlaylist()->list().size()-1)
//        item.getPlaylist()->upVideo(videoIndex);
////    auto currentVideo = item.getPlaylist()->list()[videoIndex];
////    if(videoIndex >= 0 || videoIndex < item.getPlaylist()->list().size()-1)
////    {
////        auto upVideo = item.getPlaylist()->list()[videoIndex-1];
////        item.getPlaylist()->list().replace(videoIndex-1, currentVideo);
////        item.getPlaylist()->list().replace(videoIndex, upVideo);
////    }
//}

//void DisplayWorkareaModel::itemPosDown(int index, int videoIndex)
//{
//    DisplayCell item = _displayCells->displayCells().at(index);
//    qDebug() << "size of currentPlaylist " << item.getPlaylist()->list().size();
//    if(videoIndex > 0 || videoIndex < item.getPlaylist()->list().size()-1)
//        item.getPlaylist()->downVideo(videoIndex);
////    auto currentVideo = item.getPlaylist()->list()[videoIndex];
////    if(videoIndex >= 0 || videoIndex < item.getPlaylist()->list().size()-1)
////    {
////        auto downVideo = item.getPlaylist()->list()[videoIndex+1];
////        item.getPlaylist()->list().replace(videoIndex+1, currentVideo);
////        item.getPlaylist()->list().replace(videoIndex, downVideo);
////    }
//}

//void DisplayWorkareaModel::writeToFile(int displayIndex, int videoIndex)
//{
////    QString filename="./data.txt";
////    QFile file(filename);

////    if ( file.open(QIODevice::ReadWrite) )
////    {
////        QTextStream stream( &file );
////        stream << displayIndex << ";" << videoIndex << endl;
////    }

//}

//int DisplayWorkareaModel::getDisplayIndexFromFile()
//{
////    QString res = "";
////    QString filename="./data.txt";
////    QFile inputFile(filename);
////    if (inputFile.open(QIODevice::ReadOnly))
////    {
////       QTextStream in(&inputFile);
////       while (!in.atEnd())
////       {
////          QString line = in.readLine();
////          res = line;
////          break;
////       }
////       inputFile.close();
////    }
////    int display = res.split(';')[0].toInt();
////    return display;
//    return 1;
//}

//int DisplayWorkareaModel::getVideoindexFromFile()
//{
////    QString res = "";
////    QString filename="./data.txt";
////    QFile inputFile(filename);
////    if (inputFile.open(QIODevice::ReadOnly))
////    {
////       QTextStream in(&inputFile);
////       while (!in.atEnd())
////       {
////          QString line = in.readLine();
////          res = line;
////          break;
////       }
////       inputFile.close();
////    }
////    int video = res.split(';')[1].toInt();
//    //    return video;
//    return 1;
//}

void DisplayWorkareaModel::changeTopBlending(int index, int offset)
{
    if(!_displayCells)
        return;

    _displayCells->changeTopBlend(index, offset);
    QModelIndex indexModel = createIndex(index,0);
    emit dataChanged(indexModel, indexModel);
}

void DisplayWorkareaModel::changeBottomBlending(int index, int offset)
{
    if(!_displayCells)
        return;

    _displayCells->changeBottomBlend(index, offset);
    QModelIndex indexModel = createIndex(index,0);
    emit dataChanged(indexModel, indexModel);
}

void DisplayWorkareaModel::changeLeftBlending(int index, int offset)
{
    if(!_displayCells)
        return;

    _displayCells->changeLeftBlend(index, offset);
    QModelIndex indexModel = createIndex(index,0);
    emit dataChanged(indexModel, indexModel);
}

void DisplayWorkareaModel::changeRightBlending(int index, int offset)
{
    if(!_displayCells)
        return;

    _displayCells->changeRightBlend(index, offset);
    QModelIndex indexModel = createIndex(index,0);
    emit dataChanged(indexModel, indexModel);
}

void DisplayWorkareaModel::changeTopPositionBlending(int index, double offset)
{
    if(!_displayCells)
        return;

    _displayCells->changePositionTopBlend(index, offset);
    QModelIndex indexModel = createIndex(index,0);
    emit dataChanged(indexModel, indexModel);
}

void DisplayWorkareaModel::changeBottomPositionBlending(int index, double offset)
{
    if(!_displayCells)
        return;

    _displayCells->changePositionBottomBlend(index, offset);
    QModelIndex indexModel = createIndex(index,0);
    emit dataChanged(indexModel, indexModel);
}

void DisplayWorkareaModel::changeLeftPositionBlending(int index, double offset)
{
    if(!_displayCells)
        return;

    _displayCells->changePositionLeftBlend(index, offset);
    QModelIndex indexModel = createIndex(index,0);
    emit dataChanged(indexModel, indexModel);
}

void DisplayWorkareaModel::changeRightPositionBlending(int index, double offset)
{
    if(!_displayCells)
        return;

    _displayCells->changePositionRightBlend(index, offset);
    QModelIndex indexModel = createIndex(index,0);
    emit dataChanged(indexModel, indexModel);
}

void DisplayWorkareaModel::changeTopOpacityBlending(int index, double offset)
{
    if(!_displayCells)
        return;

    _displayCells->changeOpacityTopBlend(index, offset);
    QModelIndex indexModel = createIndex(index,0);
    emit dataChanged(indexModel, indexModel);
}

void DisplayWorkareaModel::changeBottomOpacityBlending(int index, double offset)
{
    if(!_displayCells)
        return;

    _displayCells->changeOpacityBottomBlend(index, offset);
    QModelIndex indexModel = createIndex(index,0);
    emit dataChanged(indexModel, indexModel);
}

void DisplayWorkareaModel::changeLeftOpacityBlending(int index, double offset)
{
    if(!_displayCells)
        return;

    _displayCells->changeOpacityLeftBlend(index, offset);
    QModelIndex indexModel = createIndex(index,0);
    emit dataChanged(indexModel, indexModel);
}

void DisplayWorkareaModel::changeRightOpacityBlending(int index, double offset)
{
    if(!_displayCells)
        return;

    _displayCells->changeOpacityRightBlend(index, offset);
    QModelIndex indexModel = createIndex(index,0);
    emit dataChanged(indexModel, indexModel);
}
