#include "logicaldisplay.h"

LogicalDisplay::LogicalDisplay()
{

}

LogicalDisplay::LogicalDisplay(QScreen* screen, int displayIndex)
{
    this->numberId = displayIndex;
    this->_screenName = screen->name();
    this->_screenModel = screen->model();
    this->_screenSerialNumber = screen->serialNumber();
    this->_screenManufactured = screen->manufacturer();
    this->_screenGeometry = screen->geometry();
    this->_screenVirtualGeometry = screen->virtualGeometry();
    this->_screenAvailableGeometry = screen->availableGeometry();
    this->_screenAvailableVirtualGeometry = screen->availableVirtualGeometry();

}

LogicalDisplay::LogicalDisplay(const LogicalDisplay &display)
{
    this->_screenName = display._screenName;
    this->_screenModel = display._screenModel;
    this->_screenGeometry = display._screenGeometry;
    this->_screenManufactured = display._screenManufactured;
    this->_screenSerialNumber = display._screenSerialNumber;
    this->_screenVirtualGeometry = display._screenVirtualGeometry;
    this->_screenAvailableGeometry = display._screenAvailableGeometry;
    this->_screenAvailableVirtualGeometry = display._screenAvailableVirtualGeometry;
    this->isPrimary = display.isPrimary;
    this->numberId = display.numberId;
}

void LogicalDisplay::setPrimaryScreen()
{
    this->isPrimary = true;

}

QString LogicalDisplay::getScreenName()
{
    return this->_screenName;
}
