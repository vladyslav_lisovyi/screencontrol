#ifndef DISPLAY_H
#define DISPLAY_H

#include <QString>
#include <QRect>
#include <QSharedPointer>
#include "src/model/logicaldisplay.h"
#include "src/model/physicaldisplay.h"
#include "src/model/displayslist.h"
#include <Windows.h>
#include <QSettings>
#include <QThread>
#include <QLibrary>

class Display
{
    Q_GADGET
    //Q_PROPERTY(PhysicalDisplay* physicalDisplay READ getPhysicalDisplay WRITE setPhysicalDisplay)
    //Q_PROPERTY(LogicalDisplay* logicalDisplay READ getLogicalDisplay WRITE setLogicalDisplay)
    //Q_PROPERTY(QString idDisplay READ strDisplay)
public:
    Display();
    Display(QSharedPointer<PhysicalDisplay> physicalDisplay, QSharedPointer<LogicalDisplay> logicalDisplay);
    Display(const Display& display);
    ~Display() { }//delete this->_physicalDisplay; delete this->_logicalDisplay;}
    Display& operator=(const Display& device);

    QSharedPointer<PhysicalDisplay> getPhysicalDisplay() const;
    QSharedPointer<LogicalDisplay> getLogicalDisplay() const;
    QList<QRect> getAvailableResolutions() { return _availableResolutions; }
    int IdDisplay() const;
    //QString strDisplay() const { return QString(this->_idDisplay); }
    void setLogicalDisplay(LogicalDisplay& logicalDisplay);
    void setPhysicalDisplay(PhysicalDisplay& physicalDisplay);

    long setPrimaryScreen();
    //void removePrimaryScreen();
    long changeResolution(unsigned long width, unsigned long height);
    long moveScreenUp();
    long moveScreenDown();
    long moveScreenRight();
    long moveScreenLeft();
    long showHideTaskBar(bool hide);

    void moveContentUp(int pixelValue);
    void moveContentDown(int pixelValue);
    void moveContentLeft(int pixelValue);
    void moveContentRight(int pixelValue);

private:
    QSharedPointer<PhysicalDisplay> _physicalDisplay;
    QSharedPointer<LogicalDisplay> _logicalDisplay;
//    PhysicalDisplay* _physicalDisplay;
//    LogicalDisplay* _logicalDisplay;
    int _idDisplay;
    QList<QRect> _availableResolutions;
    int _upPosition=0;
    int _downPosition=0;
    int _leftPosition=0;
    int _rightPosition=0;
};
Q_DECLARE_METATYPE(Display)

#endif // DISPLAY_H
