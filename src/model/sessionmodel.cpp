#include "sessionmodel.h"

SessionModel::SessionModel(QObject *parent)
    : QAbstractListModel(parent)
{
    _sessionList = new SessionList();
}

int SessionModel::rowCount(const QModelIndex &parent) const
{
    // For list models only the root node (an invalid parent) should return the list's size. For all
    // other (valid) parents, rowCount() should return 0 so that it does not become a tree model.
    if (parent.isValid())
        return 0;

    // FIXME: Implement me!
    return _sessionList->sessions().size();
}

QVariant SessionModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    // FIXME: Implement me!
    const QSharedPointer<Session> item = _sessionList->sessions().at(index.row());
    switch (role) {
    case filename:
        return QVariant(item->filename());
    case displayworkarea:
        return QVariant::fromValue(item->displayWorkarea());
    case playlistCollectionList:
        return QVariant::fromValue(item->playlistCollectionList());
    }
    return QVariant();
}

bool SessionModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if(!_sessionList)
        return false;

    QSharedPointer<Session> item = _sessionList->sessions().at(index.row());

    switch (role) {
    case filename:
        item->setFilename(value.toString());
    }

    if (_sessionList->setItemAt(index.row(), item)) {
        // FIXME: Implement me!
        emit dataChanged(index, index, QVector<int>() << role);
        return true;
    }
    return false;
}

Qt::ItemFlags SessionModel::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return Qt::NoItemFlags;

    return Qt::ItemIsEditable; // FIXME: Implement me!
}

QHash<int, QByteArray> SessionModel::roleNames() const
{
    QHash<int, QByteArray> names;
    names[filename] = "filename";
    names[displayworkarea] = "displayworkarea";
    names[playlistCollectionList] = "playlistCollectionList";
    return names;
}

SessionList *SessionModel::sessionList() const
{
    //return this->_sessionList.get();
    return _sessionList;
}

QVariantMap SessionModel::get(int row)
{
    QHash<int,QByteArray> names = roleNames();
    QHashIterator<int, QByteArray> i(names);
    QVariantMap res;
    while (i.hasNext()) {
        i.next();
        QModelIndex idx = index(row, 0);
        QVariant data = idx.data(i.key());
        res[i.value()] = data;
        //cout << i.key() << ": " << i.value() << endl;
    }
    return res;
}

void SessionModel::setSessionList(SessionList *sessionList)
{
    beginResetModel();

    //qDebug() << "set playlist";

    if (_sessionList)
        _sessionList->disconnect(this);

    _sessionList = sessionList;

    if (_sessionList) {
        connect(_sessionList, &SessionList::preItemAppended, this, [=](QString filename) {
            const int index = _sessionList->sessions().size();
            beginInsertRows(QModelIndex(), index, index);
            //m_playlist->appendItem(file);
            //endInsertRows();
        });
        connect(_sessionList, &SessionList::postItemAppended, this, [=]() {
            endInsertRows();
        });
    }

    endResetModel();
}
