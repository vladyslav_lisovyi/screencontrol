#ifndef PLAYLISTCOLLECTIONLIST_H
#define PLAYLISTCOLLECTIONLIST_H

#include <QObject>
#include <QString>
#include "src/model/playlistcollection.h"

class PlaylistCollectionList: public QObject
{
    Q_OBJECT

public:
    explicit PlaylistCollectionList(QObject *parent = nullptr);
    //PlaylistCollectionList();
    PlaylistCollectionList(const PlaylistCollectionList& playlistCollection);
    PlaylistCollectionList& operator=(const PlaylistCollectionList& playlistCollection);
    ~PlaylistCollectionList();

    bool setItemAt(int index, const PlaylistCollection item);
    QList<PlaylistCollection> playlistColletion() const;

    Q_INVOKABLE
    QString getPlaylistName(int index);

    Q_INVOKABLE
    void initPlaylistCollectionList(PlaylistCollectionList* collection);

    Q_INVOKABLE
    int sizeOfList();

    Q_INVOKABLE
    int sizeOfVideoplaylist(int playlistCollectionIndex);

signals:
    void preItemAppended(QString source);
    //void itemAppended(QString source);
    void postItemAppended();
    void preItemRemoved(int index);
    void postItemRemoved();

public slots:
    void appendItem(QString source);
    void removeItem(int index);

private:
    QList<PlaylistCollection> _playlistColletion;
};

#endif // PLAYLISTCOLLECTIONLIST_H
