#ifndef LOGICALDISPLAY_H
#define LOGICALDISPLAY_H

#include <QString>
#include <QRect>
#include <QScreen>
#include <QGuiApplication>

class LogicalDisplay
{
public:
    LogicalDisplay();
    ~LogicalDisplay() {}
    LogicalDisplay(QScreen* screen, int displayIndex);
    LogicalDisplay(const LogicalDisplay& display);
    void setPrimaryScreen();
    int getID() {return this->numberId;}
    void setID(int id) {this->numberId = id;}
    QString getScreenName();
private:
    int numberId;
    QString _screenName;
    QString _screenModel;
    QString _screenSerialNumber;
    QString _screenManufactured;
    QRect _screenAvailableGeometry;
    QRect _screenAvailableVirtualGeometry;
    QRect _screenGeometry;
    QRect _screenVirtualGeometry;
    bool isPrimary = false;
};

#endif // LOGICALDISPLAY_H
