#ifndef DISPLAYGRID_H
#define DISPLAYGRID_H

#include <QObject>
#include <QVector>
#include "src/model/display.h"
#include "src/model/displayslist.h"

class DisplayGrid : public QObject
{
    Q_OBJECT
public:
    explicit DisplayGrid(QObject *parent = nullptr);

    QVector<Display> displays() const { return  this->_displays;}
    bool setItemAt(int index, const Display &item);

signals:
    void preItemAppended();
    void postItemAppended();
    void preItemRemoved(int index);
    void postItemRemoved();

    void preDisplaylistUpdated();
    void postDisplaylistUpdated();

public slots:
    void appendItem();
    void removeItem(int index); // don't know, can init slot with arg

    void updateDisplaylist();

private:
    QVector<Display> _displays;

    QSharedPointer<DisplaysList> _displaylist;
    //DisplaysList _displaylist;
    void initDisplays();
};

#endif // DISPLAYGRID_H
