#ifndef SESSION_H
#define SESSION_H

#include "src/model/displaysworkarea.h"
#include "src/model/playlistcollectionlist.h"
#include <QDir>
#include <QTextCodec>
#include <QException>

class ConfigurationException : public QException
{
public:
    void raise() const override { throw *this; }
    ConfigurationException *clone() const override { return new ConfigurationException(*this); }
};

class Session
{
public:
    Session();
    Session(DisplaysWorkarea *displayWorkarea);
    Session(PlaylistCollectionList *playlistCollectionList);
    Session(QString filename);
    Session(const Session& session);
    ~Session();

    QString filename() const;
    void setFilename(const QString &filename);

    bool SaveSettings();
    void LoadSettings();
    bool checkAvailableSaves();

    QSharedPointer<DisplaysWorkarea> displayWorkarea() const;
    void setDisplayWorkarea(const QSharedPointer<DisplaysWorkarea> displayWorkarea);

    QSharedPointer<PlaylistCollectionList> playlistCollectionList() const;
    void setPlaylistCollectionList(const QSharedPointer<PlaylistCollectionList> playlistCollectionList);

private:
    QSharedPointer<DisplaysWorkarea> _displayWorkarea;
    QString _filename;
    QSharedPointer<PlaylistCollectionList> _playlistCollectionList;

};

#endif // SESSION_H
