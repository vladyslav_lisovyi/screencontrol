#include "displaycell.h"

DisplayCell::DisplayCell(QObject *parent) : QObject (parent)
{
    initializeDisplays();
}

bool DisplayCell::setItemAt(int index, const Display &item)
{
    if(index < 0 || index >= this->_displays.size())
        return false;
    const Display &oldItem = this->_displays.at(index);
    if(item.getPhysicalDisplay().getDeviceName() == oldItem.getPhysicalDisplay().getDeviceName())
        return false;
    this->_displays[index] = item;
    return true;
}

QList<Display> DisplayCell::displays() const
{
    return this->_displays;
}

void DisplayCell::initializeDisplays() // TEST METHOD
{
    DisplaysList displaylist = DisplaysList(); // all availlable displays

    for(int i=0; i < displaylist.logicalDisplays.count(); i++)
        for(int j=0; j < displaylist.physicalDisplays.count(); j++)
        {
            auto str1 = displaylist.logicalDisplays[i].getScreenName().split(QRegExp("\\W+"))[1];
            auto str2 = displaylist.physicalDisplays[j].getDeviceName().split(QRegExp("\\W+"))[1];
            if(displaylist.logicalDisplays[i].getScreenName().split(QRegExp("\\W+"))[1] == displaylist.physicalDisplays[j].getDeviceName().split(QRegExp("\\W+"))[1])
                this->_displays.append(Display((displaylist.physicalDisplays[j]), (displaylist.logicalDisplays[i])));
            }
}

void DisplayCell::appendItem()
{
    emit preItemAppended();

    Display display;
    this->_displays.append(display);

    emit postItemAppended();
}

void DisplayCell::removeItem(int index) // ???????????????????????????????????????????????????
{
    emit preItemRemoved(index);

    this->_displays.removeAt(index);

    emit postItemRemoved();
}
