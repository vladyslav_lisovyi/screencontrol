#include "videoplaylist.h"

#include <QDir>
#include <QDebug>

//VideoPlaylist::VideoPlaylist(QObject *parent) : QObject(parent)
//{

//}

VideoPlaylist::VideoPlaylist()
{
    mList = {};
    mPlaylistName = "";
}

VideoPlaylist::VideoPlaylist(const VideoPlaylist &playlist)
{
    mList = playlist.mList;
    mPlaylistName = playlist.mPlaylistName;
}

VideoPlaylist &VideoPlaylist::operator=(const VideoPlaylist &playlist)
{
    if(this == &playlist)
        return *this;
    mList = playlist.mList;
    mPlaylistName = playlist.mPlaylistName;
    return *this;
}

VideoPlaylist::~VideoPlaylist()
{
    qDebug() << "~VideoPlaylist";
}

QList<VideoContent> VideoPlaylist::list() const
{
    return mList;
}

bool VideoPlaylist::setItemAt(int index, const VideoContent &item)
{
    if (index < 0 || index >= mList.size())
        return false;

    const VideoContent &oldItem = mList.at(index);
//    if (item.filename() == oldItem.filename() && item.getSource() == oldItem.getSource())
//        return false;

    mList[index] = item;
    return true;
}

void VideoPlaylist::appendItem(QString file)
{
    emit preItemAppended(file);

    qDebug() << "append video";
    VideoContent item;
    //qDebug() << "item create";
    item.setSource(file);
    //qDebug() << "file: " << file;

    mList.append(item);

    //emit itemAppended(file);
    emit postItemAppended();
}

void VideoPlaylist::setPlaylistName(const QString &playlistName)
{
    mPlaylistName = playlistName;

    emit playlistNameChanged(playlistName);
}

void VideoPlaylist::removeItem(int index)
{
    emit preItemRemoved(index);

    if(mList.size() < 0)
        return;
    if(index < 0 || index > mList.size()-1)
        return;
    mList.removeAt(index);

    emit postItemRemoved();
}

void VideoPlaylist::changeVideo(int index, QString source)
{
    emit preItemChanged(index, source);

    if(mList.size() < 0)
        return;
    if(index < 0 || index > mList.size()-1)
        return;
    VideoContent video = VideoContent(source);
    mList.replace(index, video);

    emit postItemChanged();
}

void VideoPlaylist::upVideo(int index)
{
    qDebug() << "mList size " << mList.size();
    auto currentVideo = mList[index];
    qDebug() << "currentVideo " << currentVideo.getSource();
    if(index > 0 && index <= mList.size()-1)
    {
        emit preUpVideoChanged(index);
        qDebug() << " emit preUpVideoChanged(index)";
        auto upVideo = mList[index-1];
        qDebug() << "upVideo " << upVideo.getSource();
        mList.replace(index-1, currentVideo);
        mList.replace(index, upVideo);
        emit postUpVideoChanged();
        qDebug() << " emit postUpVideoChanged()";
    }

}

void VideoPlaylist::downVideo(int index)
{
    auto currentVideo = mList[index];
    qDebug() << "mList size " << mList.size();
    if(index >= 0 && index < mList.size()-1)
    {
        emit preDownVideoChanged(index);
        auto downVideo = mList[index+1];
        qDebug() << "down video " << downVideo.getSource();
        mList.replace(index+1, currentVideo);
        mList.replace(index, downVideo);
        emit postDownVideoChanged();
        qDebug() << "postDownVideoChanged ";
    }
}

QString VideoPlaylist::playlistName() const
{
    return mPlaylistName;
}

void VideoPlaylist::initVideoPlaylistByFolder(QString source) // You choose: file:///C:/jj4
{
    QStringList sourceList = source.split(QRegExp("[/]+"));
    if(sourceList[0] == "file:")
        mPlaylistName = sourceList[sourceList.size()-1];
    else {
        mPlaylistName = sourceList[sourceList.size()-1];
    }
    QString path;
    if(sourceList[0] == "file:")
    {
        path = source.split("file:///")[1];
    }
    else {
        path = source;
    }
    //QString path = source; //.split("file:///")[1];
    QDir directory(path);
    QStringList videos = directory.entryList(QStringList() << "*.mov" <<
                                             "*.mp4" << "*.m4a" << "*.m4v" << "*.f4v" <<
                                             "*.f4a" << "*.m4b" << "*.m4r" << "*.f4b" <<
                                             "*.3gp" << "*.3gp2" << "*.3g2" << "*.3gpp" <<
                                             "*.3gpp2" << "*.ogg" << "*.oga" << "*.ogv" <<
                                             "*.ogx" << "*.wmv" << "*.wma" << "*.webm" <<
                                             "*.flv" << "*.avi" << "*.hdv" << "*.mkv"
                                             );//,QDir::Files);

    foreach(QString filename, videos)
    {
        VideoContent video;
        if(filename.split(QRegExp("[/]+"))[0] == "file:")
            video.setSource(filename);
        if(sourceList[0] == "file:")
        {
            video.setSource(QString(source + '/' + filename));
        }
        else {
            video.setSource(QString("file:///" + source + '/' + filename));
        }
        mList.append(video);
    }

    //    QString res = "";
    //    QString filename="./data.txt";
    //    QFile inputFile(filename);
    //    if (inputFile.open(QIODevice::ReadOnly))
    //    {
    //       QTextStream in(&inputFile);
    //       while (!in.atEnd())
    //       {
    //          QString line = in.readLine();
    //          res = line;
    //          break;
    //       }
    //       inputFile.close();
    //    }
    //    int display = res.split(';')[0].toInt();
    //    return display;
}

QString VideoPlaylist::getSource(int index)
{
    if(index < 0 || index > mList.size())
        return "";
    return mList[index].getSource();
}

QString VideoPlaylist::sourceForPlaylistCollection()
{
    QString resStr = "";
    if(mList.size() != 0)
    {
        QStringList list = mList[0].getSource().split(QRegExp("[/]+"));
        for(int i=0; i < list.size()-1; i++)
        {
            if(i == 0)
                resStr = resStr + list[i];
            else {
                resStr = resStr + "/" + list[i];
            }
        }
    }
    return resStr;
}
