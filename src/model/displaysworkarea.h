#ifndef DISPLAYSWORKAREA_H
#define DISPLAYSWORKAREA_H

#include <QObject>

#include <QRect>
#include <QVector>
#include <QScreen>
#include <QGuiApplication>

#include <QFile>
#include <QDir>
#include <QTextCodec>

//#include "src/model/display.h"
//#include "src/model/displayworkareamodel.h"
#include "src/model/displaygridmodel.h"
#include "src/model/displayworkareamodel.h"
#include "src/model/playlistcollectionlist.h"
//#include "src/model/displayresolutionlist.h"
//#include "src/model/videoplaylistmodel.h"

class DisplaysWorkarea : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QAbstractItemModel* model READ model) // WRITE setModel NOTIFY modelChanged)
    Q_PROPERTY(QRect fullResolution READ fullResolution NOTIFY fullResolutionChanged)
    Q_PROPERTY(QRect contentWindowResolution READ contentWindowResolution NOTIFY contentResolutionWindowChanged)

    Q_PROPERTY(QAbstractItemModel* gridModel READ gridModel)
    //Q_PROPERTY(QAbstractItemModel* resolutionModel READ resolutionModel)
    //Q_PROPERTY(QAbstractItemModel* videoModel READ videoModel)

    Q_PROPERTY(QRect startPos READ getStartPos NOTIFY startPositionChanged)

    Q_PROPERTY(int xOffset READ getXOffset NOTIFY xOffsetChanged)
    Q_PROPERTY(int yOffset READ getYOffset NOTIFY yOffsetChanged)
public:
    //explicit DisplaysWorkarea(QObject *parent = nullptr);
    DisplaysWorkarea();
    DisplaysWorkarea(bool isNew);
    DisplaysWorkarea(const DisplaysWorkarea &displayWorkarea);
    DisplaysWorkarea& operator=(const DisplaysWorkarea& workarea);
    ~DisplaysWorkarea();

    DisplayWorkareaModel *model() const {return this->_model.get();}
    void initModel();
    DisplayGridModel *gridModel() const {return this->_gridModel.get();}
    void initGridModel();
    //VideoPlaylistModel *videoModel() const {return  this->_videoModel.get();}
    //void setVideoModel(VideoPlaylistModel *setVideoModel) { this->_videoModel = QSharedPointer<VideoPlaylistModel>(new VideoPlaylistModel(setVideoModel));}

    //DisplayResolutionList *resolutionModel() const;

    QRect fullResolution() const;
    void setFullResolution(QRect &resolution);

    QRect contentWindowResolution() const;
    void setContentWindowResolution(QRect &resolution);

    QRect getStartPos() const;
    void setStartPos(const QRect &startPos);

    Q_INVOKABLE
    bool saveToFile();

    Q_INVOKABLE
    void initDisplaysWorkarea(DisplaysWorkarea* workarea);

    Q_INVOKABLE
    QSharedPointer<DisplayCells> loadFromFile();

    Q_INVOKABLE
    int deleteHighlightFromPlaylist(int displayIndex, int videoIndex, PlaylistCollectionList* collection);

    int getRows() const;
    void setRows(int rows);

    int getColumns() const;
    void setColumns(int columns);

    int getXOffset() const {return _xOffset;}
    int getYOffset() const {return _yOffset;}

signals:
    void preDimensionChanged(int row, int column);
    void postDimensionChanged();

    void preFullResolutionChanged(QRect &newFullResolution);
    void postFullResolutionChanged();

    //void preDisplayCellChanged(DisplayCell &oldDisplay, DisplayCell &newDisplay);
    //void postDisplayCellChanged();
    void fullResolutionChanged();
    void contentResolutionWindowChanged();
    void startPositionChanged();

    void xOffsetChanged();
    void yOffsetChanged();
    //void modelChanged(QAbstractItemModel* model);

public slots:
    void changeDimension(int row, int column);
    void changeFullResolution(QRect &newFullResolution);
    //void changeDisplayCell(DisplayCell &oldDisplay, DisplayCell &newDisplay);
    //void changeContenWindowRresolution(QRect &newContentWindowResolution);

    void setXStartPosition(int x);
    void setYStartPosition(int y);

    void setWidthContentWindowRes(int width);
    void setHeightContentWindowRes(int height);

private:
    QRect _contentWindowResolution;
    QRect _fullResolution;
    int _rows;
    int _columns;
    //QVector<DisplayCell> _displayCell;
    QRect _startPos;
    int _xOffset = 0;
    int _yOffset = 0;

    QSharedPointer<DisplayWorkareaModel> _model;
    QSharedPointer<DisplayGridModel> _gridModel;

    //QSharedPointer<VideoPlaylistModel> _videoModel;
    //QSharedPointer<DisplayResolutionList> _resolutionModel;
    //QSharedPointer<VideoPlaylist> _playlistModel;

    void getSavedWorkarea();
};

#endif // DISPLAYSWORKAREA_H
