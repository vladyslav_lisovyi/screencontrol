#include "videomanager.h"

VideoManager::VideoManager(QObject *parent) : QObject(parent)
{
    _pause = false;
}

void VideoManager::receiveFromQml(int i)
{
    //_pause = false;
    emit sendToQml(i);
}

bool VideoManager::pause() const
{
    return _pause;
}

void VideoManager::setPause(bool pause)
{
    _pause = pause;
    emit pausedChanged();
}

int VideoManager::videoIndex() const
{
    return _videoIndex;
}

void VideoManager::setVideoIndex(int videoIndex)
{
    _videoIndex = videoIndex;
    emit videoIndexChanged();
}

int VideoManager::displayIndex() const
{
    return _displayIndex;
}

void VideoManager::setDisplayIndex(int displayIndex)
{
    _displayIndex = displayIndex;
    emit displayIndexChanged();
}
