#ifndef DISPLAYCELL_H
#define DISPLAYCELL_H

#include "src/model/display.h"
#include "src/model/displayslist.h"
#include <QObject>

class DisplayCell : public QObject
{
    Q_OBJECT
public:
    explicit DisplayCell(QObject *parent = nullptr);
    //DisplayCell();
    ~DisplayCell() {}
    bool setItemAt(int index, const Display &item);
    QList<Display> displays() const;

private:
    QList<Display> _displays;
    void initializeDisplays();

// singnal don't need definitions
signals:
    void preItemAppended();
    void postItemAppended();

    void preItemRemoved(int index);
    void postItemRemoved();

public slots:
    void appendItem();
    void removeItem(int index);
};

#endif // DISPLAYCEL_H
