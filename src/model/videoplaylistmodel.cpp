#include "videoplaylistmodel.h"

#include "src/model/videoplaylist.h"
#include <QDebug>

VideoPlaylistModel::VideoPlaylistModel(QObject *parent)
    : QAbstractListModel(parent)//, // m_playlist(nullptr)
{
    m_playlist = QSharedPointer<VideoPlaylist>(new VideoPlaylist());
}

VideoPlaylistModel::~VideoPlaylistModel()
{
    qDebug() << "~VideoPlaylistModel";
    m_playlist.clear();
}

int VideoPlaylistModel::rowCount(const QModelIndex &parent) const
{
    // For list models only the root node (an invalid parent) should return the list's size. For all
    // other (valid) parents, rowCount() should return 0 so that it does not become a tree model.
    if (parent.isValid())
        return 0;

    // FIXME: Implement me!
    return m_playlist->list().size();
}

QVariant VideoPlaylistModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    const VideoContent item = m_playlist->list().at(index.row());
    switch (role) {
    case filename:
        return QVariant(item.filename());
    case source:
        return QVariant(item.getSource());
    }
    // FIXME: Implement me!
    return QVariant();
}

bool VideoPlaylistModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if(!m_playlist)
        return false;

    VideoContent item = m_playlist->list().at(index.row());
    switch (role) {
    case source:
        item.setSource(value.toString());
        break;
    }

    if (m_playlist->setItemAt(index.row(), item)) {
        // FIXME: Implement me!
        emit dataChanged(index, index, QVector<int>() << role);
        return true;
    }
    return false;
}

Qt::ItemFlags VideoPlaylistModel::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return Qt::NoItemFlags;

    return Qt::ItemIsEditable; // FIXME: Implement me!
}

QHash<int, QByteArray> VideoPlaylistModel::roleNames() const
{
    QHash<int, QByteArray> names;
    names[filename] = "filename";
    names[source] = "source";
    return names;
}

void VideoPlaylistModel::setPlaylist(QSharedPointer<VideoPlaylist> playlist)
{
    beginResetModel();

    qDebug() << "set playlist in VideoPlaylistModel";

    if (m_playlist)
        m_playlist->disconnect(this);

    //m_playlist = QSharedPointer<VideoPlaylist>(playlist);//(new VideoPlaylist(*playlist));//(playlist); // ???????????????
    m_playlist = playlist;

    if (m_playlist) {
        connect(m_playlist.get(), &VideoPlaylist::preItemAppended, this, [=](QString file) {
            const int index = m_playlist->list().size();
            beginInsertRows(QModelIndex(), index, index);
            //m_playlist->appendItem(file);
            //endInsertRows();
        });
        connect(m_playlist.get(), &VideoPlaylist::postItemAppended, this, [=]() {
            endInsertRows();
        });
        connect(m_playlist.get(), &VideoPlaylist::preItemRemoved, this, [=](int index) {
            //const int index = m_playlist->list().size();
            beginRemoveRows(QModelIndex(), index, index);
            //m_playlist->appendItem(file);
            //endInsertRows();
        });
        connect(m_playlist.get(), &VideoPlaylist::postItemRemoved, this, [=]() {
            endRemoveRows();
        });

        connect(m_playlist.get(), &VideoPlaylist::preUpVideoChanged, this, [=](int index) {
            //const int index = m_playlist->list().size();
            beginMoveRows(QModelIndex(), index, index, QModelIndex(), index-1);
        });
        connect(m_playlist.get(), &VideoPlaylist::postUpVideoChanged, this, [=]() {
            endMoveRows();
        });

        connect(m_playlist.get(), &VideoPlaylist::preDownVideoChanged, this, [=](int index) {
            //const int index = m_playlist->list().size();
            beginMoveRows(QModelIndex(), index+1, index+1, QModelIndex(), index);
        });
        connect(m_playlist.get(), &VideoPlaylist::postDownVideoChanged, this, [=]() {
            endMoveRows();
        });
    }

    endResetModel();
    emit playlistChanged(playlist.get());
}

void VideoPlaylistModel::updatePlaylist()
{
    qDebug() << "VideoPlaylistModel::updatePlaylist";
    for(int i=0; i < m_playlist->list().size(); i++)
    {
        QModelIndex indexModel = createIndex(i,0);
        qDebug() << "emit dataChanged";
        emit dataChanged(indexModel, indexModel);
    }
}
