#include "displayresolutionlist.h"

DisplayResolutionList::DisplayResolutionList(QObject *parent)
    : QAbstractListModel(parent)
{
    _displaysResolution = QSharedPointer<DisplayResolution>(new DisplayResolution());
}

DisplayResolutionList::DisplayResolutionList(Display &display, QObject *parent)
{
    _displaysResolution = QSharedPointer<DisplayResolution>(new DisplayResolution(display));
}

int DisplayResolutionList::rowCount(const QModelIndex &parent) const
{
    // For list models only the root node (an invalid parent) should return the list's size. For all
    // other (valid) parents, rowCount() should return 0 so that it does not become a tree model.
    if (parent.isValid())
        return 0;

    // FIXME: Implement me!
    return _displaysResolution->res().size();
}

QVariant DisplayResolutionList::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    // FIXME: Implement me!
    const QRect item = _displaysResolution->res().at(index.row());
    switch (role) {
    case widthRes:
        return QVariant(item.width());
    case heightRes:
        return QVariant(item.height());
    case resolutionStr:
        {
            QString returnValue = QString::number(item.width()) + "x" + QString::number(item.height()); // +"x"+item.height());
            return QVariant(returnValue);
        }
    }

    return QVariant();
}

bool DisplayResolutionList::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if(!_displaysResolution)
        return false;

    QRect item = _displaysResolution->res().at(index.row());
    switch (role) {
    case widthRes:
        item.setWidth(value.toInt());
        break;
    case heightRes:
        item.setHeight(value.toInt());
        break;
    }

    if (_displaysResolution->changeResItem(index.row(), item))
    {
        // FIXME: Implement me!
        emit dataChanged(index, index, QVector<int>() << role);
        return true;
    }
    return false;
}

Qt::ItemFlags DisplayResolutionList::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return Qt::NoItemFlags;

    return Qt::ItemIsEditable; // FIXME: Implement me!
}

QHash<int, QByteArray> DisplayResolutionList::roleNames() const
{
    QHash<int, QByteArray> names;
    names[widthRes] = "widthRes";
    names[heightRes] = "heightRes";
    names[resolutionStr] = "resolutionStr";
    return names;
}
