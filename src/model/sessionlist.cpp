#include "sessionlist.h"

SessionList::SessionList(QObject *parent) : QObject(parent)
{
    this->_session = {};
    //this->m_selectedSession = QSharedPointer<Session>(new Session());
}

bool SessionList::setItemAt(int index, const QSharedPointer<Session> item)
{
    if(index < 0 || index >= this->_session.size())
        return false;

    const QSharedPointer<Session> oldItem = this->_session.at(index);
    if(oldItem->filename() == item->filename())
        return false;

    this->_session[index] = item;
    return true;
}

bool SessionList::checkAvailableSaves()
{
    if(!QDir("./saves").exists())
    {
        QDir().mkdir("./saves");
        return false;
    }
    QDir saveFilesDir = QDir("./saves");
    QStringList nameFilter("*.ini");
    QStringList iniFilesAndDirectories = saveFilesDir.entryList(nameFilter);
    if(iniFilesAndDirectories.size() == 0)
        return false;
    return true;
}

bool SessionList::loadingSessionsFromFolder()
{
    if(!checkAvailableSaves())
        return false;
    else
    {
        QDir saveFilesDir = QDir("./saves");
        QStringList nameFilter("*.ini");
        QStringList iniFilesAndDirectories = saveFilesDir.entryList(nameFilter);
        for(int i=0; i < iniFilesAndDirectories.size(); i++)
        {
            QSharedPointer<Session> session = QSharedPointer<Session>(new Session());
            session->setFilename(iniFilesAndDirectories[i]);
            _session.append(session);
        }
    }
}

void SessionList::loadSave(int index)
{
    if(index >= 0 || index <= _session.size()-1)
        _session[index]->LoadSettings();
}

int SessionList::createSave(QString filename)
{
    QStringList nameList = filename.split(QRegExp("[.]"));
    if(nameList.size() <= 1)
    {
        filename = filename + ".ini";
    }
    else if(nameList[nameList.size()-1] != "ini")
    {
        filename = filename + ".ini";
    }
    emit preItemAppended(filename);

    QSharedPointer<Session> item = QSharedPointer<Session>(new Session(filename));
    this->_session.append(item);

    emit postItemAppended();
    return _session.size()-1;
}

//void SessionList::selectSession(int index)
//{
////    if(index >= 0 || index <= _session.size()-1)
////        m_selectedSession = QSharedPointer<Session>(&(_session[index]));
//}

//void SessionList::saveCurrentSession(DisplaysWorkarea* workarea, PlaylistCollectionList* collection)
//{
//    m_selectedSession->setDisplayWorkarea(QSharedPointer<DisplaysWorkarea>(workarea));
//    m_selectedSession->setPlaylistCollectionList(QSharedPointer<PlaylistCollectionList>(collection));
//    m_selectedSession->SaveSettings();
//}

//void SessionList::loadCurrentSession()
//{
//    m_selectedSession->LoadSettings();
//}

//void SessionList::deleteVideoForDisplay(int displayIndex, int videoIndexFromCurentPlaylist)
//{
//    QString source = m_selectedSession->displayWorkarea()->model()->displayCells()->displayCells()[displayIndex]->getPlaylist()->list()[videoIndexFromCurentPlaylist].getSource();
//    QStringList sourceList = source.split(QRegExp("[/]+"));
//    QString playlistName = sourceList[sourceList.size()-1];
//    int wantedIndex = -1;
//    for(int i=0; i < m_selectedSession->playlistCollectionList()->playlistColletion().size(); i++)
//    {
//        if(playlistName == m_selectedSession->playlistCollectionList()->playlistColletion()[i].playlistName())
//            wantedIndex = i;
//    }
//    //return wantedIndex;
//}

void SessionList::appendItem(QString filename)
{
    emit preItemAppended(filename);
    QSharedPointer<Session> item = QSharedPointer<Session>(new Session(filename));
    this->_session.append(item);
    emit postItemAppended();
}

void SessionList::removeItem(int index)
{
    emit preItemRemoved();
    this->_session.removeAt(index);
    emit postItemRemoved();
}


