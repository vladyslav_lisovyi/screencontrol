#include "playlistcollectionmodel.h"

#include "src/model/playlistcollectionlist.h"
#include <QDebug>

PlaylistCollectionModel::PlaylistCollectionModel(QObject *parent)
    : QAbstractListModel(parent)
{
    _playlistCollection = QSharedPointer<PlaylistCollectionList>(new PlaylistCollectionList());
}

PlaylistCollectionModel::~PlaylistCollectionModel()
{
    qDebug() << "~PlaylistCollectionModel()";
    _playlistCollection.clear();
}

int PlaylistCollectionModel::rowCount(const QModelIndex &parent) const
{
    // For list models only the root node (an invalid parent) should return the list's size. For all
    // other (valid) parents, rowCount() should return 0 so that it does not become a tree model.
    if (parent.isValid())
        return 0;

    // FIXME: Implement me!
    return _playlistCollection->playlistColletion().size();
}

QVariant PlaylistCollectionModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    const PlaylistCollection item = _playlistCollection->playlistColletion().at(index.row());
    switch (role) {
    case playlistName:
        return QVariant(item.playlistName());
    case videoplaylist:
        return QVariant::fromValue(item.videoplaylist().get());
    case videoplaylistModel:
        return QVariant::fromValue(item.videoPlaylistModel().get());
    }

    // FIXME: Implement me!
    return QVariant();
}

bool PlaylistCollectionModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if(!_playlistCollection)
        return false;

    PlaylistCollection item = _playlistCollection->playlistColletion().at(index.row());
    switch (role) {
    case playlistName:
        item.setPlaylistName(value.toString());
        break;
    }

    if (_playlistCollection->setItemAt(index.row(), item)) {
        // FIXME: Implement me!
        emit dataChanged(index, index, QVector<int>() << role);
        return true;
    }
    return false;
}

Qt::ItemFlags PlaylistCollectionModel::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return Qt::NoItemFlags;

    return Qt::ItemIsEditable; // FIXME: Implement me!
}

QHash<int, QByteArray> PlaylistCollectionModel::roleNames() const
{
    QHash<int, QByteArray> names;
    names[playlistName] = "playlistName";
    names[videoplaylist] = "videoplaylist";
    names[videoplaylistModel] = "videoplaylistModel";
    return names;
}

//void PlaylistCollectionModel::setPlaylistCollection(PlaylistCollectionList *playlistCollection)
void PlaylistCollectionModel::setPlaylistCollection(QSharedPointer<PlaylistCollectionList> playlistCollection)
{
    beginResetModel();

    if(_playlistCollection)
    {
        _playlistCollection->disconnect(this);
    }
    //_playlistCollection = QSharedPointer<PlaylistCollectionList>(playlistCollection);
    _playlistCollection = playlistCollection;

    if(_playlistCollection)
    {
        connect(_playlistCollection.get(), &PlaylistCollectionList::preItemAppended, this, [=](QString source) {
            const int index = _playlistCollection->playlistColletion().size();
            beginInsertRows(QModelIndex(), index, index);
        });
        connect(_playlistCollection.get(), &PlaylistCollectionList::postItemAppended, this, [=]() {
            endInsertRows();
        });
        connect(_playlistCollection.get(), &PlaylistCollectionList::preItemRemoved, this, [=](int index) {
            beginRemoveRows(QModelIndex(), index, index);
        });
        connect(_playlistCollection.get(), &PlaylistCollectionList::postItemRemoved, this, [=]() {
            endRemoveRows();
        });
    }
    endResetModel();
    emit playlistCollectionChanged();
}
