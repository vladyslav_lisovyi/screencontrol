#include "displayresolution.h"

DisplayResolution::DisplayResolution(QObject *parent) : QObject(parent)
{
    _res = {};
}

DisplayResolution::DisplayResolution(Display &display, QObject *parent)
{
    _res = display.getAvailableResolutions();
}

QList<QRect> DisplayResolution::res() const
{
    return _res;
}

void DisplayResolution::setRes(const QList<QRect> &res)
{
    _res = res;
}

bool DisplayResolution::changeResItem(int index, const QRect &item)
{
    if(index < 0 || index >= this->_res.size())
        return false;

    //this->_displays[index] = QSharedPointer<Display>(new Display(item));
    this->_res[index] = item;
    return true;
}
