#include "displaygrid.h"

DisplayGrid::DisplayGrid(QObject *parent) : QObject(parent)
{
    initDisplays();
}

bool DisplayGrid::setItemAt(int index, const Display &item)
{
    if(index < 0 || index >= this->_displays.size())
        return false;

    //const Display& oldItem = *this->_displays.at(index);
    const Display& oldItem = this->_displays.at(index);
    if(oldItem.getPhysicalDisplay()->getDeviceName() == item.getPhysicalDisplay()->getDeviceName())
        return false;

    //this->_displays[index] = QSharedPointer<Display>(new Display(item));
    this->_displays[index] = Display(item);
    return true;
}

void DisplayGrid::appendItem()
{
    emit preItemAppended();

//    QSharedPointer<Display> item;
//    this->_displays.append(item);

    Display item;
    this->_displays.append(item);

    emit postItemAppended();
}

void DisplayGrid::removeItem(int index) // don't know, can init slot with arg
{
    emit preItemRemoved(index);

    this->_displays.removeAt(index);

    emit postItemRemoved();
}

void DisplayGrid::updateDisplaylist() // CHECH SLOT
{
    emit preDisplaylistUpdated();

    initDisplays();

    emit postDisplaylistUpdated();
}

void DisplayGrid::initDisplays()
{
    _displaylist = QSharedPointer<DisplaysList>(new DisplaysList());
    if(DisplaysList::logicalDisplays.size() <= 0)
    {
        for(int i=0; i < _displaylist->logicalDisplays.count(); i++)
                for(int j=0; j < _displaylist->physicalDisplays.count(); j++)
                {
                    //auto str1 = displaylist.logicalDisplays[i].getScreenName().split(QRegExp("\\W+"))[1];
                    //auto str2 = displaylist.physicalDisplays[j].getDeviceName().split(QRegExp("\\W+"))[1];
                    if(_displaylist->logicalDisplays[i]->getScreenName().split(QRegExp("\\W+"))[1] == _displaylist->physicalDisplays[j]->getDeviceName().split(QRegExp("\\W+"))[1])
                    {
                        //QSharedPointer<Display> display = QSharedPointer<Display>(new Display((_displaylist->physicalDisplays[j]), (_displaylist->logicalDisplays[i])));
                        Display display = Display((_displaylist->physicalDisplays[j]), (_displaylist->logicalDisplays[i]));
                        this->_displays.append(display);
                    }
                }
    }
    else
    {
        for(int i=0; i < _displaylist->logicalDisplays.count(); i++)
                for(int j=0; j < _displaylist->physicalDisplays.count(); j++)
                {
                    //auto str1 = displaylist.logicalDisplays[i].getScreenName().split(QRegExp("\\W+"))[1];
                    //auto str2 = displaylist.physicalDisplays[j].getDeviceName().split(QRegExp("\\W+"))[1];
                    if(_displaylist->logicalDisplays[i]->getScreenName().split(QRegExp("\\W+"))[1] == _displaylist->physicalDisplays[j]->getDeviceName().split(QRegExp("\\W+"))[1])
                    {
                        //QSharedPointer<Display> display = QSharedPointer<Display>(new Display((_displaylist->physicalDisplays[j]), (_displaylist->logicalDisplays[i])));
                        Display display = Display((_displaylist->physicalDisplays[j]), (_displaylist->logicalDisplays[i]));
                        this->_displays.append(display);
                    }
                }
    }
}
