#ifndef DISPLAYRESOLUTION_H
#define DISPLAYRESOLUTION_H

#include <QObject>
#include <QRect>
#include <QVector>
#include "src/model/display.h"

class DisplayResolution : public QObject
{
    Q_OBJECT
public:
    explicit DisplayResolution(QObject *parent = nullptr);
    explicit DisplayResolution(Display &display, QObject *parent = nullptr);

    QList<QRect> res() const;
    void setRes(const QList<QRect> &res);

    bool changeResItem(int index, const QRect &item);

signals:

public slots:

private:
    QList<QRect> _res;
};

#endif // DISPLAYRESOLUTION_H
