#ifndef DISPLAYRESOLUTIONLIST_H
#define DISPLAYRESOLUTIONLIST_H

#include <QAbstractListModel>
#include "src/model/displayresolution.h"

class DisplayResolution;

class DisplayResolutionList : public QAbstractListModel
{
    Q_OBJECT
    Q_PROPERTY(DisplayResolution* resolutionList READ displayResolution)

public:
    explicit DisplayResolutionList(QObject *parent = nullptr);
    explicit DisplayResolutionList(Display &display, QObject *parent = nullptr);

    enum {
        widthRes = Qt::UserRole,
        heightRes,
        resolutionStr
    };

    // Basic functionality:
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    // Editable:
    bool setData(const QModelIndex &index, const QVariant &value,
                 int role = Qt::EditRole) override;

    Qt::ItemFlags flags(const QModelIndex& index) const override;

    virtual QHash<int, QByteArray> roleNames() const override;

    DisplayResolution* displayResolution() const { return _displaysResolution.get(); }

private:
    QSharedPointer<DisplayResolution> _displaysResolution;
};

#endif // DISPLAYRESOLUTIONLIST_H
