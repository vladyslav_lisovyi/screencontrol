#include "display.h"

Display::Display()
{
    this->_physicalDisplay = QSharedPointer<PhysicalDisplay>(nullptr);
    this->_logicalDisplay = QSharedPointer<LogicalDisplay>(nullptr);
//    this->_physicalDisplay = new PhysicalDisplay();
//    this->_logicalDisplay = new LogicalDisplay();
    this->_idDisplay = -1;
    this->_availableResolutions = {};
}

Display::Display(QSharedPointer<PhysicalDisplay> physicalDisplay, QSharedPointer<LogicalDisplay> logicalDisplay)
{
    this->_physicalDisplay = physicalDisplay;
    this->_logicalDisplay = logicalDisplay;
//    this->_physicalDisplay = new PhysicalDisplay(physicalDisplay);
//    this->_logicalDisplay = new LogicalDisplay(logicalDisplay);
    this->_idDisplay = this->_logicalDisplay->getID();

    DEVMODE dm;
    memset(&dm, 0, sizeof(dm));
    dm.dmSize = sizeof(DEVMODE);
    QRect rect;
    for( int iModeNum = 0; EnumDisplaySettings(this->_physicalDisplay->getAdapterDevice().DeviceName, iModeNum, &dm ) != 0; iModeNum++ )
    {
        if(iModeNum == 0)
        {
            rect = QRect(0,0,(int)dm.dmPelsWidth,(int)dm.dmPelsHeight);
            this->_availableResolutions.append(rect);
        }
        else
        {
            if(rect.width() != (int)dm.dmPelsWidth && rect.height() != (int)dm.dmPelsHeight)
            {
                rect = QRect(0,0,(int)dm.dmPelsWidth,(int)dm.dmPelsHeight);
                this->_availableResolutions.append(rect);
            }
        }
    }
}

Display::Display(const Display &display)
{
    this->_idDisplay = display._idDisplay;
    this->_upPosition = display._upPosition;
    this->_downPosition = display._downPosition;
    this->_leftPosition = display._leftPosition;
    this->_rightPosition = display._rightPosition;
    this->_logicalDisplay = display._logicalDisplay; //QSharedPointer<LogicalDisplay>(display.);
    this->_physicalDisplay = display._physicalDisplay;
    this->_availableResolutions = display._availableResolutions;
}

Display &Display::operator=(const Display &device)
{
    if(this == &device)
        return *this;
    this->_physicalDisplay = device._physicalDisplay;
    this->_logicalDisplay = device._logicalDisplay;
    this->_idDisplay = device._idDisplay;
    this->_upPosition = device._upPosition;
    this->_downPosition = device._downPosition;
    this->_leftPosition = device._leftPosition;
    this->_rightPosition = device._rightPosition;
    this->_availableResolutions = device._availableResolutions;
    return *this;
}

//PhysicalDisplay &Display::getPhysicalDisplay() const
//{
//    //return *(this->_physicalDisplay.get());
//    return *(this->_physicalDisplay.get());
//}

//LogicalDisplay &Display::getLogicalDisplay() const
//{
//    //return *(this->_logicalDisplay.get());
//    return *(this->_logicalDisplay.get());
//}

QSharedPointer<PhysicalDisplay> Display::getPhysicalDisplay() const
{
    //return *(this->_physicalDisplay.get());
    return this->_physicalDisplay;
}

QSharedPointer<LogicalDisplay> Display::getLogicalDisplay() const
{
    //return *(this->_logicalDisplay.get());
    return this->_logicalDisplay;
}

int Display::IdDisplay() const
{
    return this->_idDisplay;
}

long Display::changeResolution(unsigned long width, unsigned long height)
{
    DEVMODE dm;
    LONG result = -1;
    memset(&dm, 0, sizeof(dm));
    dm.dmSize = sizeof(DEVMODE);

    if(EnumDisplaySettings(this->_physicalDisplay->getAdapterDevice().DeviceName,ENUM_CURRENT_SETTINGS,&dm))
    {
        dm.dmPelsWidth = width;
        dm.dmPelsHeight = height;
        // or ChangeDisplaySettings
        result = ChangeDisplaySettingsExW(this->_physicalDisplay->getAdapterDevice().DeviceName,
                                          &dm,
                                          0,
                                          0,
                                          0);
    }
    return result;
}

long Display::setPrimaryScreen()
{
    DEVMODE dm1;
    memset(&dm1, 0, sizeof(dm1));
    dm1.dmSize = sizeof(DEVMODE);

    DEVMODE dm2;
    memset(&dm2, 0, sizeof(dm2));
    dm2.dmSize = sizeof(DEVMODE);

    //auto offsetx = 0;
    //auto offsety = 0;
    long result = -1;
    if(EnumDisplaySettings(this->_physicalDisplay->getAdapterDevice().DeviceName,ENUM_CURRENT_SETTINGS,&dm2) &&
            EnumDisplaySettings(NULL,ENUM_CURRENT_SETTINGS,&dm1))
    {
        dm1.dmFields = DM_POSITION | DM_PELSWIDTH | DM_PELSHEIGHT;
        //offsetx = dm1.dmPosition.x;
        //offsety = dm1.dmPosition.y;
        dm1.dmPosition.x = (long)dm2.dmPelsWidth; //(long)dm1.dmPelsWidth;
        dm1.dmPosition.y = 0; //(long)dm1.dmPelsHeight;

        result = ChangeDisplaySettingsExW(NULL,
                                 &dm1,
                                 0,
                                 (CDS_UPDATEREGISTRY), //CDS_SET_PRIMARY, // | CDS_UPDATEREGISTRY, // | CDS_NORESET,
                                 NULL);

        // (CDS_UPDATEREGISTRY & CDS_NORESET) or CDS_UPDATEREGISTRY - change primary without change position
        //auto list = DisplaysList().physicalDisplays;

        dm2.dmFields = DM_POSITION | DM_PELSWIDTH | DM_PELSHEIGHT;
        dm2.dmPosition.x = 0;
        dm2.dmPosition.y = 0;

        result = ChangeDisplaySettingsExW(this->_physicalDisplay->getAdapterDevice().DeviceName,
                                 &dm2,
                                 0,
                                 CDS_SET_PRIMARY, // | CDS_UPDATEREGISTRY, //CDS_SET_PRIMARY, // | CDS_UPDATEREGISTRY, // | CDS_NORESET,
                                 NULL);
        // (CDS_SET_PRIMARY | (CDS_UPDATEREGISTRY | CDS_NORESET) - change primary without change position

        ChangeDisplaySettingsExW(NULL, new DEVMODE(), 0, 0, NULL);

//        for (uint otherid = 0; otherid < list.count(); otherid++)
//        {
//            if (list[otherid].getAdapterDevice().DeviceName != this->_physicalDisplay->getAdapterDevice().DeviceName)
//            {
//                DEVMODE dm2;
//                memset(&dm2, 0, sizeof(dm2));
//                dm2.dmSize = sizeof(DEVMODE);
//                EnumDisplaySettings(list[otherid].getAdapterDevice().DeviceName,
//                                    ENUM_CURRENT_SETTINGS,
//                                    &dm2);
//                dm2.dmPosition.x -= offsetx;
//                dm2.dmPosition.y -= offsety;
//                ChangeDisplaySettingsExW(list[otherid].getAdapterDevice().DeviceName,
//                                                 &dm2,
//                                                 0,
//                                                 CDS_UPDATEREGISTRY,
//                                                 0);
//            }
//        }
    }
    return result;
}

long Display::moveScreenUp()
{
    DEVMODE dm;
    LONG result = -1;
    memset(&dm, 0, sizeof(dm));
    dm.dmSize = sizeof(DEVMODE);

    if(EnumDisplaySettings(this->_physicalDisplay->getAdapterDevice().DeviceName,ENUM_CURRENT_SETTINGS,&dm))
    {
        dm.dmFields = DM_POSITION;
        dm.dmPosition.y -= 20;
        dm.dmPosition.x += 20;
        // or ChangeDisplaySettings
        result = ChangeDisplaySettingsExW(this->_physicalDisplay->getAdapterDevice().DeviceName,
                                          &dm,
                                          0,
                                          0,
                                          0);
    }
    return result;
}

long Display::moveScreenDown()
{
    DEVMODE dm;
    LONG result = -1;
    memset(&dm, 0, sizeof(dm));
    dm.dmSize = sizeof(DEVMODE);

    if(EnumDisplaySettings(this->_physicalDisplay->getAdapterDevice().DeviceName,ENUM_CURRENT_SETTINGS,&dm))
    {
        dm.dmFields = DM_POSITION;
        dm.dmPosition.y += 1;
        // or ChangeDisplaySettings
        result = ChangeDisplaySettingsExW(this->_physicalDisplay->getAdapterDevice().DeviceName,
                                          &dm,
                                          0,
                                          0,
                                          0);
    }
    return result;
}

long Display::moveScreenLeft()
{
    DEVMODE dm;
    LONG result = -1;
    memset(&dm, 0, sizeof(dm));
    dm.dmSize = sizeof(DEVMODE);

    if(EnumDisplaySettings(this->_physicalDisplay->getAdapterDevice().DeviceName,ENUM_CURRENT_SETTINGS,&dm))
    {
        dm.dmFields = DM_POSITION;
        dm.dmPosition.x -= 1;
        // or ChangeDisplaySettings
        result = ChangeDisplaySettingsExW(this->_physicalDisplay->getAdapterDevice().DeviceName,
                                          &dm,
                                          0,
                                          0,
                                          0);
    }
    return result;
}

long Display::moveScreenRight()
{
    DEVMODE dm;
    LONG result = -1;
    memset(&dm, 0, sizeof(dm));
    dm.dmSize = sizeof(DEVMODE);

    if(EnumDisplaySettings(this->_physicalDisplay->getAdapterDevice().DeviceName,ENUM_CURRENT_SETTINGS,&dm))
    {
        dm.dmFields = DM_POSITION;
        dm.dmPosition.x += 1;
        // or ChangeDisplaySettings
        result = ChangeDisplaySettingsExW(this->_physicalDisplay->getAdapterDevice().DeviceName,
                                          &dm,
                                          0,
                                          0,
                                          0);
    }
    return result;
}

long Display::showHideTaskBar(bool bHide)
{
    long long res = -1;
    //QRect rectWorkArea = QRect(0,0,0,0);
    //QRect rectTaskBar = QRect(0,0,0,0);

//    RECT rectWorkArea;
//    RECT rectTaskBar;

//    HWND pWnd = ::FindWindow(L"Shell_TrayWnd", L"");

//    if(bHide)
//    {
//        // Code to Hide the System Task Bar
//        SystemParametersInfo(SPI_GETWORKAREA,
//                             0,
//                             (LPVOID)&rectWorkArea,
//                             0);
//        if(pWnd)
//        {
//            ::GetWindowRect(pWnd, &rectTaskBar);
//            rectWorkArea.bottom += (rectTaskBar.top - rectTaskBar.bottom);
//            SystemParametersInfo(SPI_SETWORKAREA,
//                                 0,
//                                 (LPVOID)&rectWorkArea,
//                                 0);

//            ::ShowWindow(pWnd, SW_HIDE);
//        }
//    }
//    else
//    {
//        // Code to Show the System Task Bar
//        SystemParametersInfo(SPI_GETWORKAREA,
//                             0,
//                             (LPVOID)&rectWorkArea,
//                             0);
//        if(pWnd)
//        {
//            ::GetWindowRect(pWnd, &rectTaskBar);
//            rectWorkArea.bottom -= (rectTaskBar.top - rectTaskBar.bottom);
//            SystemParametersInfo(SPI_SETWORKAREA,
//                                 0,
//                                 (LPVOID)&rectWorkArea,
//                                 0);

//            ::ShowWindow(pWnd, SW_SHOW);
//        }
//    }

//    const QUrl REG_KEY("SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\Advanced");
//    const QString TaskbarEnabled("MMTaskbarEnabled");
//    const QString TaskbarMode("MMTaskbarMode");

    // -----------------------------------------------

    //QSettings settings("HKEY_CURRENT_USER\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Explorer\\Advanced",
    //                   QSettings::NativeFormat);
    //if(bHide)
    //    settings.setValue("MMTaskbarEnabled", 0);
    //else
    //    settings.setValue("MMTaskbarEnabled", 1);
    //settings.setValue("MMTaskbarEnabled", 0);
    //return settings.value("MMTaskbarEnabled").toInt();
//    //QThread::sleep(2);

//    if (QLibrary::isLibrary("User32.dll"))
//    {
//        QLibrary lib("User32.dll");
//        lib.load();
//        if (!lib.isLoaded())
//        {
//          res = 404;
//          return res;
//        }
//        if (lib.isLoaded())
//        {
//            typedef bool (*FunctionPrototype)(HWND, UINT, WPARAM, LPARAM);
//            auto sendMessage = (FunctionPrototype)lib.resolve("SendNotifyMessage");
            //if(!sendMessage)
//            try {
//                res = sendMessage(HWND_BROADCAST, WM_SETTINGCHANGE, NULL, (LPARAM)("TraySettings"));
//            } catch (...) {
//                res = 404;
//            }
//        }
//    }
    //res = SendNotifyMessage((HWND_BROADCAST), WM_SETTINGCHANGE, NULL, (LPARAM)("TraySettings"));
    //res = SendNotifyMessageW(HWND_BROADCAST, WM_SETTINGCHANGE, NULL, (LPARAM)("TraySettings"));

    //UpdateWindow((HWND_BROADCAST));
    //res = SendMessageTimeout((HWND_BROADCAST), WM_SETTINGCHANGE, NULL,(LPARAM)("TraySettings"), 0,100, 0 );
    //res = SendNotifyMessage((HWND_BROADCAST), WM_SETTINGCHANGE, NULL, (LPARAM)("TraySettings"));
    //res = SendMessage((HWND_BROADCAST), WM_SETTINGCHANGE, NULL, (LPARAM)("TraySettings"));
    //res = SendNotifyMessageW((HWND_BROADCAST), WM_SETTINGCHANGE, NULL, (LPARAM)("TraySettings"));
    //QThread::sleep(2);

    // --------------------------------------------------

    QSettings settings("HKEY_CURRENT_USER\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Explorer\\Advanced",
                       QSettings::NativeFormat);
    settings.setValue("MMTaskbarEnabled", 0);
    APPBARDATA	apd = {0};
    apd.cbSize = sizeof(apd);
    apd.hWnd = FindWindow( _T("Shell_TrayWnd"), NULL );
    LPARAM orgState = SHAppBarMessage( ABM_GETSTATE, &apd );
    apd.lParam = orgState;
    apd.lParam |= 1;
    SHAppBarMessage( ABM_SETSTATE, &apd );
    QThread::sleep(0.2);
    apd.lParam &= ~1;
    SHAppBarMessage( ABM_SETSTATE, &apd );

    // ----------------------------------------------------

//    HKEY hKey;
//    DWORD buffersize = 1024;
//    char *buffer = new char[buffersize];

//    RegOpenKeyExW(HKEY_CURRENT_USER, _T("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Explorer\\Advanced"),NULL,KEY_READ,&hKey);
//    RegQueryValueExW(hKey,_T("MMTaskbarEnabled"),NULL,NULL,(LPBYTE) buffer, &buffersize);
//    DWORD value = 0;
//    LONG result_error = RegSetValueExW(hKey,_T("MMTaskbarEnabled"), 0, REG_DWORD, (const BYTE*)&value, sizeof(value));
//    RegCloseKey (hKey);

    return res;
}

void Display::moveContentUp(int pixelValue)
{
    this->_upPosition += pixelValue;
    this->_downPosition -= pixelValue;
}

void Display::moveContentDown(int pixelValue)
{
    this->_downPosition += pixelValue;
    this->_upPosition -= pixelValue;
}

void Display::moveContentLeft(int pixelValue)
{
    this->_leftPosition += pixelValue;
    this->_rightPosition -= pixelValue;
}

void Display::moveContentRight(int pixelValue)
{
    this->_rightPosition += pixelValue;
    this->_leftPosition -= pixelValue;
}
