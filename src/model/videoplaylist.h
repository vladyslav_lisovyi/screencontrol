#ifndef VIDEOPLAYLIST_H
#define VIDEOPLAYLIST_H

#include <QObject>
#include <QList>
#include "src/model/videocontent.h"

class VideoPlaylist : public QObject
{
    Q_OBJECT

public:
    //explicit VideoPlaylist(QObject *parent = nullptr);
    VideoPlaylist();
    VideoPlaylist(const VideoPlaylist &playlist);
    VideoPlaylist& operator=(const VideoPlaylist& playlist);
    ~VideoPlaylist();

    QList<VideoContent> list() const;

    bool setItemAt(int index, const VideoContent &item);

    QString playlistName() const;

    void initVideoPlaylistByFolder(QString source); // You choose: file:///C:/jj4

    Q_INVOKABLE
    QString getSource(int index);

    QString sourceForPlaylistCollection();

    Q_INVOKABLE
    int size() { return mList.size(); }

signals:
    //void itemAppended(QString file);
    void preItemAppended(QString file);
    void postItemAppended();
    void playlistNameChanged(QString name);
    void preItemRemoved(int index);
    void postItemRemoved();

    void preItemChanged(int index, QString source);
    void postItemChanged();

    void preUpVideoChanged(int index);
    void postUpVideoChanged();

    void preDownVideoChanged(int index);
    void postDownVideoChanged();

public slots:
    void appendItem(QString file);
    void setPlaylistName(const QString &playlistName);
    void removeItem(int index);
    void changeVideo(int index, QString source);

    void upVideo(int index);
    void downVideo(int index);

private:
    QList<VideoContent> mList;
    QString mPlaylistName;
};

#endif // VIDEOPLAYLIST_H
