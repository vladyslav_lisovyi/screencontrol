#ifndef DISPLAYGRIDMODEL_H
#define DISPLAYGRIDMODEL_H

#include <QAbstractListModel>

#include "src/model/displaygrid.h"

class DisplayGrid;

class DisplayGridModel : public QAbstractListModel
{
    Q_OBJECT
    Q_PROPERTY(DisplayGrid* grid READ grid)

public:
    explicit DisplayGridModel(QObject *parent = nullptr);

    enum {
        DisplayName = Qt::UserRole,
        DisplayId
    };

    // Basic functionality:
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    // Editable:
    bool setData(const QModelIndex &index, const QVariant &value,
                 int role = Qt::EditRole) override;

    Qt::ItemFlags flags(const QModelIndex& index) const override;

    virtual QHash<int, QByteArray> roleNames() const override;

    DisplayGrid* grid() const;
    void setGrid(DisplayGrid* grid);

    Q_INVOKABLE
    int getDisplayIndex(QString idDisplay);

private:
    QSharedPointer<DisplayGrid> _grid;
};

#endif // DISPLAYGRIDMODEL_H
