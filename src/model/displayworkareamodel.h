#ifndef DISPLAYWORKAREAMODEL_H
#define DISPLAYWORKAREAMODEL_H

#include <QAbstractListModel>
#include <QMediaPlaylist>

#include "src/model/displaycells.h"
#include "src/model/displayresolutionlist.h"
//#include "src/model/videoplaylistmodel.h"

class DisplayCells;

class DisplayWorkareaModel : public QAbstractListModel
{
    Q_OBJECT
    Q_PROPERTY(DisplayCells* displayCells READ displayCells)

public:
    explicit DisplayWorkareaModel(QObject *parent = nullptr);
    explicit DisplayWorkareaModel(int size, QObject *parent = nullptr);
    ~DisplayWorkareaModel() override;

    enum {
        Position = Qt::UserRole,
        Resolution,
        availableDisplay,
        idDisplay,
        physID,
        blendingTop,
        blendingBottom,
        blendingLeft,
        blendingRight,
        blendingPositionTop,
        blendingPositionBottom,
        blendingPositionLeft,
        blendingPositionRight,
        blendingOpacityTop,
        blendingOpacityBottom,
        blendingOpacityLeft,
        blendingOpacityRight,
        videoPlaylist,
        primaryScreen
    };

    // Basic functionality:
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    // Editable:
    bool setData(const QModelIndex &index, const QVariant &value,
                 int role = Qt::EditRole) override;

    Qt::ItemFlags flags(const QModelIndex& index) const override;

    virtual QHash<int, QByteArray> roleNames() const override;

    DisplayCells *displayCells() const;
    void setDisplayCells(DisplayCells *displayCells);
    void addDisplayCell(const DisplayCell &displayCell);

    void initDisplayIdForEditor();

    Q_INVOKABLE
    void changePositionToRight(int index, int offset);
    Q_INVOKABLE
    void changePositionToLeft(int index, int offset);
    Q_INVOKABLE
    void changePositionToTop(int index, int offset);
    Q_INVOKABLE
    void changePositionToBottom(int index, int offset);

    Q_INVOKABLE QVariantMap get(int row);

    Q_INVOKABLE
    QAbstractItemModel* resolutionList(int index);

//    Q_INVOKABLE
//    int resolutionIndex(int index);

//    Q_INVOKABLE
//    QAbstractItemModel* playlistModel(int index);

//    Q_INVOKABLE
//    void addVideo(int index, QString src);
//    Q_INVOKABLE
//    void deleteVideo(int index, int videoIndex);
//    Q_INVOKABLE
//    void changeVideoInList(int index, QString src);
    Q_INVOKABLE
    QList<QUrl> getSourcesForPlaylist(int index);
//    Q_INVOKABLE
//    void itemPosUp(int index, int videoIndex);
//    Q_INVOKABLE
//    void itemPosDown(int index, int videoIndex);

//    Q_INVOKABLE
//    void writeToFile(int displayIndex, int videoIndex);
//    Q_INVOKABLE
//    int getDisplayIndexFromFile();
//    Q_INVOKABLE
//    int getVideoindexFromFile();

    Q_INVOKABLE
    void changeTopBlending(int index, int offset);
    Q_INVOKABLE
    void changeBottomBlending(int index, int offset);
    Q_INVOKABLE
    void changeLeftBlending(int index, int offset);
    Q_INVOKABLE
    void changeRightBlending(int index, int offset);

    Q_INVOKABLE
    void changeTopPositionBlending(int index, double offset);
    Q_INVOKABLE
    void changeBottomPositionBlending(int index, double offset);
    Q_INVOKABLE
    void changeLeftPositionBlending(int index, double offset);
    Q_INVOKABLE
    void changeRightPositionBlending(int index, double offset);

    Q_INVOKABLE
    void changeTopOpacityBlending(int index, double offset);
    Q_INVOKABLE
    void changeBottomOpacityBlending(int index, double offset);
    Q_INVOKABLE
    void changeLeftOpacityBlending(int index, double offset);
    Q_INVOKABLE
    void changeRightOpacityBlending(int index, double offset);

    //Q_INVOKABLE
    //VideoPlaylistModel* playlistModelForSave(int index);

//    Q_INVOKABLE
//    QList<QUrl> playlist(int index);

private:
    QSharedPointer<DisplayCells> _displayCells;
};

#endif // DISPLAYWORKAREAMODEL_H
