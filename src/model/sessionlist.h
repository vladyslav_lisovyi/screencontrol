#ifndef SESSIONLIST_H
#define SESSIONLIST_H

#include <QObject>
#include "src/model/session.h"

class SessionList : public QObject
{
    Q_OBJECT
    //Q_PROPERTY(Session* selectedSession READ selectedSession)
public:
    explicit SessionList(QObject *parent = nullptr);
    QList<QSharedPointer<Session>> sessions() const {return this->_session;}
    bool setItemAt(int index, const QSharedPointer<Session> item);

    bool checkAvailableSaves();
    bool loadingSessionsFromFolder();
    void loadSave(int index);

    Q_INVOKABLE
    int createSave(QString filename);
//    Q_INVOKABLE
//    void selectSession(int index);

//    Q_INVOKABLE
//    void saveCurrentSession(DisplaysWorkarea* workarea, PlaylistCollectionList* collection);
//    Q_INVOKABLE
//    void loadCurrentSession();

//    Q_INVOKABLE
//    void deleteVideoForDisplay(int displayIndex, int videoIndexFromCurentPlaylist);

//    Session* selectedSession()
//    {
//        return m_selectedSession.get();
//    }

signals:
    void preItemAppended(QString filename);
    void postItemAppended();

    void preItemRemoved();
    void postItemRemoved();

public slots:
    void appendItem(QString filename);
    void removeItem(int index);

private:
    QList<QSharedPointer<Session>> _session;
    //QSharedPointer<Session> m_selectedSession;
};

#endif // SESSIONLIST_H
