#include "displaygridmodel.h"
#include <QDebug>

//#include "src/model/displaygrid.h"

DisplayGridModel::DisplayGridModel(QObject *parent)
    : QAbstractListModel(parent)
{
    this->_grid = QSharedPointer<DisplayGrid>(new DisplayGrid());
}

int DisplayGridModel::rowCount(const QModelIndex &parent) const
{
    // For list models only the root node (an invalid parent) should return the list's size. For all
    // other (valid) parents, rowCount() should return 0 so that it does not become a tree model.
    if (parent.isValid() || !_grid)
        return 0;

    // FIXME: Implement me!
    return _grid->displays().size();
}

QVariant DisplayGridModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid() || !_grid)
        return QVariant();

    // FIXME: Implement me!
    //const QSharedPointer<Display> item = _grid;
    //const Display item = (*((*_grid.get()).displays().at(index.row())).get());
    const Display item = _grid->displays().at(index.row());
    //qDebug() << item.getPhysicalDisplay() getDeviceName();
    switch (role) {
    case DisplayName:
        return QVariant((item.getPhysicalDisplay()->getDeviceName()));
        //return QVariant(QStringLiteral("Test display name"));
    case DisplayId:
        return QVariant((item.IdDisplay()));
    }
    return QVariant();
}

bool DisplayGridModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if(!_grid)
        return false;

    //Display item = *_grid->displays().at(index.row());
    Display item = _grid->displays().at(index.row());
    switch (role) {
    case DisplayName:
        item.getPhysicalDisplay()->setDeviceName(value.toString()); // TEST SET, NEED TO BE CHANGE !!!!!!!!!!!!!!!!!!!!!!!!!!
        break;
        //return QVariant(QStringLiteral("Test display name"));
    }

    //if (data(index, role) != value) {
    if (_grid->setItemAt(index.row(), item)) {
        // FIXME: Implement me!
        emit dataChanged(index, index, QVector<int>() << role);
        return true;
    }
    return false;
}

Qt::ItemFlags DisplayGridModel::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return Qt::NoItemFlags;

    return Qt::ItemIsEditable; // FIXME: Implement me!
}

QHash<int, QByteArray> DisplayGridModel::roleNames() const
{
    QHash<int, QByteArray> names;
    names[DisplayName] = "displayName";
    names[DisplayId] = "displayId";
    return names;
}

DisplayGrid* DisplayGridModel::grid() const
{
    return _grid.get();
}

void DisplayGridModel::setGrid(DisplayGrid* grid)
{
    beginResetModel();

    if(_grid)
        _grid->disconnect(this);

    _grid = QSharedPointer<DisplayGrid>(grid);

    if (_grid) {
        connect(_grid.get(), &DisplayGrid::preItemAppended, this, [=]() {
            const int index = _grid->displays().size();
            beginInsertRows(QModelIndex(), index, index);
        });
        connect(_grid.get(), &DisplayGrid::postItemAppended, this, [=]() {
            endInsertRows();
        });

        connect(_grid.get(), &DisplayGrid::preItemRemoved, this, [=](int index) {
            beginRemoveRows(QModelIndex(), index, index);
        });
        connect(_grid.get(), &DisplayGrid::postItemRemoved, this, [=]() {
            endRemoveRows();
        });
    }

    endResetModel();
}

int DisplayGridModel::getDisplayIndex(QString idDisplay)
{
    int retValue = -1;
    if(!_grid)
        return -1;
    auto vector = _grid->displays();
    for(int i = 0; i < vector.size(); i++)
    {
        //if(vector.at(i).getPhysicalDisplay()->getDeviceId() == display->getPhysicalDisplay()->getDeviceId())
        if(vector.at(i).getPhysicalDisplay()->getDeviceId() == idDisplay)
            retValue = i;
    }
    return retValue;
}

//DisplayGrid *DisplayGridModel::grid() const
//{
//    return _grid;
//}

//void DisplayGridModel::setGrid(DisplayGrid *grid)
//{
//    beginResetModel();

//    if(_grid)
//        _grid->disconnect(this);

//    _grid = QSharedPointer<DisplayGrid>(grid);

//    if (_grid) {
//        connect(_grid.get(), &DisplayGrid::preItemAppended, this, [=]() {
//            const int index = _grid->displays().size();
//            beginInsertRows(QModelIndex(), index, index);
//        });
//        connect(_grid.get(), &DisplayGrid::postItemAppended, this, [=]() {
//            endInsertRows();
//        });

//        connect(_grid.get(), &DisplayGrid::preItemRemoved, this, [=](int index) {
//            beginRemoveRows(QModelIndex(), index, index);
//        });
//        connect(_grid.get(), &DisplayGrid::postItemRemoved, this, [=]() {
//            endRemoveRows();
//        });
//    }

//    endResetModel();
//}
