#ifndef VIDEOPLAYLISTMODEL_H
#define VIDEOPLAYLISTMODEL_H

#include <QAbstractListModel>
#include <QSharedPointer>

class VideoPlaylist;

class VideoPlaylistModel : public QAbstractListModel
{
    Q_OBJECT
    Q_PROPERTY(VideoPlaylist *playlist READ playlist NOTIFY playlistChanged) // WRITE setPlaylist

public:
    explicit VideoPlaylistModel(QObject *parent = nullptr);
    ~VideoPlaylistModel() override;

    enum {
        filename = Qt::UserRole,
        source
    };

    // Basic functionality:
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    // Editable:
    bool setData(const QModelIndex &index, const QVariant &value,
                 int role = Qt::EditRole) override;

    Qt::ItemFlags flags(const QModelIndex& index) const override;

    virtual QHash<int, QByteArray> roleNames() const override;

    VideoPlaylist * playlist() const
    {
        return m_playlist.get();
    }

public slots:
    void setPlaylist(QSharedPointer<VideoPlaylist> playlist);
    void updatePlaylist();

signals:
    void playlistChanged(VideoPlaylist * playlist);

private:
    QSharedPointer<VideoPlaylist> m_playlist;
};

#endif // VIDEOPLAYLISTMODEL_H
