#ifndef PLAYLISTCOLLECTION_H
#define PLAYLISTCOLLECTION_H

#include "src/model/videoplaylist.h"
#include <QString>
#include <QSharedPointer>
#include "src/model/videoplaylistmodel.h"

class PlaylistCollection
{
public:
    PlaylistCollection();
    PlaylistCollection(VideoPlaylist playlist);
    PlaylistCollection& operator=(const PlaylistCollection& item);
    ~PlaylistCollection();

    QSharedPointer<VideoPlaylist> videoplaylist() const;
    void setVideoplaylist(const QSharedPointer<VideoPlaylist> &videoplaylist);

    QString playlistName() const;
    void setPlaylistName(const QString &playlistName);

    QSharedPointer<VideoPlaylistModel> videoPlaylistModel() const { return _videoPlaylistModel;}

private:
    QSharedPointer<VideoPlaylist> _videoplaylist;
    QString _playlistName;
    QSharedPointer<VideoPlaylistModel> _videoPlaylistModel;
};

#endif // PLAYLISTCOLLECTION_H
