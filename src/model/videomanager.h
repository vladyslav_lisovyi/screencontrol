#ifndef VIDEOMANAGER_H
#define VIDEOMANAGER_H

#include <QObject>

class VideoManager : public QObject
{
    Q_OBJECT
    Q_PROPERTY(int displayIndex READ displayIndex WRITE setDisplayIndex NOTIFY displayIndexChanged)
    Q_PROPERTY(int videoIndex READ videoIndex WRITE setVideoIndex NOTIFY videoIndexChanged)
    Q_PROPERTY(bool pause READ pause WRITE setPause NOTIFY pausedChanged)

public:
    explicit VideoManager(QObject *parent = nullptr);
    //bool pause;

    int displayIndex() const;
    void setDisplayIndex(int displayIndex);

    int videoIndex() const;
    void setVideoIndex(int videoIndex);

    bool pause() const;
    void setPause(bool pause);

signals:
    void sendToQml(int i);

    void displayIndexChanged();
    void videoIndexChanged();
    void pausedChanged();

public slots:
    void receiveFromQml(int i);

private:
    int _displayIndex;
    int _videoIndex;
    bool _pause;
};

#endif // VIDEOMANAGER_H
