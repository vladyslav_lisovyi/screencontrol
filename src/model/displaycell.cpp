#include "displaycell.h"

#include <QDebug>

DisplayCell::DisplayCell()
{
    this->_position = QRect(0,0,0,0); // 1 - leftPoint, 2 - topPoint, 3 - width, 4 - height
    this->_resolution = QRect(0,0,0,0);
    this->_blendingTop = 0;
    this->_blendingLeft = 0;
    this->_blendingRight = 0;
    this->_blendingBottom = 0;

    this->_blendingOpacityTop = 1.0;
    this->_blendingOpacityLeft = 1.0;
    this->_blendingOpacityRight = 1.0;
    this->_blendingOpacityBottom = 1.0;

    this->_blendingPositionTop = 0.0;
    this->_blendingPositionLeft = 0.0;
    this->_blendingPositionRight = 1.0;
    this->_blendingPositionBottom = 1.0;

    this->_displayNumber = -1;

    this->_primaryScreen = false;

    _playlist = QSharedPointer<VideoPlaylist>(new VideoPlaylist(), &QObject::deleteLater);
    _displayResolutionList = QSharedPointer<DisplayResolutionList>(new DisplayResolutionList());
}

DisplayCell::DisplayCell(int x, int y, int width, int height)
{
    this->_position = QRect();//QRect(x, y, width, height);
    this->_position.setX(x);
    this->_position.setY(y);
    this->_position.setWidth(width);
    this->_position.setHeight(height);
    this->_resolution = QRect();
    this->_resolution.setWidth(width);
    this->_resolution.setHeight(height);
    this->_blendingTop = 0;
    this->_blendingLeft = 0;
    this->_blendingRight = 0;
    this->_blendingBottom = 0;

    this->_blendingOpacityTop = 1.0;
    this->_blendingOpacityLeft = 1.0;
    this->_blendingOpacityRight = 1.0;
    this->_blendingOpacityBottom = 1.0;

    this->_blendingPositionTop = 0.0;
    this->_blendingPositionLeft = 0.0;
    this->_blendingPositionRight = 1.0;
    this->_blendingPositionBottom = 1.0;

    this->_displayNumber = -1;
    this->_primaryScreen = false;
    _playlist = QSharedPointer<VideoPlaylist>(new VideoPlaylist(), &QObject::deleteLater);
    _displayResolutionList = QSharedPointer<DisplayResolutionList>(new DisplayResolutionList());
}

DisplayCell::DisplayCell(const DisplayCell &cell)
{
    this->_position = cell._position;
    this->_resolution = cell._resolution;
    this->_display = cell._display;
    this->_displayResolutionList = cell._displayResolutionList;
    //this->_videoPlaylistModel = cell._videoPlaylistModel;
    this->_blendingTop = cell._blendingTop;
    this->_blendingLeft = cell._blendingLeft;
    this->_blendingRight = cell._blendingRight;
    this->_blendingBottom = cell._blendingBottom;

    this->_blendingOpacityTop = cell._blendingOpacityTop;
    this->_blendingOpacityLeft = cell._blendingOpacityLeft;
    this->_blendingOpacityRight = cell._blendingOpacityRight;
    this->_blendingOpacityBottom = cell._blendingOpacityBottom;

    this->_blendingPositionTop = cell._blendingPositionTop;
    this->_blendingPositionLeft = cell._blendingPositionLeft;
    this->_blendingPositionRight = cell._blendingPositionRight;
    this->_blendingPositionBottom = cell._blendingPositionBottom;

    this->_displayNumber = cell._displayNumber;
    this->_playlist = cell._playlist;
    this->_primaryScreen = cell._primaryScreen;
}

DisplayCell &DisplayCell::operator=(const DisplayCell &cell)
{
    if(this == &cell)
        return *this;
    this->_position = cell._position;
    this->_resolution = cell._resolution;
    this->_display = cell._display;
    this->_displayResolutionList = cell._displayResolutionList;
    //this->_videoPlaylistModel = cell._videoPlaylistModel;
    this->_blendingTop = cell._blendingTop;
    this->_blendingLeft = cell._blendingLeft;
    this->_blendingRight = cell._blendingRight;
    this->_blendingBottom = cell._blendingBottom;

    this->_blendingOpacityTop = cell._blendingOpacityTop;
    this->_blendingOpacityLeft = cell._blendingOpacityLeft;
    this->_blendingOpacityRight = cell._blendingOpacityRight;
    this->_blendingOpacityBottom = cell._blendingOpacityBottom;

    this->_blendingPositionTop = cell._blendingPositionTop;
    this->_blendingPositionLeft = cell._blendingPositionLeft;
    this->_blendingPositionRight = cell._blendingPositionRight;
    this->_blendingPositionBottom = cell._blendingPositionBottom;

    this->_displayNumber = cell._displayNumber;
    this->_playlist = cell._playlist;
    this->_primaryScreen = cell._primaryScreen;

    return *this;
}

DisplayCell::~DisplayCell()
{
    qDebug() << "~DisplayCell";
    _display.clear();
    _displayResolutionList.clear();
    _playlist.clear();
}

QRect DisplayCell::position() const
{
    return _position;
}

void DisplayCell::setPosition(const QRect &position)
{
    _position = position;
}

void DisplayCell::setPosition(const int x, const int y, const int width, const int height)
{
    this->_position = QRect(x, y, width, height);
}

QRect DisplayCell::resolution() const
{
    return _resolution;
}

void DisplayCell::setResolution(const QRect &resolution)
{
    _resolution = resolution;
}

void DisplayCell::setResolution(const int width, const int height)
{
    this->_resolution = QRect(0, 0, width, height);
}

QSharedPointer<Display> DisplayCell::display() const
{
    return _display;
}

void DisplayCell::setDisplay(const QSharedPointer<Display> display)
{
    if(this->_display == nullptr)
    {
        _display = display;
        initResolutionList();
        //initPlaylist();
        return;
    }
    if(display->getPhysicalDisplay()->getDeviceId() == this->_display->getPhysicalDisplay()->getDeviceId())
        return;
    _display = display;
    initResolutionList();
    //initPlaylist();
//    for(int i = 0; i < DisplaysList::physicalDisplays.size(); i++)
//    {
//        if(displayId == DisplaysList::physicalDisplays[i].getDeviceId())
//            _display = Display();
    //    }
}

int DisplayCell::getResolutionIndex()
{
    int retValue = -1;
    auto list = _displayResolutionList->displayResolution()->res();
    for(int i = 0; i < list.size(); i++)
    {
        if(list.at(i).width() == _resolution.width() && list.at(i).height() == _resolution.height())
        {
            retValue = i;
            break;
        }
    }
    return retValue;
}

QSharedPointer<DisplayResolutionList> DisplayCell::displayResolutionList() const
{
    return _displayResolutionList;
}

void DisplayCell::setDisplayResolutionList(const QSharedPointer<DisplayResolutionList> &displayResolutionList)
{
    _displayResolutionList = displayResolutionList;
}

//QSharedPointer<VideoPlaylistModel> DisplayCell::videoPlaylistModel() const
//{
//    return _videoPlaylistModel;
//}

//void DisplayCell::setVideoPlaylistModel(const QSharedPointer<VideoPlaylistModel> &videoPlaylistModel)
//{
//    _videoPlaylistModel = videoPlaylistModel;
//}

int DisplayCell::getDisplayNumber() const
{
    return _displayNumber;
}

void DisplayCell::setDisplayNumber(int displayNumber)
{
    _displayNumber = displayNumber;
}

double DisplayCell::getBlendingPositionTop() const
{
    return _blendingPositionTop;
}

void DisplayCell::setBlendingPositionTop(double blendingPositionTop)
{
    _blendingPositionTop = blendingPositionTop;
}

double DisplayCell::getBlendingPositionBottom() const
{
    return _blendingPositionBottom;
}

void DisplayCell::setBlendingPositionBottom(double blendingPositionBottom)
{
    _blendingPositionBottom = blendingPositionBottom;
}

double DisplayCell::getBlendingPositionLeft() const
{
    return _blendingPositionLeft;
}

void DisplayCell::setBlendingPositionLeft(double blendingPositionLeft)
{
    _blendingPositionLeft = blendingPositionLeft;
}

double DisplayCell::getBlendingPositionRight() const
{
    return _blendingPositionRight;
}

void DisplayCell::setBlendingPositionRight(double blendingPositionRight)
{
    _blendingPositionRight = blendingPositionRight;
}

double DisplayCell::getBlendingOpacityTop() const
{
    return _blendingOpacityTop;
}

void DisplayCell::setBlendingOpacityTop(double blendingOpacityTop)
{
    _blendingOpacityTop = blendingOpacityTop;
}

double DisplayCell::getBlendingOpacityBottom() const
{
    return _blendingOpacityBottom;
}

void DisplayCell::setBlendingOpacityBottom(double blendingOpacityBottom)
{
    _blendingOpacityBottom = blendingOpacityBottom;
}

double DisplayCell::getBlendingOpacityLeft() const
{
    return _blendingOpacityLeft;
}

void DisplayCell::setBlendingOpacityLeft(double blendingOpacityLeft)
{
    _blendingOpacityLeft = blendingOpacityLeft;
}

double DisplayCell::getBlendingOpacityRight() const
{
    return _blendingOpacityRight;
}

void DisplayCell::setBlendingOpacityRight(double blendingOpacityRight)
{
    _blendingOpacityRight = blendingOpacityRight;
}

QSharedPointer<VideoPlaylist> DisplayCell::getPlaylist() const
{
    return _playlist;
}

void DisplayCell::setPlaylist(const QSharedPointer<VideoPlaylist> &playlist)
{
    _playlist = playlist;
}

bool DisplayCell::addVideo(QString source)
{
    //qDebug() << "DisplayCell::addVideo";
    QStringList splitSource = source.split(QRegExp("[/]+"));
    if(splitSource[0] != "file:")
        source = QString("file:///"+source);
    if(splitSource[splitSource.size()-1].split('.').size() < 2)
        return false;
    //qDebug() << "Source in addVideo " << source;
    _playlist->appendItem(source);
    return true;
}

bool DisplayCell::removeVideo(int index)
{
    if(!_playlist.get())
        return false;
    if(index < 0 || index > _playlist->list().size()-1)
        return false;
    _playlist->removeItem(index);
    return true;
}

bool DisplayCell::changeVideo(QString source)
{
    if(!_playlist.get())
        return false;
    QStringList splitSource = source.split(QRegExp("[/]+"));
    if(splitSource[0] != "file:")
        source = QString("file:///"+source);
    QStringList changedVideoList = source.split(QRegExp("[/]+"));
    int indexForChanged = -1;
    for(int i=0; i < _playlist->list().size(); i++)
    {
        QStringList list = _playlist->list()[i].getSource().split(QRegExp("[/]+"));
        if(list[list.size()-2] == changedVideoList[changedVideoList.size()-2])
        {
            indexForChanged = i;
            break;
        }
    }
    if(indexForChanged < 0)
        return false;
    _playlist->changeVideo(indexForChanged, source);
}

bool DisplayCell::primaryScreen() const
{
    return _primaryScreen;
}

void DisplayCell::setPrimaryScreen(bool primaryScreen)
{
    _primaryScreen = primaryScreen;
}

void DisplayCell::initResolutionList()
{
    _displayResolutionList = QSharedPointer<DisplayResolutionList>(new DisplayResolutionList(*(display())));
}

//void DisplayCell::initPlaylist()
//{
//    _videoPlaylistModel = QSharedPointer<VideoPlaylistModel>(new VideoPlaylistModel());
//}
