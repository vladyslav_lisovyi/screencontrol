#include "playlistcollection.h"

#include <QDebug>

PlaylistCollection::PlaylistCollection()
{
    _videoplaylist = QSharedPointer<VideoPlaylist>(new VideoPlaylist(), &QObject::deleteLater);
    _playlistName = "";
    _videoPlaylistModel = QSharedPointer<VideoPlaylistModel>(new VideoPlaylistModel());
    _videoPlaylistModel->setPlaylist(_videoplaylist);
}

PlaylistCollection &PlaylistCollection::operator=(const PlaylistCollection &item)
{
    if(this == &item)
        return *this;
    _playlistName = item._playlistName;
    _videoplaylist = item._videoplaylist;
    return *this;
}

PlaylistCollection::PlaylistCollection(VideoPlaylist playlist)
{
    //qDebug() << "PlaylistCollection(VideoPlaylist playlist)";
    _videoplaylist = QSharedPointer<VideoPlaylist>(new VideoPlaylist(playlist), &QObject::deleteLater);
    _playlistName = _videoplaylist->playlistName();
    //qDebug() << "set VideoPlaylistModel";
    _videoPlaylistModel = QSharedPointer<VideoPlaylistModel>(new VideoPlaylistModel(), &QObject::deleteLater);
    _videoPlaylistModel->setPlaylist(_videoplaylist);
}

PlaylistCollection::~PlaylistCollection()
{
    qDebug() << "~PlaylistCollection()";
    _videoplaylist.clear();
    //_videoPlaylistModel.clear();
}

QSharedPointer<VideoPlaylist> PlaylistCollection::videoplaylist() const
{
    return _videoplaylist;
}

void PlaylistCollection::setVideoplaylist(const QSharedPointer<VideoPlaylist> &videoplaylist)
{
    _videoplaylist = videoplaylist;
    _playlistName = _videoplaylist->playlistName();
    _videoPlaylistModel->setPlaylist(_videoplaylist);
}

QString PlaylistCollection::playlistName() const
{
    return _playlistName;
}

void PlaylistCollection::setPlaylistName(const QString &playlistName)
{
    _playlistName = playlistName;
}
