#ifndef VIDEOCONTENT_H
#define VIDEOCONTENT_H

#include <QString>
#include <QStringList>
#include <QRegExp>
#include <QRect>
#include <QUrl>

//#include <QtMultimedia/QtMultimedia>
//#include <QtMultimediaWidgets/QtMultimediaWidgets>
//#include <qtmedianamespace.h>

class VideoContent
{
public:
    VideoContent();
    VideoContent(QString src);
    VideoContent(const VideoContent& content);

    QString getSource() const { return this->_src.toLocalFile(); }
    void setSource(QString source);

    QString filename() const {return this->_fileName;}

private:
    QUrl _src;
    QString _fileName;
};

#endif // VIDEOCONTENT_H
