#include "videocontent.h"

#include <QSharedPointer>
#include <QDebug>

VideoContent::VideoContent()
{
    _src = "";
    _fileName = "";
}

VideoContent::VideoContent(const QString src)
{
    //_src = src;
    QUrl source = QUrl(src);
    _src = source;
    //_duration = 0;

    if(src != "")
    {
        QStringList list = src.split(QRegExp("\/+"));
        QSharedPointer<QString> str = QSharedPointer<QString>(new QString(src.split(QRegExp("\/+"))[list.size()-1]));
        _fileName = *str.data();
        //_fileName = src.split(QRegExp("\\W+"))[0];
//        QMediaPlayer *player = new QMediaPlayer(this);
//        qDebug() << "player created";
//        connect(player, &QMediaPlayer::durationChanged, this, [&](qint64 dur) {
//            qDebug() << "duration = " << dur;
//        });
//        player->setMedia(QUrl(_src));
//        qDebug() << "set media";
//        //_duration = player->duration();
//        qDebug() << player->duration();
    }
}

VideoContent::VideoContent(const VideoContent &content)
{
    _src = content._src;
    _fileName = content._fileName;
}


void VideoContent::setSource(QString source)
{
    //qDebug() << "VideoContent::setSource";
    if(source != "" || source != nullptr)
        _src = source;
    //qDebug() << "Source " << source;
    QStringList list = source.split(QRegExp("\/+"));
    QSharedPointer<QString> str = QSharedPointer<QString>(new QString(source.split(QRegExp("\/+"))[list.size()-1]));
    _fileName = *str.data();
}


