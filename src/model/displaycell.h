#ifndef DISPLAYCELL_H
#define DISPLAYCELL_H

#include <QRect>
#include "src/model/display.h"
#include "src/model/displayslist.h"
#include "src/model/videoplaylist.h"

#include "src/model/displayresolutionlist.h"
//#include "src/model/videoplaylistmodel.h"
class DisplayCell
{
public:
    DisplayCell();
    DisplayCell(int x, int y, int width, int height);
    DisplayCell(const DisplayCell& cell);
    DisplayCell& operator=(const DisplayCell& cell);
    virtual ~DisplayCell();

    QRect position() const;
    void setPosition(const QRect &position);
    void setPosition(const int x, const int y, const int width, const int height);

    QRect resolution() const;
    void setResolution(const QRect &resolution);
    void setResolution(const int width, const int height);

    //QSharedPointer<Display> display() const;
    QSharedPointer<Display> display() const;
    void setDisplay(const QSharedPointer<Display> display);
    //void setDisplay(const QString displayId);

    int getResolutionIndex();

    QSharedPointer<DisplayResolutionList> displayResolutionList() const;
    void setDisplayResolutionList(const QSharedPointer<DisplayResolutionList> &displayResolutionList);

    //QSharedPointer<VideoPlaylistModel> videoPlaylistModel() const;
    //void setVideoPlaylistModel(const QSharedPointer<VideoPlaylistModel> &videoPlaylistModel);

    int getDisplayNumber() const;
    void setDisplayNumber(int displayNumber);

    int BlendingTop() const { return  _blendingTop;}
    void setBlendingTop(int blendPosition) { this->_blendingTop = blendPosition;}

    int BlendingBottom() const {return _blendingBottom;}
    void setBlendingBottom(int blendPosition) { this->_blendingBottom = blendPosition;}

    int BlendingLeft() const {return _blendingLeft;}
    void setBlendingLeft(int blendPosition) { this->_blendingLeft = blendPosition;}

    int BlendingRight() const {return _blendingRight;}
    void setBlendingRight(int blendPosition) { this->_blendingRight = blendPosition;}

    double getBlendingPositionTop() const;
    void setBlendingPositionTop(double blendingPositionTop);

    double getBlendingPositionBottom() const;
    void setBlendingPositionBottom(double blendingPositionBottom);

    double getBlendingPositionLeft() const;
    void setBlendingPositionLeft(double blendingPositionLeft);

    double getBlendingPositionRight() const;
    void setBlendingPositionRight(double blendingPositionRight);

    double getBlendingOpacityTop() const;
    void setBlendingOpacityTop(double blendingOpacityTop);

    double getBlendingOpacityBottom() const;
    void setBlendingOpacityBottom(double blendingOpacityBottom);

    double getBlendingOpacityLeft() const;
    void setBlendingOpacityLeft(double blendingOpacityLeft);

    double getBlendingOpacityRight() const;
    void setBlendingOpacityRight(double blendingOpacityRight);

    QSharedPointer<VideoPlaylist> getPlaylist() const;
    void setPlaylist(const QSharedPointer<VideoPlaylist> &playlist);

    bool addVideo(QString source);
    bool removeVideo(int index);
    bool changeVideo(QString source);

    bool primaryScreen() const;
    void setPrimaryScreen(bool primaryScreen);

private:
    QRect _position;
    QRect _resolution;
    //QSharedPointer<Display> _display;
    QSharedPointer<Display> _display;
    QSharedPointer<DisplayResolutionList> _displayResolutionList;
    //QSharedPointer<VideoPlaylistModel> _videoPlaylistModel;
    QSharedPointer<VideoPlaylist> _playlist;

    int _blendingTop;
    int _blendingBottom;
    int _blendingLeft;
    int _blendingRight;

    double _blendingPositionTop;
    double _blendingPositionBottom;
    double _blendingPositionLeft;
    double _blendingPositionRight;

    double _blendingOpacityTop;
    double _blendingOpacityBottom;
    double _blendingOpacityLeft;
    double _blendingOpacityRight;

    int _displayNumber;

    bool _primaryScreen = false;

    void initResolutionList();
    //void initPlaylist();
};

#endif // DISPLAYCELL_H
