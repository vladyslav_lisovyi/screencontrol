#include "displayslist.h"

QList<QSharedPointer<PhysicalDisplay>> DisplaysList::physicalDisplays;
QList<QSharedPointer<LogicalDisplay>> DisplaysList::logicalDisplays;

DisplaysList::DisplaysList()
{
    this->logicalDisplays = {};
    this->physicalDisplays = {};
    initializeLists();
}

void DisplaysList::initializeLists()
{
    DISPLAY_DEVICE lpDisplayDevice;
    lpDisplayDevice.cb = sizeof(lpDisplayDevice);
    DWORD iDevNum = 0;

    CString DeviceID;
    while (EnumDisplayDevices(NULL, iDevNum, &lpDisplayDevice, 0))
    {
        DISPLAY_DEVICE lpDisplayDevice2;
        memset(&lpDisplayDevice2, NULL, sizeof(lpDisplayDevice2));
        lpDisplayDevice2.cb = sizeof(lpDisplayDevice2);
        DWORD devMon = 0;
        //PhysicalDisplay* physicalDisplay = new PhysicalDisplay();
        QSharedPointer<PhysicalDisplay> physicalDisplay = QSharedPointer<PhysicalDisplay>(new PhysicalDisplay());

        while (EnumDisplayDevices(lpDisplayDevice.DeviceName, devMon, &lpDisplayDevice2, 0))
        {
            if (lpDisplayDevice2.StateFlags & DISPLAY_DEVICE_ACTIVE && !(lpDisplayDevice2.StateFlags & DISPLAY_DEVICE_MIRRORING_DRIVER))
            {
                physicalDisplay->setDisplayDevice(lpDisplayDevice2);
                physicalDisplay->setAdapterDevice(lpDisplayDevice);
                physicalDisplay->setDeviceName(QString::fromWCharArray(lpDisplayDevice2.DeviceName));
                physicalDisplay->setDeviceKey(QString::fromWCharArray(lpDisplayDevice2.DeviceKey));
                physicalDisplay->setDeviceKey(QString::fromWCharArray(lpDisplayDevice2.DeviceKey));
                physicalDisplay->setDeviceState(lpDisplayDevice2.StateFlags);
                physicalDisplay->setDeviceString(QString::fromWCharArray(lpDisplayDevice2.DeviceString));
            }
            while (EnumDisplayDevices(lpDisplayDevice.DeviceName, devMon, &lpDisplayDevice2, EDD_GET_DEVICE_INTERFACE_NAME))
            {
                if (lpDisplayDevice2.StateFlags & DISPLAY_DEVICE_ACTIVE && !(lpDisplayDevice2.StateFlags & DISPLAY_DEVICE_MIRRORING_DRIVER))
                {
                    physicalDisplay->setDeviceId(QString::fromWCharArray(lpDisplayDevice2.DeviceID));
                    break;
                }
            }
            devMon++;
            memset(&lpDisplayDevice2, NULL, sizeof(lpDisplayDevice2));
            lpDisplayDevice2.cb = sizeof(lpDisplayDevice2);
            physicalDisplays.append(physicalDisplay);
        }

        memset(&lpDisplayDevice, NULL, sizeof(lpDisplayDevice));
        lpDisplayDevice.cb = sizeof(lpDisplayDevice);
        iDevNum++;
    }

    int displayIndex = 0;
    foreach (QScreen *screen, QGuiApplication::screens())
    {
        QSharedPointer<LogicalDisplay> logicalDisplay = QSharedPointer<LogicalDisplay>(new LogicalDisplay(screen, displayIndex));
        if(screen->name() == QGuiApplication::primaryScreen()->name())
            logicalDisplay->setPrimaryScreen();
        logicalDisplays.append(logicalDisplay);
        ++displayIndex;
    }
}
