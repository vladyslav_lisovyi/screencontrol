#include "displaycells.h"

DisplayCells::DisplayCells(QObject *parent) : QObject(parent)
{
    this->_displayCell = {};
}

DisplayCells::DisplayCells(int size, QObject *parent)
{
    this->_displayCell = {};
    for(int i = 0; i < size; i++)
    {
        // DisplayCell(0, 0, 0, 0)
        this->_displayCell.append(QSharedPointer<DisplayCell>(new DisplayCell(0, 0, 0, 0)));
    }
}

bool DisplayCells::setItemAt(int index, const QSharedPointer<DisplayCell> item)
{
    if(index < 0 || index >= this->_displayCell.size())
        return false;

    const QSharedPointer<DisplayCell> oldItem = this->_displayCell.at(index);
    if(oldItem->display()->getPhysicalDisplay()->getDeviceName() == item->display()->getPhysicalDisplay()->getDeviceName() &&
            oldItem->getDisplayNumber() == item->getDisplayNumber())
        return false;

    this->_displayCell[index] = item;
    return true;
}

void DisplayCells::appendDisplayCell(const DisplayCell &displayCell)
{
    emit preItemAppended();

    QSharedPointer<DisplayCell> cell = QSharedPointer<DisplayCell>(new DisplayCell(displayCell));
    this->_displayCell.append(cell);

    emit postItemAppended();
}

void DisplayCells::appendItem()
{
    emit preItemAppended();

    QSharedPointer<DisplayCell> item = QSharedPointer<DisplayCell>(new DisplayCell());
    this->_displayCell.append(item);

    emit postItemAppended();
}

void DisplayCells::removeItem(int index)
{
    emit preItemRemoved(index);

    this->_displayCell.removeAt(index);

    emit postItemRemoved();
}

void DisplayCells::changePositionToRight(int index, int offset)
{
    QRect oldPosition = _displayCell[index]->position();
    oldPosition.moveTo(oldPosition.x()+offset, oldPosition.y());
    _displayCell[index]->setPosition(oldPosition);

    //emit positionToRightChanged(index, offset);
    emit positionToRightChanged();
    //emit dataChanged(index, index);
}

void DisplayCells::changePositionToLeft(int index, int offset)
{
    QRect oldPosition = _displayCell[index]->position();
    oldPosition.moveTo(oldPosition.x()-offset, oldPosition.y());
    _displayCell[index]->setPosition(oldPosition);

    emit positionToLeftChanged();
}

void DisplayCells::changePositionToTop(int index, int offset)
{
    QRect oldPosition = _displayCell[index]->position();
    oldPosition.moveTo(oldPosition.x(), oldPosition.y()-offset);
    _displayCell[index]->setPosition(oldPosition);

    emit positionToTopChanged();
}

void DisplayCells::changePositionToBottom(int index, int offset)
{
    QRect oldPosition = _displayCell[index]->position();
    oldPosition.moveTo(oldPosition.x(), oldPosition.y()+offset);
    _displayCell[index]->setPosition(oldPosition);

    emit positionToBottomChanged();
}

void DisplayCells::changeRightBlend(int index, int offset)
{
    _displayCell[index]->setBlendingRight(offset);

    emit rightBlendChanged();
}

void DisplayCells::changeLeftBlend(int index, int offset)
{
    _displayCell[index]->setBlendingLeft(offset);

    emit leftBlendChanged();
}

void DisplayCells::changeTopBlend(int index, int offset)
{
    _displayCell[index]->setBlendingTop(offset);

    emit topBlendChanged();
}

void DisplayCells::changeBottomBlend(int index, int offset)
{
    _displayCell[index]->setBlendingBottom(offset);

    emit bottomBlendChanged();
}

void DisplayCells::changePositionRightBlend(int index, double offset)
{
    if(offset > 0.0 && offset < 1.0)
        _displayCell[index]->setBlendingPositionRight(offset);
}

void DisplayCells::changePositionLeftBlend(int index, double offset)
{
    if(offset > 0.0 && offset < 1.0)
        _displayCell[index]->setBlendingPositionLeft(offset);
}

void DisplayCells::changePositionTopBlend(int index, double offset)
{
    if(offset > 0.0 && offset < 1.0)
        _displayCell[index]->setBlendingPositionTop(offset);
}

void DisplayCells::changePositionBottomBlend(int index, double offset)
{
    if(offset > 0.0 && offset < 1.0)
        _displayCell[index]->setBlendingPositionBottom(offset);
}

void DisplayCells::changeOpacityRightBlend(int index, double offset)
{
    if(offset >= 0.0 && offset <= 1.0)
        _displayCell[index]->setBlendingOpacityRight(offset);
}

void DisplayCells::changeOpacityLeftBlend(int index, double offset)
{
    if(offset >= 0.0 && offset <= 1.0)
        _displayCell[index]->setBlendingOpacityLeft(offset);
}

void DisplayCells::changeOpacityTopBlend(int index, double offset)
{
    if(offset >= 0.0 && offset <= 1.0)
        _displayCell[index]->setBlendingOpacityTop(offset);
}

void DisplayCells::changeOpacityBottomBlend(int index, double offset)
{
    if(offset >= 0.0 && offset <= 1.0)
        _displayCell[index]->setBlendingOpacityBottom(offset);
}

void DisplayCells::addVideo(int displayCellIndex, QString source)
{
    if(_displayCell.size() < 0)
        return;
    _displayCell[displayCellIndex]->addVideo(source);
}

void DisplayCells::removeVideo(int displayCellIndex, int index)
{
    if(_displayCell.size() < 0)
        return;
    _displayCell[displayCellIndex]->removeVideo(index);
}

