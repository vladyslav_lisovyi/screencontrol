#include "displaysworkarea.h"

#include <QtMath>

DisplaysWorkarea::DisplaysWorkarea()
{

}

DisplaysWorkarea::DisplaysWorkarea(bool isNew)
{
    if(isNew)
    {
        //QRect rectAdditional;
        //QRect fullRes = QGuiApplication::screens()[0]->virtualGeometry();
        if(QGuiApplication::screens().size() > 1)
        {
            QRect secondRes = QGuiApplication::screens()[1]->geometry();
            this->_startPos.setX(secondRes.x());
            this->_startPos.setY(secondRes.y());
            emit startPositionChanged();
            this->_fullResolution = QGuiApplication::screens()[0]->virtualGeometry();
            emit fullResolutionChanged();
        }
        else
        {
            this->_fullResolution = QGuiApplication::screens()[0]->virtualGeometry();
            emit fullResolutionChanged();
            this->_startPos.setX(_fullResolution.x());
            this->_startPos.setY(_fullResolution.y());
            this->_startPos.setWidth(QGuiApplication::screens()[0]->geometry().width());
            this->_startPos.setHeight(QGuiApplication::screens()[0]->geometry().height());
            emit startPositionChanged();
        }
        this->_rows = 1;
        this->_columns = QGuiApplication::screens().size();

        //DisplayCell add !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

        /// --------------------------------------------------------------------------
        /// IF Session not null = get model form file, else - create new
        /// --------------------------------------------------------------------------

        this->_model = QSharedPointer<DisplayWorkareaModel>(new DisplayWorkareaModel());
        this->_gridModel = QSharedPointer<DisplayGridModel>(new DisplayGridModel());

        //this->_videoModel = QSharedPointer<VideoPlaylistModel>(new VideoPlaylistModel());

        //DisplayGridModel* gridModel = new DisplayGridModel();
        QSharedPointer<DisplayCell> displayCell = QSharedPointer<DisplayCell>(new DisplayCell(QGuiApplication::screens()[0]->geometry().x(),
                                                                              QGuiApplication::screens()[0]->geometry().y(),
                                                                              QGuiApplication::screens()[0]->geometry().width(),
                                                                              QGuiApplication::screens()[0]->geometry().height()));
        if(QGuiApplication::primaryScreen()->name() == QGuiApplication::screens()[0]->name())
            displayCell->setPrimaryScreen(true);

        QSharedPointer<Display> display = QSharedPointer<Display>(new Display(this->_gridModel->grid()->displays()[0]));
        displayCell->setDisplay(display);

        this->_model->addDisplayCell(*displayCell);
        _model->initDisplayIdForEditor();

        QRect contentWindowResolution = QRect(0, 0, 0, 0);
        int contentWindowWidth = 0;
        int contentWindowHeight = 0;
        _contentWindowResolution.setX(_startPos.x());
        _contentWindowResolution.setY(_startPos.y());
        for(int i = 1; i < _rows*_columns; i++)
        {
            QSharedPointer<DisplayCell> displayCell = QSharedPointer<DisplayCell>(new DisplayCell(QGuiApplication::screens()[i]->geometry().x(),
                                                                                  QGuiApplication::screens()[i]->geometry().y(),
                                                                                  QGuiApplication::screens()[i]->geometry().width(),
                                                                                  QGuiApplication::screens()[i]->geometry().height()));
            if(QGuiApplication::primaryScreen()->name() == QGuiApplication::screens()[i]->name())
                displayCell->setPrimaryScreen(true);

            QSharedPointer<Display> display = QSharedPointer<Display>(new Display(this->_gridModel->grid()->displays()[i]));
            displayCell->setDisplay(display);

            contentWindowWidth += displayCell->resolution().width();
            contentWindowHeight += displayCell->resolution().height();

            this->_model->addDisplayCell(*displayCell);
            _model->initDisplayIdForEditor();

            contentWindowResolution.setWidth(contentWindowWidth);
            contentWindowResolution.setHeight(contentWindowHeight);

            _contentWindowResolution = contentWindowResolution;
        }
        emit contentResolutionWindowChanged();
    }
}

DisplaysWorkarea::DisplaysWorkarea(const DisplaysWorkarea &displayWorkarea)
{
    _fullResolution = displayWorkarea._fullResolution;
    _contentWindowResolution = displayWorkarea._contentWindowResolution;
    _rows = displayWorkarea._rows;
    _columns = displayWorkarea._columns;
    _model = displayWorkarea._model;
    _startPos = displayWorkarea._startPos;
    _gridModel = displayWorkarea._gridModel;
}

DisplaysWorkarea &DisplaysWorkarea::operator=(const DisplaysWorkarea &workarea)
{
    if(this == &workarea)
        return *this;
    _fullResolution = workarea._fullResolution;
    _contentWindowResolution = workarea._contentWindowResolution;
    _rows = workarea._rows;
    _columns = workarea._columns;
    _model = workarea._model;
    _startPos = workarea._startPos;
    _gridModel = workarea._gridModel;
    return *this;
}

DisplaysWorkarea::~DisplaysWorkarea()
{
    qDebug() << "~DisplaysWorkarea";
    _model.clear();
    _gridModel.clear();
}

void DisplaysWorkarea::changeDimension(int row, int column)
{
    emit preDimensionChanged(row, column);

    this->_rows = row;
    this->_columns = column;

    emit postDimensionChanged();
}

void DisplaysWorkarea::changeFullResolution(QRect &newFullResolution)
{
    emit preFullResolutionChanged(newFullResolution);

    this->_fullResolution = newFullResolution;

    emit postFullResolutionChanged();
}

void DisplaysWorkarea::setXStartPosition(int x)
{
    _startPos.setX(x);
    emit startPositionChanged();
    if(_model->displayCells()->displayCells().size() > 1)
    {
        _xOffset = _startPos.x() - _model->displayCells()->displayCells()[1]->position().x();
        emit xOffsetChanged();
    }
}

void DisplaysWorkarea::setYStartPosition(int y)
{
    _startPos.setY(y);
    emit startPositionChanged();
    if(_model->displayCells()->displayCells().size() > 1)
    {
        _yOffset = _startPos.y() - _model->displayCells()->displayCells()[1]->position().y();
        //_yOffset = static_cast<int>(qFabs(_yOffset));
        emit yOffsetChanged();
    }
}

void DisplaysWorkarea::setWidthContentWindowRes(int width)
{
    qDebug() << "_contentWindowResolution.width " << _contentWindowResolution.width();
    _contentWindowResolution.setWidth(width);
    qDebug() << "_contentWindowResolution.width " << _contentWindowResolution.width();
    emit contentResolutionWindowChanged();
}

void DisplaysWorkarea::setHeightContentWindowRes(int height)
{
    qDebug() << "_contentWindowResolution.height " << _contentWindowResolution.height();
    _contentWindowResolution.setHeight(height);
    qDebug() << "_contentWindowResolution.height " << _contentWindowResolution.height();
    emit contentResolutionWindowChanged();
}

int DisplaysWorkarea::getRows() const
{
    return _rows;
}

void DisplaysWorkarea::setRows(int rows)
{
    _rows = rows;
}

int DisplaysWorkarea::getColumns() const
{
    return _columns;
}

void DisplaysWorkarea::setColumns(int columns)
{
    _columns = columns;
}

QRect DisplaysWorkarea::getStartPos() const
{
    return _startPos;
}

void DisplaysWorkarea::setStartPos(const QRect &startPos)
{
    _startPos = startPos;
    emit startPositionChanged();
}

bool DisplaysWorkarea::saveToFile()
{
    QTextCodec *codec = QTextCodec::codecForName("utf-8");
    QSettings settings("save1.ini", QSettings::IniFormat);
    settings.setIniCodec(codec);

    settings.setValue("NUMBER_OF_DISPLAY", _model->displayCells()->displayCells().size());
    for(int i=0; i < this->_model->displayCells()->displayCells().size(); i++)
    {
        settings.setValue("DISPLAY", i);
        QSharedPointer<DisplayCell> cell = this->_model->displayCells()->displayCells().at(i);
        settings.setValue("X", cell->position().x());
        settings.setValue("Y", cell->position().y());
        settings.setValue("WIDTH", cell->position().width());
        settings.setValue("HEIGHT", cell->position().height());

        //VideoPlaylistModel* videoModel = _model->playlistModelForSave(i);
        //VideoPlaylistModel* videoModel = _model->

        //QList<QString> a = _model->playlistModel(i)->getListOfSource();
        //QList<QString> a = dynamic_cast<VideoPlaylistModel*>(_model->playlistModel(i))->getListOfSource();
        //QList<QString> a = dynamic_cast<VideoPlaylistModel*>(_model->playlistModel(i))->getListOfSource();
//        qDebug()<< "size of a " << a.size();
//        settings.setValue("NUMBER_OF_SRC",a.size());
//        for(int j=0; j < a.size(); j++)
//        {
//            qDebug() << j << " : " << a[j];
//            settings.setValue("SOURCE"+QString(j), QString("file:///") + a[j]);
//        }
    }

    return true;
}

void DisplaysWorkarea::initDisplaysWorkarea(DisplaysWorkarea *workarea)
{
    if(this == workarea)
        return;// *this;
    _fullResolution = workarea->_fullResolution;
    _rows = workarea->_rows;
    _columns = workarea->_columns;
    _model = workarea->_model;
    _startPos = workarea->_startPos;
    _gridModel = workarea->_gridModel;
    //return *this;
}

QSharedPointer<DisplayCells> DisplaysWorkarea::loadFromFile()
{
    if(!QFile::exists("save1.ini"))
    {
        //this->saveSettings();
        return QSharedPointer<DisplayCells>(nullptr);
    }
    QTextCodec *codec = QTextCodec::codecForName("utf-8");
    QSettings settings("save1.ini", QSettings::IniFormat);
    settings.setIniCodec(codec);

    int numberOfDisplay = settings.value("NUMBER_OF_DISPLAY","").toInt();

    QSharedPointer<DisplayCells> displayCells = QSharedPointer<DisplayCells>(new DisplayCells());
    for(int i=0; i < numberOfDisplay; i++)
    {
        //DisplayCell* displayCell = new DisplayCell();
        QSharedPointer<DisplayCell> displayCell = QSharedPointer<DisplayCell>(new DisplayCell(QGuiApplication::screens()[i]->geometry().x(),
                        QGuiApplication::screens()[i]->geometry().y(),
                        QGuiApplication::screens()[i]->geometry().width(),
                        QGuiApplication::screens()[i]->geometry().height()));

        int x = settings.value("X", "").toInt();
        int y = settings.value("Y", "").toInt();
        int width = settings.value("WIDTH", "").toInt();
        int height = settings.value("HEIGHT", "").toInt();
        displayCell->setPosition(x, y, width, height); ///////////// FIX

        int numberOfVideo = settings.value("NUMBER_OF_SRC","").toInt();
//        QSharedPointer<VideoPlaylistModel> model = QSharedPointer<VideoPlaylistModel>(new VideoPlaylistModel());

//        for(int j=0; j < numberOfVideo; j++)
//        {
//            QString str = settings.value("SOURCE"+QString(j),"").toString();
//            model->addVideo(str);

//        }
//        displayCell->setVideoPlaylistModel(model);
        displayCells->appendDisplayCell(*displayCell);
    }
    return displayCells;
}

int DisplaysWorkarea::deleteHighlightFromPlaylist(int displayIndex, int videoIndex, PlaylistCollectionList *collection)
{
    QString source = _model->displayCells()->displayCells()[displayIndex]->getPlaylist()->list()[videoIndex].getSource();
    QStringList sourceList = source.split(QRegExp("[/]+"));
    QString playlistName = sourceList[sourceList.size()-2];
    qDebug() << "playlistName " << playlistName;
    int wantedIndex = -1;
    for(int i=0; i < collection->playlistColletion().size(); i++)
    {
        if(playlistName == collection->playlistColletion()[i].playlistName())
            wantedIndex = i;
    }
    qDebug() << "wanted index " << wantedIndex;
    return wantedIndex;
}

void DisplaysWorkarea::initModel()
{
    this->_model = QSharedPointer<DisplayWorkareaModel>(new DisplayWorkareaModel());
}

void DisplaysWorkarea::initGridModel()
{
    this->_gridModel = QSharedPointer<DisplayGridModel>(new DisplayGridModel());
}

QRect DisplaysWorkarea::fullResolution() const
{
    return this->_fullResolution;
}

void DisplaysWorkarea::setFullResolution(QRect &resolution)
{
    _fullResolution = resolution;
    emit fullResolutionChanged();
}

QRect DisplaysWorkarea::contentWindowResolution() const
{
    return this->_contentWindowResolution;
}

void DisplaysWorkarea::setContentWindowResolution(QRect &resolution)
{
    _contentWindowResolution = resolution;
    emit contentResolutionWindowChanged();
}
