#ifndef DISPLAYSLIST_H
#define DISPLAYSLIST_H

#include "src/model/physicaldisplay.h"
#include "src/model/logicaldisplay.h"
#include <atlstr.h>
#include <Windows.h>
#include <QString>
#include <comdef.h>
#include <QGuiApplication>
#include <QScreen>
#include <QSharedPointer>

class DisplaysList
{
public:
    DisplaysList();
    static QList<QSharedPointer<PhysicalDisplay>> physicalDisplays;
    static QList<QSharedPointer<LogicalDisplay>> logicalDisplays;
    void initializeLists();
    void displaysListChanged();
};

#endif // DISPLAYSLIST_H
