#include "physicaldisplay.h"

PhysicalDisplay::PhysicalDisplay()
{
    this->_deviceId = "";
    this->_deviceKey = "";
    this->_deviceName = "";
    this->_deviceString = "";
    this->_deviceState = 0;
    //this->_display_device;
}

PhysicalDisplay::PhysicalDisplay(DISPLAY_DEVICE adapter, DISPLAY_DEVICE device)
{
    this->_adapter_device = adapter;
    this->_display_device = device;
}

PhysicalDisplay::PhysicalDisplay(const PhysicalDisplay &display)
{
    this->_deviceId = display.getDeviceId();
    this->_deviceKey = display.getDeviceKey();
    this->_deviceName = display.getDeviceName();
    this->_deviceState = display.getDeviceState();
    this->_deviceString = display.getDeviceString();
    this->_adapter_device = display.getAdapterDevice();
    this->_display_device = display.getDisplayDevice();
}

QString PhysicalDisplay::getDeviceId() const
{
    return this->_deviceId;
}

void PhysicalDisplay::setDeviceId(QString deviceId)
{
    this->_deviceId = deviceId;
}

void PhysicalDisplay::setDeviceKey(QString deviceKey)
{
    this->_deviceKey = deviceKey;
}

void PhysicalDisplay::setDeviceName(QString deviceName)
{
    this->_deviceName = deviceName;
}

void PhysicalDisplay::setDeviceState(unsigned long deviceState)
{
   this->_deviceState = deviceState;
}

void PhysicalDisplay::setDeviceString(QString deviceString)
{
   this->_deviceString = deviceString;
}

void PhysicalDisplay::setDisplayDevice(DISPLAY_DEVICE device)
{
    this->_display_device = device;
}

void PhysicalDisplay::setAdapterDevice(DISPLAY_DEVICE adapter)
{
    this->_adapter_device = adapter;
}

QString PhysicalDisplay::getDeviceName() const
{
    return this->_deviceName;
}

QString PhysicalDisplay::getDeviceString() const
{
    return this->_deviceString;
}

QString PhysicalDisplay::getDeviceKey() const
{
    return this->_deviceKey;
}

unsigned long PhysicalDisplay::getDeviceState() const
{
    return this->_deviceState;
}

DISPLAY_DEVICE PhysicalDisplay::getDisplayDevice() const
{
    return this->_display_device;
}

DISPLAY_DEVICE PhysicalDisplay::getAdapterDevice() const
{
    return this->_adapter_device;
}
