#include "session.h"

Session::Session()
{
    this->_filename = "";
    this->_displayWorkarea = QSharedPointer<DisplaysWorkarea>(new DisplaysWorkarea(), &QObject::deleteLater);
    this->_playlistCollectionList = QSharedPointer<PlaylistCollectionList>(new PlaylistCollectionList(), &QObject::deleteLater);
}

Session::Session(DisplaysWorkarea *displayWorkarea)
{
    this->_filename = "";
    this->_displayWorkarea = QSharedPointer<DisplaysWorkarea>((displayWorkarea), &QObject::deleteLater);
    this->_playlistCollectionList = QSharedPointer<PlaylistCollectionList>(new PlaylistCollectionList(), &QObject::deleteLater);
}

Session::Session(PlaylistCollectionList *playlistCollectionList)
{
    this->_filename = "";
    this->_displayWorkarea = QSharedPointer<DisplaysWorkarea>(new DisplaysWorkarea(), &QObject::deleteLater);;
    this->_playlistCollectionList = QSharedPointer<PlaylistCollectionList>(playlistCollectionList, &QObject::deleteLater);
}

Session::Session(QString filename)
{
    setFilename(filename);
    //this->_filename = filename;
    //this->_displayWorkarea = QSharedPointer<DisplaysWorkarea>(new DisplaysWorkarea(true));
    //this->_displayWorkarea = QSharedPointer<DisplaysWorkarea>(new DisplaysWorkarea());
    //this->_playlistCollectionList = QSharedPointer<PlaylistCollectionList>(new PlaylistCollectionList());
}

Session::Session(const Session &session)
{
    this->_filename = session._filename;
    this->_displayWorkarea = session._displayWorkarea;
    this->_playlistCollectionList = session._playlistCollectionList;
}

Session::~Session()
{
    qDebug() << "~Session";
    _displayWorkarea.clear();
    _playlistCollectionList.clear();
}

QString Session::filename() const
{
    return _filename;
}

void Session::setFilename(const QString &filename)
{
    QString sourceFilename = filename;
    QStringList filenameList = filename.split(QRegExp("[.]"));
    if(filename.size() < 2)
        sourceFilename = sourceFilename + ".ini";
    else if(filenameList[filenameList.size()-1] != "ini")
    {
        sourceFilename = sourceFilename + ".ini";
    }
    _filename = sourceFilename;
}

bool Session::SaveSettings()
{
    checkAvailableSaves();
    QSettings settings("./saves/" + _filename, QSettings::IniFormat);
    QTextCodec *codec = QTextCodec::codecForName("utf-8");
    settings.setIniCodec(codec);

    settings.setValue("NUMBER_OF_DISPLAY", _displayWorkarea->model()->displayCells()->displayCells().size());
    settings.beginGroup("FULL_RESOLUTION");
    settings.setValue("WIDTH", _displayWorkarea->fullResolution().width());
    settings.setValue("HEIGHT", _displayWorkarea->fullResolution().height());
    settings.endGroup();
    settings.beginGroup("START_POSITION");
    settings.setValue("X", _displayWorkarea->getStartPos().x());
    settings.setValue("Y", _displayWorkarea->getStartPos().y());
    settings.endGroup();
    settings.beginGroup("CONTENT_WINDOW_RESOLUTION");
    settings.setValue("WIDTH", _displayWorkarea->contentWindowResolution().width());
    settings.setValue("HEIGHT", _displayWorkarea->contentWindowResolution().height());
    settings.endGroup();
    settings.beginGroup("GRID");
    settings.setValue("ROWS", _displayWorkarea->getRows());
    settings.setValue("COLUMNS", _displayWorkarea->getColumns());
    settings.endGroup();
    for(int i=0; i < _displayWorkarea->model()->displayCells()->displayCells().size(); i++)
    {
        settings.beginGroup("DISPLAY"+QString(i));
        settings.setValue("DISPLAY", i);
        QSharedPointer<DisplayCell> cell = _displayWorkarea->model()->displayCells()->displayCells().at(i);
        settings.setValue("X", cell->position().x());
        settings.setValue("Y", cell->position().y());
        settings.setValue("WIDTH", cell->position().width());
        settings.setValue("HEIGHT", cell->position().height());
        settings.setValue("DISPLAY_NAME",cell->display()->getLogicalDisplay()->getScreenName());
        settings.setValue("DISPLAY_NUM",cell->getDisplayNumber());
        settings.setValue("PRIMARY_SCREEN",cell->primaryScreen());

        settings.setValue("BLEND_TOP",cell->BlendingTop());
        settings.setValue("BLEND_BOTTOM",cell->BlendingBottom());
        settings.setValue("BLEND_LEFT",cell->BlendingLeft());
        settings.setValue("BLEND_RIGHT",cell->BlendingRight());

        settings.setValue("BLEND_TOP_POS",cell->getBlendingPositionTop());
        settings.setValue("BLEND_BOTTOM_POS",cell->getBlendingPositionBottom());
        settings.setValue("BLEND_LEFT_POS",cell->getBlendingPositionLeft());
        settings.setValue("BLEND_RIGHT_POS",cell->getBlendingPositionRight());

        settings.setValue("BLEND_TOP_OPACITY",cell->getBlendingOpacityTop());
        settings.setValue("BLEND_BOTTOM_OPACITY",cell->getBlendingOpacityBottom());
        settings.setValue("BLEND_LEFT_OPACITY",cell->getBlendingOpacityLeft());
        settings.setValue("BLEND_RIGHT_OPACITY",cell->getBlendingOpacityRight());

        if(cell->getPlaylist() || !cell->getPlaylist()->list().isEmpty())
        {
            settings.setValue("PLAYLIST_SIZE", cell->getPlaylist()->list().size());
            for(int j=0; j < cell->getPlaylist()->list().size(); j++)
            {
                settings.setValue("SOURCE"+QString(j), cell->getPlaylist()->list()[j].getSource());
            }
        }
        else {
            settings.setValue("PLAYLIST_SIZE", 0);
        }
        settings.endGroup();
    }

    settings.beginGroup("PLAYLIST_COLLECTION");
    if(!(_playlistCollectionList->playlistColletion().size() == 0))
    {
        settings.setValue("PLAYLIST_COLLECTION_SIZE",_playlistCollectionList->playlistColletion().size());
        for(int i = 0; i<_playlistCollectionList->playlistColletion().size(); i++)
        {
            // was -> videoplaylist
            settings.setValue("PC_SOURCE"+QString(i), _playlistCollectionList->playlistColletion()[i].videoplaylist()->sourceForPlaylistCollection());
            // _playlistCollectionList->playlistColletion()[i].sourceForPlaylistCollection()); // sourceForPlaylistCollection()
        }
    }
    else
    {
        settings.setValue("PLAYLIST_COLLECTION_SIZE",0);
    }
    settings.endGroup();

    return true;

    //if(!checkAvailableSaves())
    //{

        // ... set workarea

    //}

//    QDir saveFilesDir = QDir("./saves");
//    QStringList nameFilter("*.ini");
//    QStringList iniFilesAndDirectories = saveFilesDir.entryList(nameFilter);

//    for(int i = 0; i < iniFilesAndDirectories.size(); i++)
//    {
//        QSettings settings("./saves" + iniFilesAndDirectories.at(i), QSettings::IniFormat);
//        QTextCodec *codec = QTextCodec::codecForName("utf-8");
//        settings.setIniCodec(codec);
//        Session session = Session();
//        session.setFilename(iniFilesAndDirectories.at(i));
//        // ... set workarea

//    }
}

void Session::LoadSettings()
{
    if(!QFile::exists("./saves/"+_filename))
        return;

    QTextCodec *codec = QTextCodec::codecForName("utf-8");
    QSettings settings("./saves/" + _filename, QSettings::IniFormat);
    settings.setIniCodec(codec);

    int numberOfDisplay = settings.value("NUMBER_OF_DISPLAY","").toInt();

    settings.beginGroup("FULL_RESOLUTION");
    int fullResWidth = settings.value("WIDTH", "").toInt();
    int fullResHeight = settings.value("HEIGHT", "").toInt();
    settings.endGroup();
    settings.beginGroup("START_POSITION");
    int startX = settings.value("X", "").toInt();
    int startY = settings.value("Y", "").toInt();
    settings.endGroup();
    settings.beginGroup("CONTENT_WINDOW_RESOLUTION");
    int contentWindowWidth = settings.value("WIDTH", "").toInt();
    int contentWindowHeight = settings.value("HEIGHT", "").toInt();
    settings.endGroup();
    settings.beginGroup("GRID");
    int rows =  settings.value("ROWS", "").toInt();
    int columns = settings.value("COLUMNS", "").toInt();
    settings.endGroup();

    auto availableDisplays = QGuiApplication::screens();
    if(rows*columns != availableDisplays.size())
    {
        throw ConfigurationException();
        return;
    }

    QRect rect = QRect(0, 0, fullResWidth, fullResHeight);
    QRect contRect = QRect(startX, startY, contentWindowWidth, contentWindowHeight);
    _displayWorkarea->setFullResolution(rect);
    _displayWorkarea->setStartPos(QRect(startX, startY, 0, 0));
    _displayWorkarea->setContentWindowResolution(contRect);
    _displayWorkarea->setRows(rows);
    _displayWorkarea->setColumns(columns);

    _displayWorkarea->initModel();
    _displayWorkarea->initGridModel();

    //QSharedPointer<DisplayCells> displayCells = QSharedPointer<DisplayCells>(new DisplayCells());

//    _displayWorkarea->setMainDisplayCell(new DisplayCell(QGuiApplication::screens()[0]->geometry().x(),
//                                         QGuiApplication::screens()[0]->geometry().y(),
//                                         QGuiApplication::screens()[0]->geometry().width(),
//                                         QGuiApplication::screens()[0]->geometry().height()));

    //QRect fullRes = QGuiApplication::screens()[0]->virtualGeometry();
    //QRect firstRes = QGuiApplication::screens()[0]->geometry();
    //QRect secondRes = QGuiApplication::screens()[1]->geometry();

    //QRect finalREs = QRect(0,0,0,0);
    //finalREs.setX(secondRes.x()); // int release  secondRes.x() - 3*1024 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    //finalREs.setY(secondRes.y());
    //finalREs.setWidth( fullRes.width() - firstRes.width()  );
    //finalREs.setHeight( secondRes.height() );

    //_displayWorkarea->setFullResolution(finalREs);
    //_displayWorkarea->setStartPos(QRect(finalREs.x(), finalREs.y(), 0,0));
//    this->_startPos.setY(finalREs.y());
//    this->_startPos.setX(finalREs.x());

    for(int i=0; i < numberOfDisplay; i++)
    {
        settings.beginGroup("DISPLAY"+QString(i));

        int x = settings.value("X", "").toInt();
        int y = settings.value("Y", "").toInt();
        int width = settings.value("WIDTH", "").toInt();
        int height = settings.value("HEIGHT", "").toInt();
        QString disName = settings.value("DISPLAY_NAME", "").toString();
        int disNumber = settings.value("DISPLAY_NUM", "").toInt();
        bool primaryScreen = settings.value("PRIMARY_SCREEN", "").toBool();

        int blendTop = settings.value("BLEND_TOP","").toInt();
        int blendBottom = settings.value("BLEND_BOTTOM","").toInt();
        int blendLeft = settings.value("BLEND_LEFT","").toInt();
        int blendRight = settings.value("BLEND_RIGHT","").toInt();

        double blendPositionTop = settings.value("BLEND_TOP_POS","").toDouble();
        double blendPositionBottom = settings.value("BLEND_BOTTOM_POS","").toDouble();
        double blendPositionLeft = settings.value("BLEND_LEFT_POS","").toDouble();
        double blendPositionRight = settings.value("BLEND_RIGHT_POS","").toDouble();

        double blendOpacityTop = settings.value("BLEND_TOP_OPACITY","").toDouble();
        double blendOpacityBottom = settings.value("BLEND_BOTTOM_OPACITY","").toDouble();
        double blendOpacityLeft = settings.value("BLEND_LEFT_OPACITY","").toDouble();
        double blendOpacityRight = settings.value("BLEND_RIGHT_OPACITY","").toDouble();

        int playlistSize = settings.value("PLAYLIST_SIZE", "").toInt();

        QSharedPointer<DisplayCell> displayCell = QSharedPointer<DisplayCell>(new DisplayCell());

        int displayIndex = -1;
        for(int h=0; h < _displayWorkarea->gridModel()->grid()->displays().size(); h++)
        {
            if(disName == _displayWorkarea->gridModel()->grid()->displays()[h].getLogicalDisplay()->getScreenName())
            {
                displayIndex = h;
                break;
            }
        }

        for(int j=0; j < availableDisplays.size(); j++)
        {
            QString name = availableDisplays[j]->name();
            if(name == disName)
            {
                displayCell->setPosition(x, y, width, height);
                displayCell->setResolution(QRect(0,0, width, height));
                displayCell->setDisplayNumber(disNumber);
                displayCell->setPrimaryScreen(primaryScreen);

                displayCell->setBlendingTop(blendTop);
                displayCell->setBlendingLeft(blendLeft);
                displayCell->setBlendingRight(blendRight);
                displayCell->setBlendingBottom(blendBottom);

                displayCell->setBlendingPositionTop(blendPositionTop);
                displayCell->setBlendingPositionBottom(blendPositionBottom);
                displayCell->setBlendingPositionLeft(blendPositionLeft);
                displayCell->setBlendingPositionRight(blendPositionRight);

                displayCell->setBlendingOpacityTop(blendOpacityTop);
                displayCell->setBlendingOpacityBottom(blendOpacityBottom);
                displayCell->setBlendingOpacityLeft(blendOpacityLeft);
                displayCell->setBlendingOpacityRight(blendOpacityRight);

                //Display display = _displayWorkarea->gridModel()->grid()->displays()[displayIndex];
                QSharedPointer<Display> display =
                        QSharedPointer<Display>(new Display(_displayWorkarea->gridModel()->grid()->displays()[displayIndex]));
                displayCell->setDisplay(display);

                if(playlistSize != 0)
                {
                    for(int k=0; k < playlistSize; k++)
                    {
                        QString source = settings.value("SOURCE"+QString(k), "").toString();
                        displayCell->addVideo(source);
                    }
                }

                _displayWorkarea->model()->addDisplayCell(*displayCell);
            }
        }

        settings.endGroup();
    }

    settings.beginGroup("PLAYLIST_COLLECTION");
    int playlistCollectionSize = settings.value("PLAYLIST_COLLECTION_SIZE", "").toInt();
    for(int i = 0; i<playlistCollectionSize; i++)
    {
        QString source = settings.value("PC_SOURCE"+QString(i), "").toString();
        _playlistCollectionList->appendItem(source);
        //PlaylistCollection collection = PlaylistCollection();
        //QSharedPointer<VideoPlaylist> playlist = QSharedPointer<VideoPlaylist>(new VideoPlaylist());
        //playlist->initVideoPlaylistByFolder(source);
        //PlaylistCollection collection = PlaylistCollection(*playlist.get());
        //_playlistCollectionList->playlistColletion().append(collection);
        //_playlistColletion.append(collection);
    }
    settings.endGroup();

    //_displayWorkarea->model()->initDisplayIdForEditor();
}

bool Session::checkAvailableSaves()
{
    if(!QDir("./saves").exists())
    {
        QDir().mkdir("./saves");
        return false;
    }
    QDir saveFilesDir = QDir("./saves");
    QStringList nameFilter("*.ini");
    QStringList iniFilesAndDirectories = saveFilesDir.entryList(nameFilter);
    if(iniFilesAndDirectories.size() == 0)
        return false;
    bool isSaveFile = false;
    //int index
    for(int i=0; i<iniFilesAndDirectories.size(); i++)
    {
        if(iniFilesAndDirectories[i] == _filename)
            isSaveFile = true;
    }
    if(!isSaveFile)
        return false;
    return true;
}

QSharedPointer<DisplaysWorkarea> Session::displayWorkarea() const
{
    return _displayWorkarea;
}

void Session::setDisplayWorkarea(const QSharedPointer<DisplaysWorkarea> displayWorkarea)
{
    _displayWorkarea = displayWorkarea;
}

QSharedPointer<PlaylistCollectionList> Session::playlistCollectionList() const
{
    return _playlistCollectionList;
}

void Session::setPlaylistCollectionList(const QSharedPointer<PlaylistCollectionList> playlistCollectionList)
{
    _playlistCollectionList = playlistCollectionList;
}
