#ifndef SESSIONMODEL_H
#define SESSIONMODEL_H

#include <QAbstractListModel>

#include "src/model/sessionlist.h"

class SessionList;

class SessionModel : public QAbstractListModel
{
    Q_OBJECT
    Q_PROPERTY(SessionList* sessionList READ sessionList WRITE setSessionList)

public:
    explicit SessionModel(QObject *parent = nullptr);

    enum{
        filename = Qt::UserRole,
        displayworkarea,
        playlistCollectionList
    };

    // Basic functionality:
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    // Editable:
    bool setData(const QModelIndex &index, const QVariant &value,
                 int role = Qt::EditRole) override;

    Qt::ItemFlags flags(const QModelIndex& index) const override;

    virtual QHash<int, QByteArray> roleNames() const override;

    SessionList *sessionList() const;

    Q_INVOKABLE
    QVariantMap get(int row);

public slots:
    void setSessionList(SessionList* sessionList);

private:
    SessionList* _sessionList;
};

#endif // SESSIONMODEL_H
