#ifndef PLAYLISTCOLLECTIONMODEL_H
#define PLAYLISTCOLLECTIONMODEL_H

#include <QAbstractListModel>
#include <QSharedPointer>

class PlaylistCollectionList;

class PlaylistCollectionModel : public QAbstractListModel
{
    Q_OBJECT
    Q_PROPERTY(PlaylistCollectionList *playlistCollection READ playlistCollection NOTIFY playlistCollectionChanged) // WRITE setPlaylistCollection
public:
    explicit PlaylistCollectionModel(QObject *parent = nullptr);
    ~PlaylistCollectionModel() override;

    enum {
        playlistName = Qt::UserRole,
        videoplaylist,
        videoplaylistModel
    };

    // Basic functionality:
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    // Editable:
    bool setData(const QModelIndex &index, const QVariant &value,
                 int role = Qt::EditRole) override;

    Qt::ItemFlags flags(const QModelIndex& index) const override;

    virtual QHash<int, QByteArray> roleNames() const override;

    PlaylistCollectionList* playlistCollection() const { return _playlistCollection.data(); }

signals:
    void playlistCollectionChanged();

public slots:
    //void setPlaylistCollection(PlaylistCollectionList* playlistCollection);
    void setPlaylistCollection(QSharedPointer<PlaylistCollectionList> playlistCollection);

private:
    QSharedPointer<PlaylistCollectionList> _playlistCollection;
};

#endif // PLAYLISTCOLLECTIONMODEL_H
