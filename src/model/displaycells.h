#ifndef DISPLAYCELLS_H
#define DISPLAYCELLS_H

#include <QObject>
#include "src/model/displaycell.h"

class DisplayCells : public QObject
{
    Q_OBJECT
public:
    explicit DisplayCells(QObject *parent = nullptr);
    explicit DisplayCells(int size, QObject *parent = nullptr);

    QVector<QSharedPointer<DisplayCell>> displayCells() {return this->_displayCell; }
    // init setItemAt
    bool setItemAt(int index, const QSharedPointer<DisplayCell> item);
    void appendDisplayCell(const DisplayCell &displayCell);

signals:
    void preItemAppended();
    void postItemAppended();
    void preItemRemoved(int index);
    void postItemRemoved();

    //void positionToRightChanged(int index, int offset);
    void positionToRightChanged();
    void positionToLeftChanged();
    void positionToTopChanged();
    void positionToBottomChanged();

    void rightBlendChanged();
    void leftBlendChanged();
    void topBlendChanged();
    void bottomBlendChanged();

public slots:
    void appendItem();
    void removeItem(int index);

    void changePositionToRight(int index, int offset);
    void changePositionToLeft(int index, int offset);
    void changePositionToTop(int index, int offset);
    void changePositionToBottom(int index, int offset);

    void changeRightBlend(int index, int offset);
    void changeLeftBlend(int index, int offset);
    void changeTopBlend(int index, int offset);
    void changeBottomBlend(int index, int offset);

    void changePositionRightBlend(int index, double offset);
    void changePositionLeftBlend(int index, double offset);
    void changePositionTopBlend(int index, double offset);
    void changePositionBottomBlend(int index, double offset);

    void changeOpacityRightBlend(int index, double offset);
    void changeOpacityLeftBlend(int index, double offset);
    void changeOpacityTopBlend(int index, double offset);
    void changeOpacityBottomBlend(int index, double offset);

    void addVideo(int displayCellIndex, QString source);
    void removeVideo(int displayCellIndex, int index);

private:
    QVector<QSharedPointer<DisplayCell>> _displayCell;
};

#endif // DISPLAYCELLS_H
